DROP PROCEDURE IF EXISTS FindNearbyCandidate;
DELIMITER $$
CREATE PROCEDURE FindNearbyCandidate(user_id varchar(64), latin DECIMAL(9,6), longin DECIMAL(9,6), distance_filter int)
BEGIN

DECLARE uid varchar(64);
DECLARE lat DECIMAL(9,6);
DECLARE longi DECIMAL(9,6);
DECLARE done INT DEFAULT FALSE;
DECLARE distance float;

DECLARE EarthRadius float;
DECLARE MathPI float;
DECLARE latInRadians float;
DECLARE longInRadians float;
DECLARE latRadians float;
DECLARE longRadians float;

declare cur cursor for select userID,latitude,longitude from user_profile;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

SET EarthRadius = 6371;
SET MathPI = PI();

DROP TEMPORARY TABLE IF EXISTS candidate;
CREATE TEMPORARY TABLE candidate(userID varchar(64), latitude DECIMAL(9,6), longitude DECIMAL(9,6), distance float);

open cur;

start_loop: loop

fetch next from cur into uid,lat,longi;

IF done THEN
	leave start_loop;
END IF;

IF (lat = latin AND longi = longin) or (user_id = uid) THEN
	SET distance = 0.0;
ELSE
	SET latInRadians = latin * MathPI / 180;
    SET longInRadians = longin * MathPI / 180;
    SET latRadians = lat * MathPI / 180;
    SET longRadians = longi * MathPI / 180;
    
    SET distance = sin(latRadians/2 - latInRadians/2) * sin(latRadians/2 - latInRadians/2) + cos(latInRadians) * cos(latRadians) * sin(longRadians/2 - longInRadians/2) * sin(longRadians/2 - longInRadians/2);
    
    SET distance = 2 * atan2(sqrt(distance), sqrt(1 - distance)) * EarthRadius;
END IF; 

IF distance < distance_filter AND user_id <> uid THEN
	INSERT INTO candidate VALUES(uid,lat,longi,distance);
END IF;

end loop;
Close cur;

SELECT user_profile.userID, user_profile.name, user_profile.avatar_url, candidate.latitude, candidate.longitude, candidate.distance FROM user_profile INNER JOIN candidate ON user_profile.userID=candidate.userID;
DROP TEMPORARY TABLE IF EXISTS candidate;

END$$
DELIMITER ;