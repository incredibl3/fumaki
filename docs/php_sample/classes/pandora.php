<?php
require_once('request.php');
require_once('cache.php');
require_once('eve.php');

class PandoraService
{
    const ALERT = 'alert';
    const ASSET = 'asset';
    const AUTH = 'auth';
    const BRIDGE = 'bridge';
    const CONFIG = 'config';
    const CHAT = 'chat';
    const EVENT = 'event';
    const EVENT_PUBLISHING = 'event_publishing';
    const FEEDS = 'feeds';
    const GAMESPACE = 'gamespace';
    const GIFT_ALL = 'gift_all';
    const GLLIVE = 'gllive';
    const LAUNCHER = 'launcher';
    const LEADERBOARD = 'leaderboard';
    const LOBBY = 'lobby';
    const LOTTERY = 'lottery';
    const MATCHMAKER = 'matchmaker';
    const MESSAGE = 'message';
    const SOCIAL = 'social';
    const SCHEDULE = 'schedule';
    const STATS = 'stats';
    const STORAGE = 'storage';
    const SWAMP = 'swamp';
    const VOICE = 'voice';

    private static $environments = array(
        'mdc:alpha' => 'valpha.gameloft.com',
        'mdc:beta' => 'valpha.gameloft.com',
        'mdc:gold' => 'vgold.gameloft.com',
        'eur:gold' => 'vgold.eur.gameloft.com',
    );

    /**
     * Locate service URL by environment. I think it could be useful.
     *
     * @param string $env - Environment. Format is mdc:alpha
     * @param $service - Service name from the list. Like PandoraService::AUTH
     * @return string - Service URL
     * @throws Exception
     */
    public static function locateByEnv($env = 'mdc:alpha', $service)
    {
        if (!self::$environments[$env])
        {
            throw new Exception('Environment ' . $env . ' not found');
        }

        $pandoraURL = 'http://' . self::$environments[$env] . ':20000/locate/' . $service;
        return self::_locate($pandoraURL);
    }


    public static function cacheServiceURL($serviceURL, Array $cache_params)
    {
        $cache_params = array_merge(array(
            'clientId' => '',
            'service' => '',
            'datacenter' => false,
        ), $cache_params);
        Cache::getInstance()->set($cache_params, $serviceURL, array('expirationTime' => 10));
    }

    /**
     * Locate service URL by clientId. The heart of the PandoraService class.
     *
     * @param $clientId - User clientId
     * @param $service - Service name from the list. Like PandoraService::AUTH
     * @param bool/string $datacenter - Datacenter name (mdc, eur, ...). Default is false which means choose the first.
     * @return string - Service URL
     */
    public static function locateByClientID($clientId, $service, $datacenter = false)
    {
        $cache_params = array(
            'clientId' => $clientId,
            'service' => $service,
            'datacenter' => $datacenter,
        );

        if (!$serviceURL = Cache::getInstance()->get($cache_params))
        {
            $eveService = new EveService($clientId);
            $pandoraURL = $eveService->getServiceURL($datacenter, 'pandora');
            if (!$pandoraURL)
            {
                return false;
            }
            $serviceURL = self::_locate($pandoraURL . '/locate/' . $service);
        }
        return $serviceURL;
    }

    /**
     * Locate service by provided Pandora URL. Aux method.
     *
     * @param $pandoraURL - Pandora URL for locating service URL
     * @return string - Service URL
     */
    protected static function _locate($pandoraURL)
    {
        $cache_params = array('url' => $pandoraURL);
        if (!$serviceURL = Cache::getInstance()->get($cache_params))
        {
            $request = Request::doGet(array('url' => $pandoraURL));
            $result = $request->getRawResult();
            if ($request->isOK() && $result)
            {
                $serviceURL = 'https://' . $result;
                Cache::getInstance()->set($cache_params, $serviceURL, array('expirationTime' => 10));
            }
        }
        return $serviceURL;
    }
}
