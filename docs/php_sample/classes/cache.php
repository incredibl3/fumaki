<?php

class Cache
{
    protected static $_instance;
    protected $_cachedValues = array();
    protected $_expiredKeys = array();

    private function __construct() { }
    private function __clone()     { }
    private function __wakeup()    { }

    /**
     * Return instance of the Cache class. Cache is a singleton.
     *
     * @return Cache
     */
    public static function getInstance()
    {
        if (!isset(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Return expiration time, set to a future unix timestamp, defaults to 1 day if no expiration given
     *
     * @param mixed $expiration unix timestamp or formatted date string
     * @see strtotime() for accepted string inputs.
     * @return int Expiration time in seconds
     */
    public static function setExpiration($expiration) {
        $time = time();
        if (is_numeric($expiration)) {
            $expTime = $expiration > $time ? $expiration : $time + $expiration;
        } else {
            $expTime = strtotime($expiration, $time);
            if(!$expTime || $expTime < $time) {
                // Set to daily if something goes wrong
                $expTime = $time + 86400;
            }
        }
        return $expTime;
    }

    /**
     * Check if expired data in the cache with provided cacheKey or not
     * Also mark this cacheKey as expired
     *
     * @param $time - Unix timestamp time
     * @param $cacheKey - Cache key for the cached data
     * @return bool - True is expired
     */
    public function isExpired($time, $cacheKey)
    {
        if (!$time || time() < $time)
        {
            return false;
        }
        $this->_expiredKeys[$cacheKey] = true;
        return true;
    }

    /**
     * Check if key marked as expired using Params array instead of cacheKey
     *
     * @param array $params - Params for creating cacheKey
     * @return bool - Tru is marked as expired
     */
    public function isKeyExpired($params)
    {
        $cacheKey = $this->_getCacheKey($params);
        return !empty($this->_expiredKeys[$cacheKey]);
    }

    /**
     * Get cached data
     *
     * @param array $params - Params for creating cacheKey
     * @param array $optionalParams - Optional parameters. Type could be 'mem' or 'file'. returnExpired default is false.
     * @return mixed - false / cached data
     * @throws Exception - if don't support provided caching type
     */
    public function get(Array $params, Array $optionalParams = array())
    {
        $optionalParams = array_merge(array('type' => 'mem', 'returnExpired' => false), $optionalParams);
        if (!$GLOBALS['config']['file_caching_enabled']) { $optionalParams['type'] = 'mem'; }

        $cacheKey = $this->_getCacheKey($params);
        $this->_expiredKeys[$cacheKey] = false;
        switch ($optionalParams['type'])
        {
            case 'mem':
                $result = $this->_getMem($cacheKey);
            break;

            case 'file':
                $result = $this->_getFile($cacheKey);
            break;

            default:
              throw new Exception('Unsupported caching type "' . $optionalParams['type'] . '"');
            break;
        }
        if ($result)
        {
            if (!$this->isExpired($result['expirationTime'], $cacheKey) || $optionalParams['returnExpired'])
            {
                return $result['value'];
            }
        }
        return false;
    }

    /**
     * Set cached data
     *
     * @param array $params - Params for creating cacheKey
     * @param $value - Value for caching
     * @param array $optionalParams - Optional parameters. Type could be 'mem' or 'file'. expirationType in seconds. Default is 1 day.
     * @throws Exception - if don't support provided caching type
     */
    public function set(Array $params, $value, Array $optionalParams = array())
    {
        $optionalParams = array_merge(array('type' => 'mem', 'expirationTime' => 86400), $optionalParams);
        if (!$GLOBALS['config']['file_caching_enabled']) { $optionalParams['type'] = 'mem'; }
        //$optionalParams['expirationTime'] = 10; // for test

        $cacheKey = $this->_getCacheKey($params);
        $expirationTime = $this->setExpiration($optionalParams['expirationTime']);
        switch ($optionalParams['type'])
        {
            case 'mem':
                $this->_setMem($cacheKey, $value, $expirationTime);
                break;

            case 'file':
                $this->_setFile($cacheKey, $value, $expirationTime);
                break;

            default:
                throw new Exception('Unsupported caching type "' . $optionalParams['type'] . '"');
                break;
        }
        $this->_expiredKeys[$cacheKey] = false;
    }

    /**
     * Get cached data using 'mem' type. (Auxiliary method)
     *
     * @param $cacheKey - cacheKey for data
     * @return mixed - false / Cached value
     */
    protected function _getMem($cacheKey)
    {
        return isset($this->_cachedValues[$cacheKey]) ? $this->_cachedValues[$cacheKey] : false;
    }

    /**
     * Set cached data using 'mem' type. (Auxiliary method)
     *
     * @param string $cacheKey - cacheKey for data
     * @param mixed $value - Value for caching
     * @param $expirationTime - expirationType in seconds
     */
    protected function _setMem($cacheKey, $value, $expirationTime)
    {
        $this->_cachedValues[$cacheKey] = array(
            'expirationTime' => $expirationTime,
            'value' => $value,
        );
    }

    /**
     * Get cached data using 'file' type. (Auxiliary method). Fetch the cached data from file.
     *
     * @param string $cacheKey - cacheKey for data
     * @return mixed - false / File data
     */
    protected function _getFile($cacheKey) {
        $result = false;
        $filename = $this->_getFilename($cacheKey);
        $file = @file_get_contents($filename);
        if (!$file)
        {
            $filename = $this->_getFilename($cacheKey, true);
            $file = @file_get_contents($filename);
        }
        if ($file)
        {
            $pos = strpos($file, "\n");
            $result = array(
                'expirationTime' => (int)substr($file, 0, $pos),
                'value' => json_decode(substr($file, $pos+1), true),
            );
        }
        return $result;
    }

    /**
     * Set cached data using 'file' type. (Auxiliary method)
     *
     * @param string $cacheKey - cacheKey for data
     * @param bool $yesterday
     * @return string
     */
    protected static function _getFilename($cacheKey, $yesterday = false)
    {
        $subdir = strftime('%Y%m%d', $yesterday ? strtotime('-1 day') : time()) . '/';
        $subdir .= intval(substr($cacheKey, 0, 4), 16) % 256;
        $dir = $GLOBALS['config']['caching_path'] . '/' . $subdir;

        if (!is_dir($dir))
        {
            mkdir($dir, 0777, true);
            chmod($dir, 0777);
        }
        return $dir . '/' . $cacheKey . '.cache';
    }


    /**
     * Set cached data using 'file' type. (Auxiliary method). Save data to file.
     *
     * @param string $cacheKey - cacheKey for data
     * @param mixed $value - Value for caching
     * @param $expirationTime - expirationType in seconds
     */
    protected function _setFile($cacheKey, $value, $expirationTime)
    {
        $filename = $this->_getFilename($cacheKey);
        $data = $expirationTime . "\n" . json_encode($value);
        file_put_contents($filename, $data, LOCK_EX);
    }

    /**
     * @param array $params - Parameters to create cacheKey
     * @return string - CacheKey
     */
    protected function _getCacheKey(Array $params)
    {
        return md5(json_encode($params));
    }
}
