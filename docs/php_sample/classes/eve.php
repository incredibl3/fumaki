<?php
require_once(dirname(__FILE__) . '/../includes/common.php');
require_once('request.php');
require_once('cache.php');

define('EVE_SERVER_URL', 'http://eve.gameloft.com:20001');

class EveService
{
    protected $_clientId = false;
    protected $_datacenters = array();
    protected $_datacentersNames = array();
    protected $_servicesList = array();

    /**
     * Class constructor
     *
     * @param bool $clientId - User clientId
     */
    public function __construct($clientId = false)
    {
        $this->_clientId = $clientId;
        $this->_getDatacenters();
    }

    /**
     * Get datacenters list for the game
     *
     * @return array - Datacenters list for the game
     */
    protected function _getDatacenters()
    {
        $cache_params = array('name' => 'eveDatacenters', 'clientId' => $this->_clientId);
        if (!$this->_datacenters = Cache::getInstance()->get($cache_params))
        {
            $request = Request::doGet(array('url' => EVE_SERVER_URL . '/config/' . $this->_clientId . '/datacenters'));
            if (!$request->isOK())
            {
                error_log('ClientId ' . $this->_clientId . ' was not found on Eve');
                return array();
            }
            $this->_datacenters = $request->getResult();
            Cache::getInstance()->set($cache_params, $this->_datacenters);
        }
        $this->_datacentersNames = array_get_column_key($this->_datacenters, 'name');
        return $this->_datacenters;
    }

    /**
     * Get services list for a datacenter
     *
     * @param string $datacenter
     * @return array - Datacenters services list
     */
    protected function _getServicesList($datacenter)
    {
        if (!$this->_datacentersNames)
        {
            error_log('No datacenters found on Eve');
            return array();
        }
        if (!in_array($datacenter, $this->_datacentersNames))
        {
            error_log('Datacenter \'' . $datacenter . '\' not found on Eve');
            return array();
        }

        $cache_params = array('name' => 'eveServicesList', 'clientId' => $this->_clientId);
        if (!$this->_servicesList[$datacenter] = Cache::getInstance()->get($cache_params))
        {
            $this->_servicesList[$datacenter] = Request::doGet(array('url' => EVE_SERVER_URL . '/config/' . $this->_clientId . '/datacenters/' . $datacenter . '/urls'))->getResult();
            Cache::getInstance()->set($cache_params, $this->_servicesList[$datacenter]);
        }
        return $this->_servicesList[$datacenter];
    }

    /**
     * @param string $datacenter - Datacenter name (mdc, eur, ...). Default is empty which means choose the first.
     * @return string - Datacenter.
     */
    public function getDatacenter($datacenter = '')
    {
        if (empty($datacenter))
        {
            $datacenter = current($this->_datacentersNames);
        }
        return $datacenter;
    }

    /**
     * Return service URL for provided datacenter
     *
     * @param string $datacenter - Datacenter name (mdc, eur, ...). Default is empty which means choose the first.
     * @param string $serviceName - Service name (pandora, game_portal, ...). Default is 'pandora'.
     * @return string - Service URL for the datacenter
     */
    public function getServiceURL($datacenter = '', $serviceName = 'pandora')
    {
        $servicesList = $this->_getServicesList($this->getDatacenter($datacenter));
        if ($servicesList && !empty($servicesList[$serviceName]))
        {
            return $servicesList[$serviceName];
        }
        return false;
    }
}