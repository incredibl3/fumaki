<?php
require_once(dirname(__FILE__) . '/../includes/common.php');
require_once('cache.php');
require_once('request.php');
require_once('eve.php');
require_once('pandora.php');

class User
{
    const CLIENTID_NOT_FOUND = 'ClientId not found.';
    const INCORRECT_CREDENTIALS = 'User credentials are incorrect.';

    protected $_clientId = false;
    protected $_settings = array();
    protected $_error = '';
    protected $_userCacheKey = array();
    protected $_optEnvironment = false;
    protected $_requestNumber = 0;
    protected $_requestErrors = array();

    /**
     * Class constructor
     *
     * @param string $clientId - ClientID for the user
     * @param string $env - Environment for multi environmental game. Default is empty which means the first.
     */
    public function __construct($clientId, $env = '')
    {
        $this->_clientId = $clientId;
        if (!empty($env))
        {
            $this->_optEnvironment = $env;
        }
    }

    public function getError()
    {
        return $this->_error;
    }

    public function getLastQueryError()
    {
        $requestNumber = $this->_getRequestNumber();
        if (isset($this->_requestErrors[$requestNumber]))
        {
            return $message = $this->_requestErrors[$requestNumber];
        }
        return false;
    }

    /**
     * Set user settings
     */
    private function setSettings($settings)
    {
        if (!empty($settings) && is_array($settings))
        {
            $this->_settings = $settings;
        }
    }

    /**
     * Get saved user settings
     * @return mixed - false / Data
     */
    public function getSettings()
    {
        return !empty($this->_settings) ? $this->_settings : false;
    }

    /**
     * Get saved user settings
     * @param string $settingKey - Key for saved settings
     * @return mixed - false / Data
     */
    public function getSetting($settingKey)
    {
        return !empty($this->_settings[$settingKey]) ? $this->_settings[$settingKey] : false;
    }

    /**
     * Get saved Access Token
     * @return string - Access Token
     */
    public function getAccessToken()
    {
        return $this->getSetting('access_token');
    }

    /**
     * Return current environment for the clientId
     *
     * @return string - Current environment. Format 'datacenter:environment'
     */
    public function getEnvironment()
    {
        $eveService = new EveService($this->_clientId);
        $datacenter = $eveService->getDatacenter($this->_optEnvironment);
        $cache_params = array('clientId' => $this->_clientId, 'datacenter' => $datacenter);
        if (!$environment = Cache::getInstance()->get($cache_params))
        {
            $pandoraURL = $eveService->getServiceURL($datacenter, 'pandora');
            $env = 'gold';
            if (strpos($pandoraURL, 'beta'))
            {
                $env = 'beta';
            }
            elseif (strpos($pandoraURL, 'alpha'))
            {
                $env = 'alpha';
            }
            $environment = array(
                'datacenter' => $datacenter,
                'environment' => $env
            );
            Cache::getInstance()->set($cache_params, $environment);
        }
        return $environment['datacenter'] . ':' . $environment['environment'];
    }

    /**
     * Return service URL for the clientId
     *
     * @param string $serviceName - Service name (pandora, game_portal, ...)
     * @return string - Current service URL.
     */
    public function getServiceURL($serviceName = 'game_portal')
    {
        $eveService = new EveService($this->_clientId);
        return $eveService->getServiceURL($this->_optEnvironment, $serviceName);
    }

    // auth part
    /**
     * User auth (static)
     *
     * @param $clientId - ClientId for the game.
     * @param string $username - User credential. Format 'credentialType:Username'
     * @param string $password - User password
     * @param string $scope - User scopes. Format 'auth message'
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    static public function auth($clientId, $username, $password, $scope, Array $optionalParams = array())
    {
        $user = new User($clientId);
        $user->_auth($username, $password, $scope, $optionalParams);
        return $user;
    }

    /**
     * User impersonated auth (static)
     *
     * @param $clientId - ClientId for the game.
     * @param string $username - User credential. Format 'credentialType:Username'
     * @param string $password - User password
     * @param $impersonatedUsername - Impersonated user credential. Format 'credentialType:Username'
     * @param string $scope - User scopes. Format 'auth message'
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    static public function authImpersonated($clientId, $username, $password, $impersonatedUsername, $scope, Array $optionalParams = array())
    {
        $credentialParts = self::_getCredentialParts($impersonatedUsername);
        $optionalParams = array_merge(array(
            'for_username' => $credentialParts['username'],
            'for_credential_type' => $credentialParts['credential_type'],
        ), $optionalParams);
        return self::auth($clientId, $username, $password, $scope, $optionalParams);
    }

    static public function getUserByAccessToken($clientId, $accessToken)
    {
        $user = new User($clientId);
        $user->setSettings(array(
            'access_token' => $accessToken
        ));
        return $user;
    }

    /**
     * Refreshing user token using refresh_token
     *
     * @param string $refreshToken - Refresh token for user auth
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    private function _authRefresh($refreshToken, Array $optionalParams = array())
    {
        $params = array_merge(array(
            'grant_type' => 'refresh_token',
            'refresh_token' => $refreshToken
        ), $optionalParams);

        $authURL = PandoraService::locateByClientID($this->_clientId, PandoraService::AUTH, $this->_optEnvironment);
        $request = Request::doPost(array('url' => $authURL . '/authorize', 'tryNum' => 5, 'data' => $params));
        $this->_debug($request);
        $this->setSettings($request->getResult());
        if (!$request->isOK())
        {
            error_log('Cannot refresh user. ' . $request->getRawResult());
        }
        if (!$this->_userCacheKey)
        {
            $this->_userCacheKey = $this->getInfo();
        }
        Cache::getInstance()->set($this->_userCacheKey, $this->getSettings() /* value */, array('expirationTime' => '4 hours', 'type' => 'file'));
        return (bool)$this->getSettings();
    }

    /**
     * Link user with other user
     *
     * @param string $credential - User credential for linking. Format is 'credentialType:UserName'
     * @param string $password - Linked User password
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    public function linkCredential($credential, $password, Array $optionalParams = array())
    {
        $params = array_merge(array(
            'credential' => $credential,
            'password' => $password,
            'if_exists' => 'error', // default
        ), $optionalParams);

        // POST /users/me/credentials
        // credential:	The identifier for the login that will be added to the current account
        // password:	The secret for the user in whose name the client will be accessing services (not required for gamecenter credentials)
        // access_token:	access token for the “auth” scope, representing the current account
        // if_exists:	one of “error”, “relink” or “relink_all” for what to do if the credential is already linked for this game
        return $this->_queryAPI(array(
            'service' => PandoraService::AUTH,
            'url' => '/users/me/credentials',
            'method' => 'post',
            'errorMessage' => 'Cannot link credential ' . $credential . ' to user.',
        ), $params);
    }

    /**
     * Unlink user with other user
     *
     * @param string $credential - User credential for unlinking. Format is 'credentialType:UserName'
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    public function unlinkCredential($credential, Array $optionalParams = array())
    {
        $params = array_merge(array(
            'credential' => self::_getCredentialEncoded($credential),
        ), $optionalParams);

        // POST /users/me/credentials/<credential>/unlink
        // credential:	the credential that should be removed from the account
        // access_token:	access token for the scope “auth”
        return $this->_queryAPI(array(
            'service' => PandoraService::AUTH,
            'url' => '/users/me/credentials/' . $params['credential'] . '/unlink',
            'method' => 'post',
            'errorMessage' => 'Cannot link credential ' . $credential . ' to user.',
        ), $params);
    }

    /**
     * Get user information
     *
     * @param string $credential - User credential for getting info. Format is 'credentialType:UserName' or 'me'. If empty then used "me"
     * @return mixed - false / array User info
     */
    public function getInfo($credential = '')
    {
        $credential = $credential ? self::_getCredentialEncoded($credential) : 'me';

        // GET /users/<credential>
        // access_token:	access token for the “auth” scope. If credential is not “me”, the scope “auth_admin_ro” is required
        return $this->_queryAPI(array(
            'service' => PandoraService::AUTH,
            'url' => '/users/' . $credential,
            'errorMessage' => 'Cannot get user info for ' . $credential . '.',
        ));
    }

    /**
     * Create alias
     *
     * @return mixed - array User alias
     */
    function createAlias()
    {
        //POST /games/mygame/alias/
        //access_token:	access token representing the current account containing “auth” scope
        //alias:	the user’s alias you want to retrieve
        return $this->_queryAPI(array(
            'service' => PandoraService::AUTH,
            'url' => '/games/mygame/alias',
            'method' => 'post',
            'mustReturn' => true,
            'errorMessage' => 'Cannot create alias for user.',
        ));
    }

    /**
     * Lookup alias
     *
     * @param $alias - The user’s alias you want to retrieve
     * @return mixed - false / array User credentials
     */
    function lookupAlias($alias)
    {
        //GET /games/mygame/alias/<alias>
        //access_token:	access token representing the current account containing “auth” scope
        //alias:	the user’s alias you want to retrieve
        return $this->_queryAPI(array(
            'service' => PandoraService::AUTH,
            'url' => '/games/mygame/alias/' . $alias,
            'errorMessage' => 'Cannot get user credential by alias "' . $alias . '".',
        ));
    }

    // messages part
    /**
     * Send message to end user
     *
     * @param string $credential - To user credential
     * @param string $transport - Transport parameter
     * @param array $body - Message body. Should be Array
     * @param array $optionalParams - Optional parameters
     * @return bool Operation result
     */
    public function sendMessage($credential, $transport = 'inbox', Array $body = array(), Array $optionalParams = array())
    {
        $params = array_merge(array(
            'username' => self::_getCredentialEncoded($credential),
            'body' => json_encode($body),
            'type' => 'info', //default
        ), $optionalParams);

        // POST /messages/<sendable_transport>/<credential_type>:<username>/games
        // sendable_transport: 	the transport to use to send the message - all transports but inbox, secure
        // username:	Username for the recipient
        // credential_type:	Type of username of the recipient (eg “facebook”: see Janus documentation “credential types”)
        // access_token:	The access token supplied by the Janus service for “message” scope
        // replace_label:	Replaces any pending messages created with the same replace_label. Optional.
        // delay:	number of seconds to delay before sending message (defaults to 0)
        // payload:	Send this raw payload instead of using args from Base Message. Optional
        return $this->_queryAPI(array(
            'service' => PandoraService::MESSAGE,
            'method' => 'post',
            'url' => '/messages/' . $transport . '/' . $params['username'],
            'errorMessage' => 'Cannot send message to user ' . $credential . '.',
        ), $params);
    }

    /**
     * Get user messages
     *
     * @param string $transport - Transport parameter
     * @param array $optionalParams - Optional parameters
     * @return mixed false / array Messages
     */
    public function getMessages($transport = 'inbox', Array $optionalParams = array())
    {
        // GET /messages/<transport>/me
        // access_token:	The access token supplied by the Janus service
        // delete:	Pass this argument with any value to delete all messages once retrieved
        return $this->_queryAPI(array(
            'service' => PandoraService::MESSAGE,
            'url' => '/messages/'  . $transport .  '/me',
            'errorMessage' => 'Cannot get messages.',
        ), $optionalParams);
    }

    public function deleteMessage($transport = 'inbox', $msgid, Array $optionalParams = array())
    {
        // DELETE /messages/<transport>/me/<msgid>
        // transport:	the transport for the message to be deleted
        // msgid:	The “id” field of the message to be deleted
        // access_token:	The access token supplied by the Janus service
        return $this->_queryAPI(array(
            'service' => PandoraService::MESSAGE,
            'method' => 'delete',
            'url' => '/messages/'  . $transport .  '/me/' . $msgid,
            'errorMessage' => 'Cannot get messages.',
        ), $optionalParams);
    }

    // storage part
    /**
     * Get data from storage
     *
     * @param $key - The key that was used when storing the desired data
     * @param array $optionalParams - Optional parameters
     * @return mixed false / array User profile
     */
    public function getData($key, Array $optionalParams = array())
    {
        $credential = !empty($optionalParams['credential']) ? self::_getCredentialEncoded($optionalParams['credential']) : 'me';
        unset($optionalParams['credential']);

        // GET /data/<credential>/<key>
        // access_token:	The access token supplied by the Janus service
        // key:	The key that was used when storing the desired data
        // credential:	The user whose key should be retrieved (for current user use “me”)
        return $this->_queryAPI(array(
            'service' => PandoraService::STORAGE,
            'url' => '/data/' . $credential . '/' . rawurlencode($key),
            'resultType' => 'raw_data',
            'errorMessage' => 'Cannot get user data.',
        ), $optionalParams);
    }

    /**
     * Put data to storage
     *
     * @param $key - The name that will be used to identify the data on retrieval
     * @param $data - The actual data the client wants to store
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    public function putData($key, $data, Array $optionalParams = array())
    {
        $credential = !empty($optionalParams['credential']) ? self::_getCredentialEncoded($optionalParams['credential']) : 'me';
        unset($optionalParams['credential']);

        $params = array_merge(array(
            'data' => $data,
        ), $optionalParams);

        //POST /data/<credential>/<key>
        //key:	The name that will be used to identify the data on retrieval
        //access_token:	The access token supplied by the Janus service
        //data:	The actual data the client wants to store
        //visibility:	Defaults to “private”. Set to “public” or “public_write” to allow other users to read or write the value
        //credential:	The user whose key should be overwritten (for current user use “me”)
        //expiration:	Number of seconds (integer) until the document expires. (Optional)
        return $this->_queryAPI(array(
            'service' => PandoraService::STORAGE,
            'url' => '/data/' . $credential . '/' . rawurlencode($key),
            'method' => 'post',
            'errorMessage' => 'Cannot put data to user ' . $credential . '.',
        ), $params);
    }

    /**
     * @param $key - The key that was used when storing the desired data
     * @return bool - Operation result
     */
    public function deleteData($key) {

        // POST /data/me/<key>/delete
        // access_token:	The access token supplied by the Janus service
        // key:	The key that was used when storing the desired data
        return $this->_queryAPI(array(
            'service' => PandoraService::STORAGE,
            'url' => '/data/me/' . rawurlencode($key) . '/delete',
            'method' => 'post',
            'errorMessage' => 'Cannot delete user data.',
        ));
    }

    /**
     * Get user profile
     *
     * @param array $optionalParams - Optional parameters
     * @return mixed false / array User profile
     */
    public function getProfile(Array $optionalParams = array())
    {
        $params = $optionalParams;
        $credential = 'me';
        if (!empty($params['credential']))
        {
            $credential = self::_getCredentialEncoded($params['credential']);
            unset($params['credential']);
        }
        $selector = '';
        if (!empty($params['selector']))
        {
            $selector = '/' . $params['selector'];
            unset($params['selector']);
        }

        // GET /profiles/<credential>/myprofile/<selector>
        // access_token:	Access token for scope “storage”
        // credential:	The user whose profile you want to get (for current user use “me”)
        // selector:	the part of the profile to get, see Selectors (optional)
        // include_fields:	a comma-separated list of selectors to include in the response, instead of returning all fields
        return $this->_queryAPI(array(
            'service' => PandoraService::STORAGE,
            'url' => '/profiles/' . $credential . '/myprofile' . $selector,
            'errorMessage' => 'Cannot get user profile.',
        ), $params);
    }

    /**
     * Set user profile data
     *
     * @param array $profileData - Profile data to save
     * @param array $optionalParams - Optional parameters
     * @return bool Operation result
     */
    public function setProfile($profileData, Array $optionalParams = array())
    {
        $params = array_merge(array(
            'object' => json_encode($profileData),
        ), $optionalParams);
        $credential = 'me';
        if (!empty($params['credential']))
        {
            $credential = self::_getCredentialEncoded($params['credential']);
            unset($params['credential']);
        }
        $selector = '';
        if (!empty($params['selector']))
        {
            $selector = '/' . $params['selector'];
            unset($params['selector']);
        }

        // GET /profiles/<credential>/myprofile/<selector>
        // access_token:	Access token for scope “storage”
        // credential:	The user whose profile you want to get (for current user use “me”)
        // selector:	the part of the profile to get, see Selectors (optional)
        // include_fields:	a comma-separated list of selectors to include in the response, instead of returning all fields
        return $this->_queryAPI(array(
            'service' => PandoraService::STORAGE,
            'url' => '/profiles/' . $credential . '/myprofile' . $selector,
            'method' => 'post',
            'errorMessage' => 'Cannot set user profile.',
        ), $params);
    }

    /**
     * @param string $credential - The user whose profile you want to delete (for current user use “me”)
     * @return bool - Operation result
     */
    public function deleteProfile($credential = '') {
        $credential = $credential ? self::_getCredentialEncoded($credential) : 'me';
        // POST /profiles/<credential>/myprofile/delete
        // access_token:	Access token for scope “storage”
        // credential:	The user whose profile you want to delete (for current user use “me”)
        return $this->_queryAPI(array(
            'service' => PandoraService::STORAGE,
            'url' => '/profiles/' . $credential . '/myprofile/delete',
            'method' => 'post',
            'errorMessage' => 'Cannot delete user profile.',
        ));
    }

    // social part
    /**
     * Get user connections
     *
     * @param string $connectionType - Connection type. "friend" as default
     * @param array $optionalParams - Optional parameters
     * @return mixed false / User friends
     */
    public function getConnections($connectionType = 'friend', Array $optionalParams = array())
    {
        $params = array_merge(array(
            'connection_type' => $connectionType,
        ), $optionalParams);

        // GET /accounts/me/connections/<connection_type>
        return $this->_queryAPI(array(
            'service' => PandoraService::SOCIAL,
            'url' => '/accounts/me/connections/' . $connectionType,
            'errorMessage' => 'Cannot get user connections.',
        ), $params);
    }

    /**
     * Add connection to user
     *
     * @param string $connectionType - Connection type. "friend" as default
     * @param $targetCredential - Connection credential
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    public function addConnection($connectionType = 'friend', $targetCredential, Array $optionalParams = array())
    {
        $params = array_merge(array(
            'connection_type' => $connectionType,
            'target_credential' => $targetCredential,
        ), $optionalParams);

        // POST /accounts/me/connections/<connection_type>
        // access_token:	access token identifying the current user and granted for the “social” scope
        // connection_type: 	This is the type of the connection. (Type “friend” is the ONLY game-neutral type for now.)
        // target_credential: 	This is the credential that identifies the user we want to connect with
        // requester_credential: 	(optional) credential linked to the user that will be displayed in the friend request.
        // required_approval: 	(Optional) If False, the connection will automatically been made. In this case the scope required_approval is needed. Default to True.
        // alert_kairos:	(Optional) If True, an alert of type “connection_request” will be sent to kairos. False by default.
        return $this->_queryAPI(array(
            'service' => PandoraService::SOCIAL,
            'url' => '/accounts/me/connections/' . $connectionType,
            'method' => 'post',
            'errorMessage' => 'Cannot add ' . $targetCredential .' as connection to user.',
        ), $params);
    }

    /**
     * Remove user connection
     *
     * @param string $connectionType - Connection type. "friend" as default
     * @param $targetCredential - Connection credential
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    public function removeConnection($connectionType = 'friend', $targetCredential, Array $optionalParams = array())
    {
        $params = array_merge(array(
            'target_credential' => self::_getCredentialEncoded($targetCredential),
        ), $optionalParams);

        // POST /accounts/me/connections/<connection_type>/<target_credential>/delete
        // access_token:	access token identifying the current user and granted for the “social” scope
        // connection_type:  	This is the type of the connection to be deleted
        // target_credential: 	This is the credential that identifies the user whose connection is to be deleted
        // This command will remove an existing connection of type connection_type between the current user and the user identified by target_credential.
        return $this->_queryAPI(array(
            'service' => PandoraService::SOCIAL,
            'url' => '/accounts/me/connections/' . $connectionType . '/' . $params['target_credential'] . '/delete',
            'method' => 'post',
            'errorMessage' => 'Cannot remove connection ' . $targetCredential .' from user.',
        ), $params);
    }

    /**
     * @param string $credential - the credential of the user you want to display. This user has to be link to the owner of the access_token
     * @return array|bool|mixed - Operation result
     */
    public function getSocialProfile($credential = '') {
        $credential = $credential ? self::_getCredentialEncoded($credential) : 'me';

        // GET /accounts/<credential>
        // access_token:	access token identifying the current user and granted for the “social” scope
        // credential:	the credential of the user you want to display. This user has to be link to the owner of the access_token
        // Display the user profile. You can only display profile from ‘me’ or a user connected to ‘me’. Returns a json string of the user.
        return $this->_queryAPI(array(
            'service' => PandoraService::SOCIAL,
            'url' => '/accounts/' . $credential,
            'errorMessage' => 'Cannot get social profile for user ' . $credential . '.',
        ));
    }

    // leaderboard part
    /**
     * Create League Leaderboard
     *
     */
    public function createLeagueLeaderboard($sort = 'desc', $leaderboardName, $groupSize = 100, $expirationDuration, Array $optionalParams = array())
    {
        $params = array_merge(array(
            'type' => 'league',
            'group_size' => $groupSize,
            'expiration_duration' => $expirationDuration,
        ), $optionalParams);
        // POST /leaderboards/<sort>/<name>/settings
        // access_token:	access token granting scope “leaderboard_admin”
        // name:	The identifier for the leaderboard
        // sort:	The sort order used for this leaderboard (either “desc” or “asc”)
        // type:	The type of leaderboard. Must be “league”
        // group_size:	The maximum number of users that can be in one division
        // expiration_duration:	How long in seconds the leagues will live before they expire. Cannot exceed 4 months.
        // first_expire:	The date the user wants the leaderboard to expire the first time. Optional (defaults to now + expiration_duration)
        // leaderboard_type:	Optional, can only have the value: “restricted”, and you need an access_token with scope: leaderboard_restricted.

        return $this->_queryAPI(array(
            'service' => PandoraService::LEADERBOARD,
            'url' => '/leaderboards/' . $sort . '/' . rawurlencode($leaderboardName) . '/settings',
            'method' => 'post',
            'errorMessage' => 'Cannot create league leaderboard.',
        ), $params);
    }

    /**
     * Get leaderboard entries. Get the top entries by default.
     *
     * @param string $sort - Sort order for this leaderboard
     * @param $leaderboardName - The identifier for this leaderboard, unique within each game
     * @param array $optionalParams - Optional parameters
     * @return mixed false / Leaderboard array
     */
    public function getLeaderboard($sort = 'desc', $leaderboardName, Array $optionalParams = array())
     {
         $errorMessage = 'Cannot get leaderboard.'; //default
         if (!empty($optionalParams['error_message']))
         {
             $errorMessage = $optionalParams['error_message'];
             unset($optionalParams['error_message']);
         }

         $fullLeaderboardURL = '/leaderboards/' . $sort . '/' . rawurlencode($leaderboardName);
         if (isset($optionalParams['credential']))
         {
             $credential = $optionalParams['credential'] ? self::_getCredentialEncoded($optionalParams['credential']) : 'me';
             $fullLeaderboardURL .= '/' . $credential;
             unset($optionalParams['credential']);
         }
         elseif (isset($optionalParams['friends']))
         {
             $fullLeaderboardURL .= '/me/friends';
             unset($optionalParams['friends']);
         }

         //GET /leaderboards/<sort>/<name> | GET /leaderboards/<sort>/<name>/<credential>
         //sort:	The sort order used for this leaderboard (either “desc” or “asc”)
         //name:	The identifier for this leaderboard, unique within each game
         //access_token:	The previously obtained access token serving to authenticate the client and user, must contain “leaderboard” or “leaderboard_ro” scope
         //offset:	The first entry index to retrieve, defaults to 0, maximum 1000.
         //limit:	The maximum number of entries that the service should return. Defaults to a reasonable number
         //tiebreak:	Default to True. If False, several users with the same score will have the same rank. This option is very expensive, you have to use it if your creating a game with a very low score cardinality.
         //connection_type: 	The connection type you want to get from Osiris. (Optional, defaults to game neutral “friend”)
         return $this->_queryAPI(array(
             'service' => PandoraService::LEADERBOARD,
             'url' => $fullLeaderboardURL,
             'errorMessage' => $errorMessage,
         ), $optionalParams);
     }

    /**
     * Get entries around a user
     *
     * @param string $sort - Sort order for this leaderboard
     * @param $leaderboardName - The identifier for this leaderboard, unique within each game
     * @param array $optionalParams - Optional parameters
     * @return mixed false / Leaderboard array
     */
    public function getLeaderboardAroundUser($sort = 'desc', $leaderboardName, Array $optionalParams = array())
     {
         $optionalParams['error_message'] = 'Cannot get leaderboard for user ' . $optionalParams['credential'] . '.';
         return $this->getLeaderboard($sort, $leaderboardName, $optionalParams);
     }

    /**
     * Retrieve Friend Leaderboard
     *
     * @param string $sort - Sort order for this leaderboard
     * @param $leaderboardName - The identifier for this leaderboard, unique within each game
     * @param array $optionalParams - Optional parameters
     * @return mixed false / Leaderboard array
     */
     public function getLeaderboardFriend($sort = 'desc', $leaderboardName, Array $optionalParams = array())
     {
         $optionalParams['friends'] = 1;
         $optionalParams['error_message'] = 'Cannot get friend leaderboard.';
         return $this->getLeaderboard($sort, $leaderboardName, $optionalParams);
     }

    /**
     * Post new Leaderboard entry
     *
     * @param string $sort - Sort order for this leaderboard
     * @param $leaderboardName - The identifier for this leaderboard, unique within each game
     * @param $displayName - The display name for the leaderboard entry (probably a username)
     * @param int $score - The score for the new leaderboard entry
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    public function postLeaderboardEntry($sort = 'desc', $leaderboardName, $displayName, $score = 0, Array $optionalParams = array())
    {
        $params = array_merge(array(
            'display_name' => $displayName,
            'score' => $score
        ), $optionalParams);

        //POST /leaderboards/<sort>/<name>/me
        //sort:	The sort order to be used for this leaderboard (either “desc” or “asc”)
        //name:	The identifier for the specific leaderboard to add an entry to
        //access_token:	The previously obtained access token serving to authenticate the client and user, must contain scope “leaderboard”
        //score:	The score for the new leaderboard entry
        //display_name:	The display name for the leaderboard entry (probably a username)
        //replace_score_if: 	Set this to “lower” or “higher” to guard against replacing an entry erroneously. Optional
        //expiration_date: 	The expiration date of the entry in iso format (Y-m-d H:M:Sz). Optional. Either expiration date or expiration duration should be provided but not both
        //expiration_duration: 	The expiration time of the date (amount of second before the entry expire). Optional. Either expiration date or expiration duration should be provided but not both
        //<custom_attrs>:	Any other argument is considered a custom attribute that is added to the leaderboard entry
        return $this->_queryAPI(array(
            'service' => PandoraService::LEADERBOARD,
            'url' => '/leaderboards/' . $sort . '/' . rawurlencode($leaderboardName) . '/me',
            'method' => 'post',
            'errorMessage' => 'Cannot post leaderboard entry.',
        ), $params);
    }

    /**
     * Delete Leaderboard entry
     *
     * @param string $sort - Sort order for this leaderboard
     * @param $leaderboardName - The identifier for this leaderboard, unique within each game
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    public function deleteLeaderboardEntry($sort = 'desc', $leaderboardName, Array $optionalParams = array())
    {
        //$leaderboardURL = PandoraService::locateByClientID($this->_clientId, PandoraService::LEADERBOARD, $this->_optEnvironment);
        //POST /leaderboards/<sort>/<name>/me/delete
        //sort:	The sort order to be used for this leaderboard (either “desc” or “asc”)
        //name:	The identifier for the specific leaderboard to add an entry to
        //access_token:	The previously obtained access token serving to authenticate the client and user, must contain scope “leaderboard”
        return $this->_queryAPI(array(
            'service' => PandoraService::LEADERBOARD,
            'url' => '/leaderboards/' . $sort . '/' . rawurlencode($leaderboardName) . '/me/delete',
            'method' => 'post',
            'errorMessage' => 'Cannot delete leaderboard entry.',
        ), $optionalParams);
    }

    public function clearLeaderboard($sort = 'desc', $leaderboardName)
    {
        // POST /leaderboards/desc/<name>/clear
        // name:	The identifier for the specific leaderboard to add an entry to
        // access_token:	access token which must contain scope “leaderboard_clear”
        return $this->_queryAPI(array(
            'service' => PandoraService::LEADERBOARD,
            'url' => '/leaderboards/' . $sort . '/' . rawurlencode($leaderboardName) . '/clear',
            'method' => 'post',
            'errorMessage' => 'Cannot clear leaderboard.',
        ));
    }

    // scheduling
    /**
     * Schedule running callback script on the Game Portal
     *
     * @param $key - The name under which to store the scheduler for the current user
     * @param array $callback - The Array callback object
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    function schedule($key, Array $callback = array(), Array $optionalParams = array())
    {
        $params = array_merge(array(
            'refresh_token' => $this->getSetting('refresh_token'),
            'callback' => json_encode($callback),
        ), $optionalParams);
        /*
        POST /schedules/me/<key>
        key:	The name under which to store the scheduler for the current user
        access_token:	The access token supplied by the Janus service for the “schedule” scope, and any scopes required for the callback.
        refresh_token:	A refresh token supplied by Janus containing the scopes required to run the callback
        callback:	The JSON callback object (see Callback Objects for details)
        callback_name:	The name of a previously added named callback (see Add Named Callback)
        callback_credential::	The credential associated with the named callback (optional)
        callback_gamespace::	The gamespace associated with the named callback (optional)
        start_date:	The date of the first callback to be called (format “2012-12-12 12:12:12Z”). Defaults to current time
        interval:	The time, in seconds, between each callback if run_limit is greater than 1.
        run_limit:	The number of times to call back, defaults to “forever”
        contact_type:	The contact method used to notify when a task fails. Optional (currently the only accepted value is “email”).
        contact_address::	The contact address to which a task failure notification should be sent. Optional.
         */
        return $this->_queryAPI(array(
            'service' => PandoraService::SCHEDULE,
            'url' => '/schedules/me/' . rawurlencode($key),
            'method' => 'post',
            'errorMessage' => 'Unable to schedule',
        ), $params);
    }

    // assets part
    public function getAsset($assetName)
    {
        // GET /assets/<client_id>/<asset_name>
        // client_id:	The official client ID for the game client (see Janus and Federation docs for more details)
        // asset_name:	This is the well-known name for the asset to be downloaded (this must have been uploaded previously)
        return $this->_queryAPI(array(
            'service' => PandoraService::ASSET,
            'url' => '/assets/' . rawurlencode($this->_clientId) . '/' . rawurlencode($assetName),
            'useAccessToken' => false,
            'resultType' => 'raw_data',
            'errorMessage' => 'Unable to get asset with name `' . $assetName . '`',
        ));
    }

    // private auxiliary methods
    /**
     * Encode credential to be safe in URLs
     *
     * @param string $credential
     * @return string - Encoded credential
     */
    private static function _getCredentialEncoded($credential = '')
    {
        $credentialParts = self::_getCredentialParts($credential);
        return $credentialParts['credential_type'] . ':' . rawurlencode($credentialParts['username']);
    }

    /**
     * Get credential parts
     *
     * @param string $credential
     * @return array - Credential parts (credential_type, username)
     */
    private static function _getCredentialParts($credential = '')
    {
        $credentialArray = explode(':', $credential);
        $credentialType = array_shift($credentialArray);
        return array('credential_type' => $credentialType, 'username' => implode(':', $credentialArray));
    }

    /**
     * User auth
     *
     * @param string $username - User credential. Format 'credentialType:Username'
     * @param string $password - User password
     * @param string $scope - User scopes. Format 'auth message'
     * @param array $optionalParams - Optional parameters
     * @return bool - Operation result
     */
    private function _auth($username, $password, $scope, Array $optionalParams = array())
    {
        $params = array_merge(array(
            'username' => $username,
            'scope' => $scope,
            'password' => $password,
            'client_id' => $this->_clientId,
        ), $optionalParams);
        $this->_userCacheKey = $params;

        $this->setSettings(Cache::getInstance()->get($this->_userCacheKey, array('type' => 'file', 'returnExpired' => true)));
        if (!$this->getSettings())
        {
            // not cached
            $authURL = PandoraService::locateByClientID($this->_clientId, PandoraService::AUTH, $this->_optEnvironment);
            if (!$authURL)
            {
                $this->_error = self::CLIENTID_NOT_FOUND;
                return false;
            }
            $request = Request::doGet(array('url' => $authURL . '/authorize', 'tryNum' => 5, 'data' => $params));
            $this->_debug($request);
            if (!$request->isOK())
            {
                error_log('Cannot authorize user "' . $username .'". '. $request->getRawResult());
                $this->_error = self::INCORRECT_CREDENTIALS;
                return false;
            }
            $this->setSettings($request->getResult());
            Cache::getInstance()->set($this->_userCacheKey, $this->getSettings() /* value */, array('expirationTime' => '4 hours', 'type' => 'file'));
        }
        elseif ($this->getSettings() && Cache::getInstance()->isKeyExpired($this->_userCacheKey))
        {
            // cached but expired
            $this->_authRefresh($this->getSetting('refresh_token'));
        }
        return (bool)$this->getSettings();
    }

    public function getPandoraServiceURL($service)
    {
        return PandoraService::locateByClientID($this->_clientId, $service, $this->_optEnvironment);
    }

    /**
     * Heart of the user class. Unified method for get/post/delete queries. Should be used only for simple queries.
     *
     * @param array $params - Parameters for creating API query
     * @param array $queryParams - Parameters which will be passed for API query
     * @return array|bool|mixed - Boolean operation result for non-get queries. False for failure in all cases. Array/Mixed data for get queries.
     */
    private function _queryAPI(Array $params, Array $queryParams = array())
    {
        $this->_increaseRequestNumber();

        $params = array_merge(array(
            'method' => 'get',              // default
            'resultType' => 'json_decoded', // default
            'mustReturn' => false,          // default
            'useAccessToken' => true,       // default
        ), $params);

        if ($params['useAccessToken'])
        {
            $queryParams = array_merge(array(
                'access_token' => $this->getAccessToken(),
            ), $queryParams);
        }

        $serviceURL = $this->getPandoraServiceURL($params['service']) . $params['url'];

        $requestMethod = 'doGet';
        if ($params['method'] == 'post')
        {
            $requestMethod = 'doPost';
        }
        if ($params['method'] == 'delete')
        {
            $requestMethod = 'doDelete';
        }
        $request = Request::$requestMethod(array('url' => $serviceURL, 'data' => $queryParams));
        $this->_debug($request);
        if ($request && !$request->isOK())
        {
            $message = $params['errorMessage'] . ' Error message: ' . $request->getRawResult();
            $this->_saveRequestError($message);
            error_log($message);
            return false;
        }
        if ($params['method'] == 'get' || $params['mustReturn']) {
            return $params['resultType'] == 'json_decoded' ? $request->getResult() : $request->getRawResult();
        }
        return true;
    }

    private function _increaseRequestNumber()
    {
        $this->_requestNumber += 1;
    }

    private function _getRequestNumber()
    {
        return $this->_requestNumber;
    }

    private function _saveRequestError($message)
    {
        $requestNumber = $this->_getRequestNumber();
        if (!isset($this->_requestErrors[$requestNumber]))
        {
            $this->_requestErrors[$requestNumber] = $message;
        }
        else
        {
            $this->_requestErrors[$requestNumber] += '\n' . $message;
        }
    }

    private function _debug(Request $request)
    {
        if (is_debugmode()) {
            $request->debug(array(
                'Env' => $this->getEnvironment()
            ));
        }
    }
}
