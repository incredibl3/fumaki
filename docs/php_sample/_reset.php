<?php
/*
 * IAV -> IAA Cross promotional script (Portal Script, RESET User Profile)
 *
 */

require_once('config.php');
require_once('classes/user.php');
require_once('includes/project_common.php');

if (!empty($_GET['debug']))
{
    debugmode($_GET['debug']);
    echo '<b>Version:</b> ' . $GLOBALS['config']['version'] . '<br>';
}

if (!empty($_GET['credential']) && !empty($_GET['deviceId']))
{
    $credential = $_GET['credential'];
    $deviceId = $_GET['deviceId'];
	$client = $GLOBALS['config']['clientId_GP'];
	if ($_GET['space'] == 'iaa_1936_glshop')
	{
		$client = $GLOBALS['config']['clientId_GLSHOP'];
	} else if ($_GET['space'] == 'iaa_1936_amz')
	{
		$client = $GLOBALS['config']['clientId_AMZ'];
	} else if ($_GET['space'] == 'iaa_1936_ssapps')
	{
		$client = $GLOBALS['config']['clientId_SSAPPS'];
	}

    // super user authorisation in the Federation
    $superUser = User::auth(
        $client,
        $GLOBALS['config']['userCredential'],
        $GLOBALS['config']['userPassword'],
        'auth storage storage_admin'
    );

    if ($error = $superUser->getError())
    {
        error_log('UserError: ' . $error);
        return false;
    }

    // obtaining GDID by DeviceId
    $gdid = get_global_id($superUser->getServiceURL('gdid'), $deviceId);
    if (!$gdid) return false;

    // mark User Profile as gift haven't sent
    $superUser->setProfile('', array(
        'credential' => $credential,
        'selector' => $GLOBALS['config']['user_profile_selector']
    ));

    $database_script_url = $superUser->getServiceURL('gllive-ope') . $GLOBALS['config']['database_script_url'];
    // mark database that gift have already sent for the gdid (request database script)
    Request::doGet(array('url' => $database_script_url, 'data' => array(
        'gdid' => $gdid,
        'credential' => $credential,
        'action' => 'reset',
    )));

    error_log('User Profile and DB field were updated for ' . $credential);
}

debugmode(false);