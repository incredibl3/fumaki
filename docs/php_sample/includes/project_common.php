<?php

function m_error_log($environment, $message)
{
    if (!empty($GLOBALS['config']['email_logs_to']))
    {
        error_log($environment . '| ' . $message, 1, $GLOBALS['config']['email_logs_to']);
    }
    else
    {
        error_log($environment . '| ' . $message);
    }
}

function parseDeviceId($deviceId)
{
    parse_str(str_replace(' ', '&', $deviceId), $deviceIdParts);
    if (!is_array($deviceIdParts)) $deviceIdParts = array();
    return $deviceIdParts;
}

function get_global_id($gdidServiceURL, $deviceId)
{
    $params = array_merge(array(
        'source'=> 'CrossPromo_0.0.1',
    ), parseDeviceId($deviceId));
    $request = Request::doGet(array('url' => $gdidServiceURL . '/get_global_id/', 'data' => $params));
    if ($request && !$request->isOK())
    {
        m_error_log($env, 'Cannot obtain GDID for DeviceId "' . $deviceId . '". HTTP ' . $request->getHTTPCode());
        return false;
    }
    return $request->getRawResult();
}