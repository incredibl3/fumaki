<?php

/*
 *  This file include some helpful functions which is really need for the project.
 */

$GLOBALS['config_default'] = array(
    'caching_path' => dirname(__FILE__) . '/../cache/',
    'file_caching_enabled' => true,
);

function config_prepare()
{
    if (empty($GLOBALS['config'])) $GLOBALS['config'] = array();
    $GLOBALS['config'] = array_merge($GLOBALS['config_default'], $GLOBALS['config']);
}

function array_get_column_key(Array $array, $key)
{
    $result = array();
    foreach ($array as $element)
    {
        $result[] = $element[$key];
    }
    return $result;
}

function debug_log($data)
{
    ob_start();
    var_dump($data);
    $message = ob_get_contents();
    ob_end_clean();
    error_log(str_replace("\n", '', $message));
}

function debugmode($mode = 'query')
{
    $GLOBALS['debugmode'] = $mode;
}

function is_debugmode($mode = 'query')
{
    return (!empty($GLOBALS['debugmode']) && ($GLOBALS['debugmode'] == $mode));
}

function var_dump_pre($param)
{
    echo '<pre>';
    var_dump($param);
    echo '</pre>';
}


config_prepare();