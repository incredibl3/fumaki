<?php
require_once('config.php');
require_once('classes/user.php');
require_once('includes/project_common.php');

// super user authorisation in the Federation
$superUser = User::auth(
    $GLOBALS['config']['clientId_SSAPPS'],
    $GLOBALS['config']['userCredential'],
    $GLOBALS['config']['userPassword'],
    'auth storage storage_ro storage_admin'
);

if (!empty($_GET['action']) && $_GET['action'] == 'get_gdid') {
    // reading user profile to find his/her current_device_id
    $userProfile = $superUser->getProfile(array('credential' => $_GET['credential']));
    if (!empty($userProfile['current_device_id']))
    {
        // obtaining GDID by DeviceId
        $gdid = get_global_id($superUser->getServiceURL('gdid'), $userProfile['current_device_id']);
        if ($gdid) echo $gdid;
    }
    exit();
}

function getGiftPackSelect($id)
{
    return '<select id="' . $id . '" class="gift-pack" name="gift_pack">'.
               '<option value="NPAU">NonPAU (Free)</option>'.
               '<option value="Bronze">Bronze (PAUs)</option>'.
               '<option value="Silver">Silver (Whales)</option>'.
               '<option value="Gold">Gold (Blue Whales)</option>'.
           '</select>';
}

function getShopSelect($id)
{
    return '<select id="' . $id . '" class="shop" name="shop">'.
    '<option value="GPlay">Google Play (Android)</option>'.
    '<option value="iOS">Apple Store (iOS)</option>'.
    '<option value="Amz">Amz</option>'.
    '<option value="GLS">GLS</option>'.
    '<option value="Sams">Sams</option>'.
    '<option value="BnN">BnN</option>'.
    '<option value="Cyrus">Cyrus</option>'.
    '<option value="KDDI">KDDI</option>'.
    '<option value="SKT">SKT</option>'.
    '<option value="Win8">Windows 8</option>'.
    '</select>';
}

$upload_script_url = $superUser->getServiceURL('gllive-ope') . $GLOBALS['config']['upload_script_url'];
//$upload_script_url = 'http://localhost/gift_sender/gllive/1725/data_upload.php';

$title = $superUser->getEnvironment() . ' | Cross Promo Data Upload Tool';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title><?php echo $title; ?></title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/uploadfile.min.css" rel="stylesheet">
    <style type="text/css">
        * { font-size: 16px; }
        div.error, div.message { width: 830px; padding: 10px; margin: 20px auto; color: #090; outline: #090 1px solid; }
        div.error { color: #900; outline: #900 1px solid; }
        .form { max-width: 860px; padding: 19px 29px 29px; margin: 20px auto; background-color: #fff; border: 1px solid #e5e5e5; border-radius: 5px; box-shadow: 0 1px 2px rgba(0,0,0,.05); }
        form input, form select { font-size: 16px; height: auto; margin-bottom: 0 !important; }
        #credential { width: 235px; }
        .btn { width: 140px; }
        button.btn { float: right; }
        .gift-pack, .shop { float: right; margin-right: 10px; }
        h2 { font-size: 20px; line-height: 20px; }
        .ajax-upload-dragdrop { padding: 10px; width: 400px !important; }
        .ajax-file-upload-statusbar { width: auto !important; border: 1px #ccc solid; }
        #download-iframe { display: none; }
    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bootstrap/js/jquery.uploadfile.min.js"></script>
    <script type="text/javascript">

        function showJSMessage(is_error, message) {
            $("#js-error").html(message).addClass(is_error ? "error" : "message").removeClass(is_error ? "message" : "error").show();
        }

        function getGDIDAndSubmit() {
            $('#add-credential-btn').attr("disabled", "disabled");
            $.ajax({
                url: "data_upload_ssapps.php",
                data: {
                    'credential': $('#credential').val(),
                    'action': 'get_gdid'
                },
                success: function( data ) {
                    if (data) {
                        var gdid = data;
                        $.ajax({
                            url: "<?php echo $upload_script_url; ?>",
                            type: "POST",
                            dataType: "json",
                            data: {
                                'action': "add_credential",
                                'gdid': gdid,
                                'gift_pack': $("#gift-pack-qa").val(),
                                'shop': $("#shop-qa").val()
                            },
                            success:function(data)
                            {
                                $('#add-credential-btn').removeAttr("disabled");
                                showJSMessage(data.is_error, data.message);
                            }
                        });

                    } else {
                        showJSMessage(true, 'Cannot obtain gdid for the credential');
                        $('#add-credential-btn').removeAttr("disabled");
                    }
                }
            });
        }

        var manual_upload;
        $(document).ready(function() {
            manual_upload = $("#fileuploader-manual").uploadFile({
                url: "<?php echo $upload_script_url; ?>",
                fileName: "list",
                multiple: false,
                dragDrop: false,
                showProgress: true,
                uploadButtonClass: "btn",
                autoSubmit: false,
                allowedTypes: "csv",
                method: "post",
                returnType: "json",
                dynamicFormData: function()
                {
                    var data = {
                        "action": "upload_manual",
                        "gift_pack": $("#gift-pack-manual").val(),
                        "shop": $("#shop-manual").val()
                    }
                    return data;
                },
                onSuccess:function(files, data, xhr)
                {
                    showJSMessage(data.is_error, data.message);
                }
            });
        });

        function downloadList() {
            $('#download-iframe').attr('src', '<?php echo $upload_script_url; ?>?action=download&type='+$("#type-download").val()+'&gift_pack='+$("#gift-pack-download").val()+'&shop='+$("#shop-download").val());
        }
    </script>
</head>
<body>
    <h2 style="text-align: center"><?php echo $title; ?></h2>

    <div class="form">
        <h2>Upload list of UDIDs (manual category selection)</h2>
        <button class="btn btn-success" type="button" onclick="$('#js-error').hide(); manual_upload.startUpload();">Upload list</button>
        <?php echo getShopSelect('shop-manual'); ?>
        <?php echo getGiftPackSelect('gift-pack-manual'); ?>
        <div id="fileuploader-manual">Choose file</div>
        <div style="clear: both"></div>
    </div>

    <div class="form">
        <h2>Add one credential (for QA tests only)</h2>
        <button id="add-credential-btn" class="btn btn-primary" type="button" onclick="$('#js-error').hide(); getGDIDAndSubmit()">Add credential</button>
        <input type="text" id="credential" name="credential" value="" placeholder="Credential">
        <?php echo getShopSelect('shop-qa'); ?>
        <?php echo getGiftPackSelect('gift-pack-qa'); ?>
        <div style="clear: both"></div>
    </div>

    <div class="form">
        <h2>Download list of GDIDs</h2>
        <button id="download-list-btn" class="btn btn-info" type="button" onclick="downloadList()">Download</button>
        <?php echo getShopSelect('shop-download'); ?>
        <?php echo getGiftPackSelect('gift-pack-download'); ?>
        <select id="type-download">
            <option value="not-awarded">Not awarded</option>
            <option value="awarded">Awarded</option>
        </select>
        <iframe id="download-iframe" scr=""></iframe>
        <div style="clear: both"></div>
    </div>

    <div id="js-error" style="display: none"></div>
</body>
</html>