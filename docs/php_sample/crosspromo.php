<?php
/*
 * IAV -> IAA Cross promotional script (Portal Script)
 *
 */

require_once('config.php');
require_once('classes/user.php');
require_once('includes/project_common.php');

function sendCrossPromoGift($credential, $deviceId, $space)
{
    // super user authorisation in the Federation
	$client = $GLOBALS['config']['clientId_GP'];
	if ($space == 'iaa_1936_amz')
	{
		$client = $GLOBALS['config']['clientId_AMZ'];
	}
	else if ($space == 'iaa_1936_glshop')
	{
		$client = $GLOBALS['config']['clientId_GLSHOP'];
	}
	else if ($space == 'iaa_1936_ssapps')
	{
		$client = $GLOBALS['config']['clientId_SSAPPS'];
	}
			
    $superUser = User::auth(
        $client,
        $GLOBALS['config']['userCredential'],
        $GLOBALS['config']['userPassword'],
        'auth storage storage_admin message_secured'
    );
    $env = $superUser->getEnvironment();

    if ($error = $superUser->getError())
    {
        m_error_log($env, 'UserError: ' . $error);
        return false;
    }

    // check User Profile if CCgift has already been sent
    $userProfile = $superUser->getProfile(array('credential' => $credential));
    if (!empty($userProfile[$GLOBALS['config']['user_profile_selector']]))
    {
        m_error_log($env, 'UserProfile: CCGift had already been requested for user `' . $credential . '`. The status is `' . $userProfile[$GLOBALS['config']['user_profile_selector']] . '`');
        return false;
    }

    // if cross promo disabled
    if ($GLOBALS['config']['disabled'])
    {
        // mark User Profile as gift already sent
        $superUser->setProfile('disabled', array(
            'credential' => $credential,
            'selector' => $GLOBALS['config']['user_profile_selector']
        ));
        m_error_log($env, 'UserProfile: Cross Promo disabled for user `' . $credential . '`.');
        return false;
    }

    // obtaining GDID by DeviceId
    $gdid = get_global_id($superUser->getServiceURL('gdid'), $deviceId);
    if (!$gdid) return false;

    $database_script_url = $superUser->getServiceURL('gllive-ope') . $GLOBALS['config']['database_script_url'];

    // checking if we need to send CCGift for the gdid (request database script)
    $request = Request::doGet(array('url' => $database_script_url, 'data' => array(
        'gdid' => $gdid,
        'action' => 'check',
    )));
    if ($request && !$request->isOK())
    {
        // mark User Profile as user not eligible to get a gift
        $superUser->setProfile('not_eligible', array(
            'credential' => $credential,
            'selector' => $GLOBALS['config']['user_profile_selector']
        ));

        if ($request->getHTTPCode() == 409)
        {
            $result = $request->getResult();
            m_error_log($env, 'Database: CCGift had already been sent for credential "' . $result['was_sent_for_user']);
        }
        else
        {
            m_error_log($env, 'Database: GDID "'. $gdid .'" not found in the list');
        }
        return false;
    }

    $giftInfo = $request->getResult();
    if (!empty($giftInfo['gift_type']))
    {
        // send customer care gift
        $superUser->sendMessage($credential, 'secured',
            array(
                'gifts' => array(
                    array(
                        'name' => $giftInfo['gift_type'],
                        'value' => $giftInfo['gift_value'],
                    ),
                ),
            ),
            array(
                'gift_type' => 'customer_care',
                'type' => 'gift',
            )
        );

        // mark User Profile as gift already sent
        $superUser->setProfile('gift_sent', array(
            'credential' => $credential,
            'selector' => $GLOBALS['config']['user_profile_selector']
        ));

        // mark database that gift have already sent for the gdid (request database script)
        Request::doGet(array('url' => $database_script_url, 'data' => array(
            'gdid' => $gdid,
            'credential' => $credential,
            'action' => 'mark',
        )));
        m_error_log($env, 'CCGift was successfully sent for user "' . $credential . '"');
        return true;
    }
}

// ------------------------------------------------ MAIN script --------------------------------------------------------
if (!empty($_POST['deviceId']) && !empty($_POST['credential']))
{
    if (!empty($_GET['debug']))
    {
        debugmode($_GET['debug']);
        echo '<b>Version:</b> ' . $GLOBALS['config']['version'] . '<br>';
    }
    $result = sendCrossPromoGift($_POST['credential'], $_POST['deviceId'], $_POST['space']);
    if ($result)
    {
        echo 'OK';
        exit();
    }
    debugmode(false);
}
header("HTTP/1.0 404 Not Found");
echo 'Not found';
