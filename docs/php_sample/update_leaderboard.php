<?php
/*
 * Leaderboard Updater
 *
 */

require_once('leaderboard_config.php');
require_once('classes/user.php');
require_once('includes/project_common.php');

function checkParams($paramsKeys = array(), $params = array(), &$error = '')
{
    foreach ($paramsKeys as $key)
    {
        if (empty($params[$key]))
        {
            $error = '`' . $key . '` param missed';
            return false;
        }
    }
    return true;
}

function getEventType($params)
{
    if (strpos($params['challengeType'], 'daily') !== false)
    {
        $eventType = 'daily';
    } elseif (strpos($params['challengeType'], 'weekly') !== false)
    {
        $eventType = 'weekly';
    }
    else
    {
        $eventType = 'friendrunner';
    }
    return $eventType;
}

function getConfigValue($key, $configData, $params)
{
    $eventType = getEventType($params);
    if (empty($configData[$eventType][$key]))
    {
        return false;
    }
    return $configData[$eventType][$key];
}

function checkChallengeType($configData, $params)
{
    $configValue = getConfigValue('challenge_types', $configData, $params);
    return !empty($configValue[$params['challengeType']]);
}

function validateData($configData, $params)
{
    $configValue = getConfigValue('challenge_types', $configData, $params);
    if (empty($configValue[$params['challengeType']]['max_score']) || ($params['score'] > $configValue[$params['challengeType']]['max_score']))
    {
        return false;
    }
    return true;
}


/*

Daily:
 Clear Obstacles  129,000
 Berries Collected 516,000
 Red Rocks Destroyed 4,607
 Animals Fed 272
 Blue Clamshells Opened 12,900
 Tokens Used 50

Weekly:
 Berries Collected 200,000
 Points (cumulated) 20,000,000

*/


function processLeaderboardUpdating($params = array())
{

    // user authorisation in the Federation
    /*$user = User::authImpersonated(
        $GLOBALS['config']['clientId'],
        $GLOBALS['config']['userCredential'],
        $GLOBALS['config']['userPassword'],
        'iphone:test_user_0',
        'auth leaderboard'
    );
    var_dump($user->getAccessToken());
    var_dump($user->getPandoraServiceURL('leaderboard'));
    die();*/

    $params = array_merge(array(
        'leaderboardSort' => 'desc',
        'expirationDuration' => '86400',
    ), $params);

    // get already authorized user
    $user = User::getUserByAccessToken(
        $GLOBALS['config']['clientId'],
        $params['accessToken']
    );

    $env = 'mdc:beta'; //$user->getEnvironment();
    if ($error = $user->getError())
    {
        m_error_log($env, 'UserError: ' . $error);
        return array('status' => 'federation_error', 'message' => $error);
    }

    // read config file
    $configData = file_get_contents($GLOBALS['config']['leaderboard_config_filename']);
    if (!$configData)
    {
        $message = 'Cannot get config file `' . $GLOBALS['config']['leaderboard_config_filename'] . '`';
        m_error_log($env, $message);
        return array('status' => 'error', 'message' => $message);
    }
    $configData = json_decode($configData, true);

    // challenge type checking
    if (!checkChallengeType($configData, $params))
    {
        m_error_log($env, 'Post blocked, value too high');
        return array('status' => 'error', 'message' => 'Challenge type `'.$params['challengeType'].'` is not found');
    }

    // user score validation
    if (!validateData($configData, $params))
    {
        m_error_log($env, 'Post blocked, value too high');
        return array('status' => 'post_blocked');
    }

    if (!empty($params['leaderboardURL']))
    {
        // put provided service URL to cache to avoid unnecessary requests
        PandoraService::cacheServiceURL($params['leaderboardURL'], array(
            'clientId' => $GLOBALS['config']['clientId'],
            'service' => 'leaderboard',
        ));
    }

    // post entry to the leaderboard
    if (!$user->postLeaderboardEntry(
        $params['leaderboardSort'],
        $params['leaderboardName'],
        $params['displayName'],
        $params['score'],
        array(
            'expiration_duration' => $params['expirationDuration'],
            'replace_score_if' => 'higher'
        )
    ))
    {
        m_error_log($env, $user->getLastQueryError());
        return array('status' => 'federation_error', 'message' => $user->getLastQueryError());
    }
    return array('status' => 'success');
}

/*

$_POST['accessToken'] = '14e49f8c-0b17-11e4-bed5-b8ca3a636820,auth leaderboard,1725:53618:0.1.4:ios:appstore,1415684885.553715,iphone:test_user_0,|136db128e090962858a18444db04a499';
$_POST['displayName'] = 'test name';
$_POST['score'] = 15000;
$_POST['challengeType'] = 'daily_clear_obstacles';
$_POST['leaderboardName'] = 'daily_clear_obstacles_leaderboard_2';
opt  $_POST['expirationDuration'] = '86400';
opt  $_POST['leaderboardSort'] = 'desc';
opt  $_POST['leaderboardURL'] = 'https://dev-genfed-fsb001.gameloft.com:45761';
*/

$paramKeys = array(
    'accessToken',
    'displayName',
    'score',
    'challengeType',
    'leaderboardName',
);

if (!checkParams($paramKeys, $_POST, $error))
{
    header("HTTP/1.0 404 Not Found");
    echo $error . "\n";
    exit();
}

if (!empty($_GET['debug']))
{
    debugmode($_GET['debug']);
    echo '<b>Version:</b> ' . $GLOBALS['config']['version'] . '<br>';
}

$result = processLeaderboardUpdating($params = $_POST);
if ($result['status'] != 'success')
{
    switch ($result['status'])
    {
        case 'post_blocked':
            $http_status = 'HTTP/1.0 409 Conflict';
            $json_response = json_encode($result);
            break;
        default:
            $http_status = 'HTTP/1.0 404 Not Found';
            $json_response = json_encode($result);
    }
    header($http_status);
    header('Content-Type: application/json');
    echo $json_response;
}
debugmode(false);
