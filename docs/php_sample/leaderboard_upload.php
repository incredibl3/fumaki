<?php
/*
 * Leaderboard Uploader
 *
 */

require_once('leaderboard_config.php');
require_once('classes/user.php');
require_once('includes/project_common.php');

function postEntry($credential, $display_name, $score, $leaderboard_name, $leaderboard_sort, $expiration_duration)
{
    // user authorisation in the Federation
    $user = User::authImpersonated(
        $GLOBALS['config']['clientId'],
        $GLOBALS['config']['userCredential'],
        $GLOBALS['config']['userPassword'],
        $credential,
        'auth leaderboard'
    );
    // post entry to the leaderboard
    $user->postLeaderboardEntry(
        $leaderboard_sort,
        $leaderboard_name,
        $display_name,
        $score,
        array(
            'expiration_duration' => $expiration_duration
        )
    );
}

function clearLeaderboard($leaderboard_name, $leaderboard_sort)
{
    // user authorisation in the Federation
    $user = User::auth(
        $GLOBALS['config']['clientId'],
        $GLOBALS['config']['userCredential'],
        $GLOBALS['config']['userPassword'],
        'auth leaderboard_clear'
    );
    // clear leaderboard
    $user->clearLeaderboard($leaderboard_sort, $leaderboard_name);
}

function processLeaderboardUploading($params = array(), &$message)
{
    if (empty($params['leaderboard_name']))
    {
        $message = 'You should provide Leaderboard Name';
        return false;
    }

    if (empty($params['entries_num'])) $params['entries_num'] = 10; // default
    if (empty($params['sleep_seconds']) && $params['sleep_seconds'] == '') $params['sleep_seconds'] = 2; // default
    if (empty($params['min_range']) && $params['min_range'] == '') $params['min_range'] = 10000; // default
    if (empty($params['max_range'])) $params['max_range'] = 70000; // default

    ob_end_flush();
    $list = array();
    if ($params['action'] == 'upload')
    {
        if (empty($_FILES['list']['tmp_name']))
        {
            $message = 'No file uploaded';
            return false;
        }

        $file_handle = fopen($_FILES['list']['tmp_name'], 'r');
        while (!feof($file_handle))
        {
            $row = fgetcsv($file_handle, 1024);
            if (is_numeric($row[2]))
            {
                $list[] = $row;
            }
        }
        fclose($file_handle);
        $message = 'File successfully uploaded';
    }
    elseif ($params['action'] == 'generate')
    {
        for ($i=0; $i < $params['total_users']; $i++)
        {
            $credential = 'iphone:'.$params['leaderboard_name'] . '_user_' . $i;
            $randomUser = User::auth(
                $GLOBALS['config']['clientId'],
                $credential,
                'password',
                'auth leaderboard'
            );
            if (!$error = $randomUser->getError())
            {
                $list[] = array($credential, 'User Name #' . $i, rand($params['min_range'], $params['max_range']));
                $percent = round(($i / $params['total_users']) * 100);
                echo '<script>$("#progressbar").progressbar("option", "subtitle", "Generating users - "); $( "#progressbar" ).progressbar({value: ' . $percent . '}); $("#progressbar").show();</script>';
            }
            flush();
        }
        $message = count($list) . ' random users successfully generated';
    }

    if (in_array($params['action'], array('upload', 'generate')))
    {
        $num = 0;
        foreach ($list as $row)
        {
            list($credential, $display_name, $score) = $row;
            postEntry(
                $credential,
                $display_name,
                $score,
                $params['leaderboard_name'],
                $leaderboard_sort = 'desc',
                $expiration_duration = '2592000' /* 1 month */
            );
            $percent = round(($num / count($list)) * 100);
            echo '<script>$("#progressbar").progressbar("option", "subtitle", "Posting entries - "); $( "#progressbar" ).progressbar({value: ' . $percent . '}); $("#progressbar").show();</script>';
            $num++;
            if ($num % $params['entries_num'] == 0)
            {
                sleep($params['sleep_seconds']);
            }
            flush();
        }
        $message .= '<br>' . count($list) . ' entries has been posted';
    }

    if ($params['action'] == 'clear_leaderboard')
    {
        clearLeaderboard(
            $params['leaderboard_name'],
            $leaderboard_sort = 'desc'
        );
        $message = 'Leaderboard successfully cleared';
    }
    return true;
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Leaderboard data uploader</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        * { font-size: 16px; }
        div.error, div.message { width: 600px; padding: 10px; margin: 20px auto; color: #090; outline: #090 1px solid; }
        div.error { color: #900; outline: #900 1px solid; }
        form { max-width: 630px; padding: 19px 29px 29px; margin: 20px auto; background-color: #fff; border: 1px solid #e5e5e5; border-radius: 5px; box-shadow: 0 1px 2px rgba(0,0,0,.05); }
        form input { font-size: 16px; height: auto; margin-bottom: 0 !important; width: 430px; }
        #entries-num, #sleep-seconds, #min-range, #max-range { width: 50px; }
        #total-users { width: 180px; }
        form input[type="file"] { width: 450px; display: inline-block; }
        button.btn { width: 160px; float: right; }
        h2 { font-size: 20px; line-height: 20px; }
        #progressbar { max-width: 630px; margin: 0 auto;}
        .ui-progressbar { position: relative; }
        .progress-label { position: absolute; width: 100%; margin: 0 auto; text-align: center; top: 4px; font-weight: bold; text-shadow: 1px 1px 0 #fff; }
        .debug { margin: 0 auto; width: 630px; height: 200px; overflow-y: auto; }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
</head>
<body>

<form method="post" action="" enctype="multipart/form-data">
    <h1>Post entries to the leaderboard</h1>
    Post <input type="text" id="entries-num" name="entries_num" value="10" placeholder="10">
    entries every
    <input type="text" id="sleep-seconds" name="sleep_seconds" value="2" placeholder="2"> seconds
    into<br><br>
    <input type="text" id="credential" name="leaderboard_name" value="" placeholder="Leaderboard Name">

    <h2><br>Upload list of credentials</h2>
    <input type="file" id="list" name="list" value="" placeholder="List">
    <button id="upload-list-btn" class="btn btn-success" type="submit" disabled="disabled" name="action" value="upload">Upload list</button>

    <h2>OR</h2>

    <h2>Generate random credentials</h2>
    <input type="text" id="total-users" name="total_users" value="" placeholder="Number of users">&nbsp;&nbsp;&nbsp;
    Score from <input type="text" id="min-range" name="min_range" value="10000" placeholder="Score minimum">
    to <input type="text" id="max-range" name="max_range" value="70000" placeholder="Score maximum">
    <button id="generate-list-btn" class="btn btn-success" type="submit" disabled="disabled" name="action" value="generate">Generate</button>
</form>

<form method="post" action="" enctype="multipart/form-data" id="add-form">
    <h2>Clear leaderboard</h2>
    <input type="text" id="credential" name="leaderboard_name" value="" placeholder="Leaderboard Name">
    <input type="hidden" name="action" value="clear_leaderboard">
    <button id="clear-leaderboard-btn" class="btn btn-primary" type="submit" disabled="disabled">Clear leaderboard</button>
</form>
<div id="progressbar" style="display: none"><div class="progress-label">Processing...</div></div>
<script>
    var progressbar = $( "#progressbar" ),
        progressLabel = $( ".progress-label");

    progressbar.progressbar({
        value: false,
        change: function() {
            progressLabel.text( progressbar.progressbar("option", "subtitle" ) + progressbar.progressbar( "value" ) + "%" );
        },
        complete: function() {
            progressLabel.text( "Complete!" );
        }
    });
</script>
<?php

if (!empty($_GET['debug']))
{
    debugmode($_GET['debug']);
    echo '<div class="debug">';
    echo '<b>Version:</b> ' . $GLOBALS['config']['version'] . '<br>';
}

if (!empty($_POST))
{
    $is_error = !processLeaderboardUploading($_POST, $message);
}

if (!empty($_GET['debug']))
{
    echo '</div>';
}

if (!empty($message))
{
    echo $is_error ? '<div class="error"><b>Error:</b> ' . $message . '</div>' : '<div class="message">' . $message . '</div>';
}
?>
<script>
    $('#progressbar').hide();
    $('#upload-list-btn').removeAttr("disabled");
    $('#generate-list-btn').removeAttr("disabled");
    $('#clear-leaderboard-btn').removeAttr("disabled");
</script>
</body>
</html>