<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (!empty($_POST['fbuid']) && !empty($_POST['name']) && !empty($_POST['avatar_url'])
	&& !empty($_POST['latitude']) && !empty($_POST['longitude'])) {
 
    // include db connect class
    require_once __DIR__ . '/db_connect.php';
 
    // connecting to db
    $db = new DB_CONNECT();
	
	if (isset($_POST['fbuid'])) {
		$fbuid = mysqli_real_escape_string($db->mysqli, $_POST['fbuid']);
	}
	if (isset($_POST['name'])) {
		$name = mysqli_real_escape_string($db->mysqli, $_POST['name']);
	}

    if (isset($_POST['avatar_url'])) {
        $avatar_url = mysqli_real_escape_string($db->mysqli, urlencode($_POST['avatar_url']));
    }
    $latitude = floatval($_POST['latitude']);
    $longitude = floatval($_POST['longitude']);

    // mysql inserting a new row
    $result = mysqli_query($db->mysqli, "INSERT INTO user_profile(`userID`, `name`, `avatar_url`,`latitude`, `longitude`) VALUES('$fbuid', '$name', '$avatar_url', $latitude, $longitude)") or trigger_error(mysqli_error($db->mysqli));
	
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "User successfully inserted.";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = mysqli_error($db->mysqli);
 
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>