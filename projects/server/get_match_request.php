<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (!empty($_POST['fbuid'])) {
 
    // include db connect class
    require_once __DIR__ . '/db_connect.php';
 
    // connecting to db
    $db = new DB_CONNECT();

    $fbuid = mysqli_real_escape_string($db->mysqli, $_POST['fbuid']);
	
    $result = mysqli_query($db->mysqli, "SELECT user_profile.userID, user_profile.name, user_profile.avatar_url
							FROM user_profile
							INNER JOIN match_handler
							ON user_profile.userID=match_handler.sent_from
							WHERE match_handler.sent_to='$fbuid'");
	if (!$result) {
		die('Could not query:' . mysqli_error());
	}
	
	if (!empty($result)) {
        // check for empty result
        if (mysqli_num_rows($result) > 0) {
 
            $rows = array();
			 while($r = mysqli_fetch_assoc($result)) {
				$rows[] = $r;
			}
 
			$response["success"] = 1;
			$response["match_request"] = $rows;
			
            // echoing JSON response
            echo json_encode($response);
        } else {
            // no user found
            $response["success"] = 0;
            $response["message"] = "No user found";
 
            // echo no users JSON
            echo json_encode($response);
        }
    } else {
        // no user found
        $response["success"] = 0;
        $response["message"] = "No user found";
 
        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>