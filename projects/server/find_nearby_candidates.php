<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (!empty($_GET['fbuid']) && !empty($_GET['latitude']) && !empty($_GET['longitude']) && !empty($_GET['distance'])) {
 
    // include db connect class
    require_once __DIR__ . '/db_connect.php';
 
    // connecting to db
    $db = new DB_CONNECT();

    $fbuid = mysqli_real_escape_string($db->mysqli, $_GET['fbuid']);
	$lat = floatval($_GET['latitude']);
	$long = floatval($_GET['longitude']);
	$distance = floatval($_GET['distance']);
	
    // mysql inserting a new row
	$result = mysqli_query($db->mysqli, "Call FindNearbyCandidate($fbuid, $lat, $long, $distance)");
	if (!$result) {
		die('Could not query:' . mysqli_error());
	}
	
	$resJson = array();
	while ($row = mysqli_fetch_assoc($result))
	{
		$resJson[] = $row;
	}
	$response["success"] = 1;
	$response["candidates"] = $resJson;
	
	echo json_encode($response);
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>