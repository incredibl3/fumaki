<?php
 
/*
 * Following code will update a product information
 * A product is identified by user id (user_id)
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_POST['user_id']) && isset($_POST['match_candidate']) && isset($_POST['match'])) {
 
    $user_id = $_POST['user_id'];
    $match_candidate = $_POST['match_candidate'];
    $match = $_POST['match'];
 
    // include db connect class
    require_once __DIR__ . '/db_connect.php';
 
    // connecting to db
    $db = new DB_CONNECT();
 
    // mysql update row with matched pid
    $result = mysqli_query($db->mysqli, "UPDATE users SET `match` = $match WHERE user_id = $user_id AND match_candidate = $match_candidate");
 
    // check if row inserted or not
    if ($result) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "Product successfully updated.";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
 
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>