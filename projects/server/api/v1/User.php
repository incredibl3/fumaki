<?php

include_once __DIR__ . '/DB_Connect.inc.php';

class User {

	private $con;
	
	private static $_instance;
	
	public function __construct() {
		$this->con = new DB_CONNECT();
	}
	
	public function __destruct() {
		self::$_instance = null;
		$this->con = null;
    }
	
	
	public static function getInstance(){
		if(is_null(self::$_instance) || !isset(self::$_instance)){
			$user = new self();
			self::$_instance = $user;
		}
		
		return self::$_instance;
	}
	
	public function createUser($data){
    	$fbid 			= mysqli_real_escape_string($this->con->mysqli, $data["userID"]);
    	$name 			= mysqli_real_escape_string($this->con->mysqli, $data["name"]);
    	$avatar_url		= mysqli_real_escape_string($this->con->mysqli, $data["avatar_url"]);
    	$profilePic0_url= mysqli_real_escape_string($this->con->mysqli, $data["profilePic0_url"]);
    	$profilePic1_url= mysqli_real_escape_string($this->con->mysqli, $data["profilePic1_url"]);
    	$profilePic2_url= mysqli_real_escape_string($this->con->mysqli, $data["profilePic2_url"]);
    	$profilePic3_url= mysqli_real_escape_string($this->con->mysqli, $data["profilePic3_url"]);
    	$profilePic4_url= mysqli_real_escape_string($this->con->mysqli, $data["profilePic4_url"]);
    	$profilePic5_url= mysqli_real_escape_string($this->con->mysqli, $data["profilePic5_url"]);
    	$birthday 		= mysqli_real_escape_string($this->con->mysqli, $data["birthday"]);
    	$gender 		= mysqli_real_escape_string($this->con->mysqli, $data["gender"]);
    	$latitude 		= floatval($data["latitude"]);
    	$longitude 		= floatval($data["longitude"]);
        $interest_distance = empty($data["interest_distance"]) ? 30 : intval($data["interest_distance"]);
        $distance_unit  =  empty($data["distance_unit"]) ? "km" : mysqli_real_escape_string($this->con->mysqli, $data["distance_unit"]);
		$gcm_regid = mysqli_real_escape_string($this->con->mysqli, $data["gcm_regid"]);
		
    	$query = "INSERT INTO `user_profile`(`userID`, `name`, `avatar_url`, `profilePic0_url`, `profilePic1_url`, `profilePic2_url`, `profilePic3_url`, `profilePic4_url`, `profilePic5_url`, 
												`birthday`, `gender`,`latitude`, `longitude`, `interest_distance`, `distance_unit`, `gcm_regid`) 
    				VALUES('$fbid', '$name', '$avatar_url', '$profilePic0_url', '$profilePic1_url', '$profilePic2_url', '$profilePic3_url', '$profilePic4_url', '$profilePic5_url', 
												'$birthday', '$gender',  $latitude, $longitude, $interest_distance, '$distance_unit', '$gcm_regid')";

    	$result = mysqli_query($this->con->mysqli, $query) ;

    	if ($result) {
    		// return array("id" => $fbid
    					// , "name" => $name
    					// , "avatar_url" => $avatar_url
    					// , "birthday" => $birthday
    					// , "gender" => $gender
    					// , "latitude" => $latitude
                        // , "logitude" => $longitude
                        // , "interest_distance" => $interest_distance
    					// , "distance_unit" => $distance_unit
                        // );
			return $this->getUser($fbid);
    	}
    		
    	throw new RestException(500, RestServer::getInstance()->getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
	}

	public function getUser($fbid){

    	$query = "SELECT * FROM `user_profile` WHERE `userID`=$fbid";

    	$result = mysqli_query($this->con->mysqli, $query) ;//or trigger_error(mysqli_error($this->con->mysqli));

    	if ($result) {

			return mysqli_fetch_assoc($result);
    	}

    	throw new RestException(500, RestServer::getInstance()->getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
	}

	public function deleteUser($fbid){

    	$query = "DELETE FROM `user_profile` WHERE `userID`=$fbid";

    	$result = mysqli_query($this->con->mysqli, $query) ;
		

    	if ($result) {
    		
			return ;
    	}

    	throw new RestException(500, RestServer::getInstance()->getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
	}

	public function getAllUsers(){

    	$query = "SELECT * FROM `user_profile` WHERE 1";

    	$result = mysqli_query($this->con->mysqli, $query);
		

    	if ($result) {
    		$response_array = array();
    		while( $row = mysqli_fetch_assoc( $result)){
			    $response_array[] = $row; // Inside while loop
			}
			return $response_array;
    	}

    	throw new RestException(500, RestServer::getInstance()->getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
	}

	public function updateUser($id, $data){
        // $fbid           = mysqli_real_escape_string($this->con->mysqli, $data["userID"]);
        $name           = mysqli_real_escape_string($this->con->mysqli, $data["name"]);
        // $avatar_url     = mysqli_real_escape_string($this->con->mysqli, $data["avatar_url"]);
        $birthday       = mysqli_real_escape_string($this->con->mysqli, $data["birthday"]);
        $gender         = mysqli_real_escape_string($this->con->mysqli, $data["gender"]);
        $latitude       = floatval($data["latitude"]);
        $longitude      = floatval($data["longitude"]);
        // $interest_distance = intval($data["interest_distance"]);
        // $distance_unit  = mysqli_real_escape_string($this->con->mysqli, $data["distance_unit"]);

        $query = "UPDATE `user_profile` 
                    SET `name`='$name',`birthday`='$birthday',`gender`='$gender',`latitude`=$latitude,`longitude`=$longitude
                    WHERE `userID`=$id";

        $result = mysqli_query($this->con->mysqli, $query);
        

        if ($result) {
            return $this->getUser($id);
        }

        throw new RestException(500, RestServer::getInstance()->getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
    }

    public function authenticateWithFacebook($username, $password)
    {
    	// TODO
		return true;
    }

	public function seekCandidates($id){

    	$query = "Call FindNearbyCandidate($fbuid, $lat, $long, $distance)";

    	$result = mysqli_query($this->con->mysqli, $query);
		

    	if ($result) {
    		$num = mysqli_num_rows($result);
			$response_array = array();
			while( $row = mysqli_fetch_assoc( $result)){
		    	$response_array[] = $row; // Inside while loop
			}
			return $response_array;
			
    	}

    	throw new RestException(500, RestServer::getInstance()->getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
	}

    public function getMatches($id)
    {

        $query = "SELECT  `user_profile`.`userID`, `user_profile`.`name` ,  `user_profile`.`avatar_url` ,  `user_profile`.`birthday` ,  `user_profile`.`gender` ,  `user_profile`.`description` ,  `user_profile`.`latitude` ,  `user_profile`.`longitude` ,  `match_handler`.`action` 
                    FROM  `match_handler` 
                    INNER JOIN  `user_profile` 
                    WHERE (
                        `match_handler`.`user1` ='$id'
                        AND  `match_handler`.`user2` =  `user_profile`.`userID`
                    )
                    OR (
                        `match_handler`.`user2` ='$id'
                        AND  `match_handler`.`user1` =  `user_profile`.`userID`
                    )";

        

        $result = mysqli_query($this->con->mysqli, $query);
        

        if ($result) {
            $num = mysqli_num_rows($result);
            $response_array = array();
            while( $row = mysqli_fetch_assoc( $result)){
                $response_array[] = $row; // Inside while loop
            }
            return $response_array;
            
        }

        throw new RestException(500, RestServer::getInstance()->getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
    }

    public function deleteMatches($id)
    {
        $query = "DELETE FROM `match_handler` WHERE `user1`='$id' OR `user2`='$id'";
        
        $result = mysqli_query($this->con->mysqli, $query);
        if($result){
            return true;
        }

        throw new RestException(500, RestServer::getInstance()->getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
    }

    public function checkExistingUser($username) {
        $query = "SELECT * FROM `user_profile` WHERE `userID`=$username";
        
        $result = mysqli_query($this->con->mysqli, $query);
        
        if ($result) {
            $num = mysqli_num_rows($result);
            
            if ($num > 0) return true;
            else return false;
        }
        
        return false;
    }

}