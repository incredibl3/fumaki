<?php

include_once __DIR__ . '/DB_Connect.inc.php';

class GCM {
 
    private $con;
	private static $_instance;
	    
    // constructor
    function __construct() {
        
        // connecting to database
        $this->con = new DB_CONNECT();
    }
 
    // destructor
    function __destruct() {
        self::$_instance = null;
		$this->con = null; 
    }
 
	public static function getInstance(){
		if(is_null(self::$_instance) || !isset(self::$_instance)){
			$user = new self();
			self::$_instance = $user;
		}
		
		return self::$_instance;
	}
 
    /**
     * Sending Push Notification
     */
    public function send_notification($registration_ids, $message) {
        // include config
        include_once __DIR__ . '/db_config.php';
 
        // Set POST variables
        // $url = 'https://gcm-http.googleapis.com/gcm/send';
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message,
        );
 
        $headers = array(
            'Authorization: key='.GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        // Close connection
        curl_close($ch);
        //echo $result;
    }
 
	
    /**
     * Storing new user
     * returns user details
     */
	public function createUserPN($data) {
        // insert userPN into database
    	$name 			= mysqli_real_escape_string($this->con->mysqli, $data["name"]);
    	$email			= mysqli_real_escape_string($this->con->mysqli, $data["email"]);
    	$gcm_regid 		= mysqli_real_escape_string($this->con->mysqli, $data["gcm_regid"]);
    	
        $result = mysqli_query($this->con->mysqli, "INSERT INTO gcm_users(name, email, gcm_regid, created_at) VALUES('$name', '$email', '$gcm_regid', NOW())");
        // check for successful store
        if ($result) {
            // get user details
            $id = mysqli_insert_id(); // last inserted id
            $result = mysqli_query($this->con->mysqli, "SELECT * FROM gcm_users WHERE id = $id") or die(mysqli_error($this->con->mysqli));
            // return user details
            if (mysqli_num_rows($result) > 0) {
                return mysqli_fetch_array($result);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
 
    /**
     * Getting all users
     */
    public function getAllUsers() {
        $query = "select * FROM gcm_users";
        $result = mysqli_query($this->con->mysqli, $query);
		

    	if ($result) {
    		$response_array = array();
    		while( $row = mysqli_fetch_assoc( $result)){
			    $response_array[] = $row; // Inside while loop
			}
			return $response_array;
    	}

    	throw new RestException(500, RestServer::getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
    }
 
	 
	/**
	 * Registering a user device
	 * Store reg id in users table
	 */
	public function registerPN($data) {
		 
		// response json
		$json = array();
		 
    	$name 			= mysqli_real_escape_string($this->con->mysqli, $data["name"]);
    	$email			= mysqli_real_escape_string($this->con->mysqli, $data["email"]);
    	$gcm_regid 		= mysqli_real_escape_string($this->con->mysqli, $data["gcm_regid"]);		 
		
		// Store user details in db	 
		$res = $this->con->storeUser($name, $email, $gcm_regid);
	 
		$registration_ids = array($gcm_regid);
		$message = array("product" => "shirt");
	 
		$result = $this->send_notification($registration_ids, $message);
	 
		//echo $result;
	}
	
	/*
	* send push notification to android device by making a request to GCM server
	*/
	public function sendMessage($data) {
    	$message		= mysqli_real_escape_string($this->con->mysqli, $data["message"]);
    	$gcm_regid 		= mysqli_real_escape_string($this->con->mysqli, $data["regId"]);
		
		//$message = "Test PN 1";
		//$gcm_regid = "dMXeJQ4wSHw:APA91bFF0s1-5MV70-BTufY33AE9LRStp-UHe_2pjHv4XZYdB8j_aDsc_HZzIkgkM_f92PMEO0U8SCp2AVETY3OIeFQusFTZZaa5JMhS_QuTCwYi18DW1jpliSb8_92iJtmjZtfTGkMk";
		
		$registration_ids = array($gcm_regid);
		$messagePN = array("message" => $message);
		
		$result = $this->send_notification($registration_ids, $messagePN);
		//echo $result;		
	}
}
 
?>