<?php

include_once __DIR__ . '/DB_Connect.inc.php';

class Utils {

	private $con;
	
	private static $_instance;
	
	public function __construct() {
		$this->con = new DB_CONNECT();
	}
	
	public function __destruct() {
		self::$_instance = null;
		$this->con = null;
    }
	
	
	public static function getInstance(){
		if(is_null(self::$_instance) || !isset(self::$_instance)){
			$utils = new self();
			self::$_instance = $utils;
		}
		
		return self::$_instance;
	}


    public function authenticateWithFacebook($username, $password)
    {
    	// TODO
		return true;
    }

	public function doAuth()
    {
        if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
            $username = $_SERVER['PHP_AUTH_USER'];
            $password = $_SERVER['PHP_AUTH_PW'];

            if($this->authenticateWithFacebook($username, $password)){
                return true;
            }
        }
        return false;
    }

    public function isAdminUser($username){
        // TODO
        return true;
    }

	public function authAndPermissionCheck($id)
    {
    	if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
            $username = $_SERVER['PHP_AUTH_USER'];
            $password = $_SERVER['PHP_AUTH_PW'];
	        if($this->getInstance()->authenticateWithFacebook($username, $password)){
                 if($id == "me")
		            $id = $username;

		        if($id != $username){
		            if($this->getInstance()->isAdminUser()){
		            	return true;
		            }
		            else{
		                throw new RestException(403, "Forbidden!!You don't have permission!");
		            }
		        }
            }
            else RestServer::getInstance()->unauthorized(false);
	    }
	    else RestServer::getInstance()->unauthorized(false);

	    return true;
    }

}