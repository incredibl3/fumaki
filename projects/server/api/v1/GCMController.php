<?php

include_once __DIR__ . '/GCM.php';

class GCMController
{
    /**
     * Returns a JSON string object to the browser when hitting the root of the domain
     *
     * @url GET /usersPN
     */
    public function getAllUsersPN()
    {
       return GCM::getInstance()->getAllUsers();
    }
	
    /**
     * Create a new user
     *
     * @url POST /usersPN
     */
    public function createUserPN()
    {
        $server = RestServer::getInstance();
        if ($server->getMethod() == 'POST'){
            if($server->getFormat() != RestFormat::JSON){
                //throw new RestException(415, "POST/PUT/PATCH request occurred without a application/json");
            }
            $data = $server->getData(true);
            if($data == null){
                throw new RestException(406, "POST method is required to execute creating a user PN!");
            }

            return UserPN::getInstance()->createUserPN($data);
        }
        else {

            throw new RestException(406, "POST method is required to execute creating a user PN!");
        }
        
    }
	
	/**
     * Register Push Notification
     *
     * @url POST /registerPN
     */
	public function registerPN()
	{
		$server = RestServer::getInstance();
        if ($server->getMethod() == 'POST'){
            if($server->getFormat() != RestFormat::JSON){
                //throw new RestException(415, "POST/PUT/PATCH request occurred without a application/json");
            }
            $data = $server->getData(true);
            if($data == null){
                throw new RestException(406, "POST method is required to execute registerPN!");
            }

            return GCM::getInstance()->registerPN($data);
        }
        else {

            throw new RestException(406, "POST method is required to execute registerPN!");
        }
	}
	
	/**
     * Send Push Notification
     *
     * @url POST /sendMessage
     */
	public function sendMessage()
	{
		$server = RestServer::getInstance();
        if ($server->getMethod() == 'POST'){
            if($server->getFormat() != RestFormat::JSON){
                throw new RestException(415, "POST/PUT/PATCH request occurred without a application/json");
            }			
            $data = $server->getData(true);
            if($data == null){
                throw new RestException(406, "POST method is required to execute sendMessage data == null !");
            }
            return GCM::getInstance()->sendMessage($data);
        }
        else {

            throw new RestException(406, "POST method is required to execute sendMessage!");
        }
	}
}