<?php

include_once __DIR__ . '/Request.php';
include_once __DIR__ . '/User.php';

class RequestController
{
    /**
     * Returns a JSON string object to the browser when hitting the root of the domain
     *
     * @url GET /requests
     */
    public function getAllRequests()
    {
       return Request::getInstance()->getAllRequests();
    }

    /**
     * Create a new request
     *
     * @url POST /requests
     */
    public function createRequests()
    {
        $server = RestServer::getInstance();
        $data = $server->getData(true);
        
        // return $data[0];
        foreach ($data as $element){
            Request::getInstance()->createRequest($element);
        }
        return $data;
    }
    /**
     * Gets the request by id
     *
     * @url GET /requests/$id
     */
    public function getRequest($id)
    {

        return Request::getInstance()->getRequest($id); // serializes object into JSON
    }

    /**
     * Delete the request by id
     *
     * @url DELETE /requests/$id
     */
    public function deleteRequest($id)
    {

        return Request::getInstance()->deleteRequest($id); // serializes object into JSON
    }

    /**
     * Update a request to the database
     *
     * @url PUT /requests/$id
     */
    public function updateRequest($id = null, $data)
    {
        // ... validate $data properties such as $data->username, $data->firstName, etc.
        // $data->id = $id;
        // $user = User::saveUser($data); // saving the user to the database
        $user = array("id" => $id, "name" => null);
        return $user; // returning the updated or newly created user object
    }
}