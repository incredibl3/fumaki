<?php

require 'RestServer.php';
require 'RequestController.php';
require 'UserController.php';
require 'GCMController.php';

$mode = 'debug'; // either debug or production
$server = RestServer::getInstance();
$server->setMode($mode);

$server->addClass('UserController');
$server->addClass('GCMController');
$server->addClass('RequestController');
$server->handle();
