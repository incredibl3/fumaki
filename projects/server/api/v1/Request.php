<?php

include_once __DIR__ . '/DB_Connect.inc.php';
include_once __DIR__ . '/GCM.php';

class Request {

	private $con;
	
	private static $_instance;
	
	public function __construct() {
		$this->con = new DB_CONNECT();
	}
	
	public function __destruct() {
		self::$_instance = null;
		$this->con = null;
    }
	
	
	public static function getInstance(){
		if(is_null(self::$_instance) || !isset(self::$_instance)){
			$req = new self();
			self::$_instance = $req;;
		}
		
		return self::$_instance;
	}
	
	public function createRequest($data)
    {
    	$from 			= mysqli_real_escape_string($this->con->mysqli, $data["from"]);
    	$to		        = mysqli_real_escape_string($this->con->mysqli, $data["to"]);
    	$action 		= mysqli_real_escape_string($this->con->mysqli, $data["action"]);

        // return $from;
        if($from == $to)
            return;

        $query1 = "SELECT `action` FROM `requests` WHERE `from`='$to' AND `to`='$from'";

        $result1 = mysqli_query($this->con->mysqli, $query1) ;

        if ($result1) {
            $num = mysqli_num_rows($result1);
            // return $num;

            if($num == 0)
            {
                $query2 = "INSERT INTO `requests`(`from`, `to`, `action`) VALUES('$from', '$to', '$action')";
                $temp_query = "SELECT `requestID` FROM `requests` WHERE `from`='$from' AND `to`='$to'";
                $temp_result = mysqli_query($this->con->mysqli, $temp_query); 
                if($temp_result){
                    $temp_num = mysqli_num_rows($temp_result);
                    if($temp_num > 0){
                        $row = mysqli_fetch_assoc($temp_result);
                        $rqId = $row["requestID"];
                        $query2 = "UPDATE `requests` SET `from`='$from',`to`='$to',`action`='$action' WHERE `requestID`=$rqId";
                   } 
                }
                $result2 = mysqli_query($this->con->mysqli, $query2) ; 

                if ($result2) {
                    return;
                }
            }
            else
            {
                $existReq = mysqli_fetch_assoc($result1);
                // return $act;
                if($existReq["action"] == $action){
                    $this->createMatch($from, $to, $action);
                } 

                $query2 = "DELETE FROM `requests` WHERE `from`=$to AND `to`=$from";
                $result2 = mysqli_query($this->con->mysqli, $query2) ; 
                if ($result2) {
                    return;
                }
            
            }

        } 
    		
    	throw new RestException(500, RestServer::getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
	}


    public function createMatch($user1, $user2, $action)
    {
		//hungnt send PN to matched user
		$temp_query = "SELECT `gcm_regid` FROM `user_profile` WHERE `userID`='$user2'";
		$temp_result = mysqli_query($this->con->mysqli, $temp_query); 
		if($temp_result){
			$temp_num = mysqli_num_rows($temp_result);
			if($temp_num > 0){
				$row = mysqli_fetch_assoc($temp_result);
				$rqId = $row["gcm_regid"];			
				$mes = "Hello World! You got match request";
				$registration_ids = array($rqId);
				$messagePN = array("message" => $mes);
				GCM::getInstance()->send_notification($registration_ids, $messagePN);
			} 
		}
		//hungnt end
		
        $query = "INSERT INTO `match_handler`(`user1`, `user2`, `action`) 
                VALUES('$user1', '$user2', '$action')";

        $temp_query = "SELECT matchID FROM `match_handler` WHERE (`user1`='$user1' ANDO `user2`='$user2') OR (`user1`='$user2' ANDO `user2`='$user1')";
        $temp_result = mysqli_query($this->con->mysqli, $temp_query) ;
        if($temp_result){
            $temp_num = mysqli_num_rows($temp_result);
            if($temp_num > 0){
                $row = mysqli_fetch_assoc($temp_result);
                $matchId = $row["matchID"];
                $query = "UPDATE `match_handler` SET `user1`='$user1',`user2`='$user2',`action`='$action' WHERE `matchID`='$matchID'";
            }
        }

        $result = mysqli_query($this->con->mysqli, $query) ;

        if ($result) {
            return;
        }
            
        throw new RestException(500, RestServer::getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
    }

	public function getRequest($id){

    	$query = "SELECT `userID`, `name`, `avatar_url`, `birthday`, `gender`, `latitude`, `longitude` FROM `user_profile` WHERE `userID`=$fbid";

    	$result = mysqli_query($this->con->mysqli, $query) ;//or trigger_error(mysqli_error($this->con->mysqli));

    	if ($result) {

			return mysqli_fetch_assoc($result);
    	}

    	throw new RestException(500, RestServer::getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
	}

	public function deleteRequest($fbid){

    	$query = "DELETE FROM `user_profile` WHERE `userID`=$fbid";

    	$result = mysqli_query($this->con->mysqli, $query) ;
		

    	if ($result) {
    		
			return ;
    	}

    	throw new RestException(500, RestServer::getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
	}

	public function getAllRequests(){

    	$query = "SELECT * FROM `requests` WHERE 1";

    	$result = mysqli_query($this->con->mysqli, $query);
		

    	if ($result) {
    		$response_array = array();
    		while( $row = mysqli_fetch_assoc( $result)){
			    $response_array[] = $row; // Inside while loop
			}
			return $response_array;
    	}

    	throw new RestException(500, RestServer::getMode() == 'debug' ? mysqli_error($this->con->mysqli) : null);
	}

}