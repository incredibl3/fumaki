<?php
 
/**
 * A class file to connect to database
 */
// MySQLi Database Connection
class DB_CONNECT {

	// MySQLi object instance
	public $mysqli = null;

    // Class constructor override
    public function __construct() {

        require_once __DIR__ . '/db_config.php';

        $this->mysqli = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);

        if ($this->mysqli->connect_errno) {
            echo "Error MySQLi: (". $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
            exit();
        }
    }

    // select database to use
    public function selectDB($dbname) {
        $selectedDB = $this->mysqli->select_db($dbname);
        return $selectedDB;
    }

    // Class deconstructor override
    public function __destruct() {
        $this->CloseDB();
    }

    // Close database connection
    public function CloseDB() {
        $this->mysqli->close();
    }

} //end Class DBConn
 