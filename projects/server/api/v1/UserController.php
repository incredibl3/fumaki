<?php

include_once __DIR__ . '/User.php';
include_once __DIR__ . '/Utils.php';
include_once __DIR__ . '/GCM.php';

class UserController
{
    /**
     * Logs in a user with the given username and password POSTed. Though true
     * REST doesn't believe in sessions, it is often desirable for an AJAX server.
     *
     * @url POST /users/auth
     */
    public function auth()
    {
      
        $server = RestServer::getInstance();
        $user = User::getInstance();
        if ($server->getMethod() == 'POST'){
            $data = $server->getData(true);

            $authWithFbOk = false;
            if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
                $username = $_SERVER['PHP_AUTH_USER'];
                $password = $_SERVER['PHP_AUTH_PW'];

                if($user->authenticateWithFacebook($username, $password)){
                    $authWithFbOk = true;
                }
            }
            if($authWithFbOk){
                $response_array = array();
                if(User::getInstance()->checkExistingUser($username))
                    $response_array["is_new_user"]  = 0;
                else
                    $response_array["is_new_user"]  = 1;
                
                return $response_array;
            }
            else{
				// TODO: dungnx:
				// delete $_SERVER['PHP_AUTH_USER'];
				// delete $_SERVER['PHP_AUTH_PW'];
                $server->unauthorized(false);
            }

        } else {
            throw new RestException(406, "POST method is required to authenticate!");
        }
    }


    /**
     * Returns a JSON string object to the browser when hitting the root of the domain
     *
     * @url GET /users
     */
    public function getAllUsers()
    {
        // TODO: require admin right
       return User::getInstance()->getAllUsers();
    }

    /**
     * Create a new user
     *
     * @url POST /users
     */
    public function createUser()
    {
        $server = RestServer::getInstance();

        // TODO: require admin right
        if ($server->getMethod() == 'POST'){
            if($server->getFormat() != RestFormat::JSON){
                //throw new RestException(415, "POST/PUT/PATCH request occurred without a application/json");
            }
            $data = $server->getData(true);
            if($data == null){
                throw new RestException(406, "POST method is required to execute creating a user!");
            }

            return User::getInstance()->createUser($data);
        }
        else {

            throw new RestException(406, "POST method is required to execute creating a user!");
        }
        
    }

    /**
     * Gets the user by id or current user
     *
     * @url GET /users/$id
     * @url GET /users/me
     */
    public function getUser($id)
    {
        //$id = mysqli_real_escape_string($id);
        $server = RestServer::getInstance();
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])){
            $server->unauthorized(false);
        }

        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];
        if($id != "me"){
            // TODO: require admin right
            return User::getInstance()->getUser($id); // serializes object into JSON
        }
           
        return User::getInstance()->getUser($username);
    }

    /**
     * Delete the user by id or current user
     *
     * @url DELETE /users/$id
     * @url DELETE /users/me
     */
    public function deleteUser($id)
    {
        //$id = mysqli_real_escape_string($id);
        $server = RestServer::getInstance();
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])){
            $server->unauthorized(false);
        }
        
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];

        if($id != 'me'){
            // TODO: require admin right
            return User::getInstance()->deleteUser($id); // serializes object into JSON
        } 

       
        return User::getInstance()->deleteUser($username);
    }

    /**
     * Saves a user to the database
     *
     * @url PATCH /users/$id
     * @url PATCH /users/me
     */
    public function updateUser($id)
    {
        //$id = mysqli_real_escape_string($id);
        $server = RestServer::getInstance();
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])){
            $server->unauthorized(false);
        }
        
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];
        $data = $server->getData(true);

        if($id != 'me'){
            //TODO: require admin right
            
            if(!User::getInstance()->checkExistingUser($id)){
                return User::getInstance()->createUser($data);
            }
            return User::getInstance()->updateUser($id, $data);
        } 

       
        if(!User::getInstance()->checkExistingUser($username)){
            return User::getInstance()->createUser($data);
        }
        return User::getInstance()->updateUser($username, $data);
    }


    /**
     * Saves a user to the database
     *
     * @url GET /users/$id/candidates
     * @url GET /users/me/candidates
     */
    public function seekCandidates($id)
    {
        //$id = mysqli_real_escape_string($id);
        // return User::getInstance()->seekCandidates($id); // serializes object into JSON
				
		//hungnt test PN
		$pnID = "c5aeKcJ4izY:APA91bGuStUpX6k8HctE1iEtzJaH0JxXvSax0tljNPrlF0Qc39qbmvTKXIdQhWf4LhGZu2WWjKqO6NQF82BCFEAaejMoG96kydQIlCcjEOhMNfd7gvqFqqB-b5NIpzRwT7QDTS1Bicy3";		
		$mes = "Hungnt Hello World!";
		$registration_ids = array($pnID);
		$messagePN = array("message" => $mes);
		GCM::getInstance()->send_notification($registration_ids, $messagePN);
		
        return User::getInstance()->getAllUsers();
    
    }

    /**
     * Saves a user to the database
     *
     * @url GET /users/$id/matches
     * @url GET /users/me/matches
     */
    public function getMatches($id)
    {
        if(Utils::getInstance()->authAndPermissionCheck($id)){
            if($id == "me"){
                $id = $_SERVER['PHP_AUTH_USER'];
            }
        }

        return User::getInstance()->getMatches($id); // serializes object into JSON 
    
    }

    /**
     * Delete a user to the database
     *
     * @url DELETE /users/$id/matches
     * @url DELETE /users/me/matches
     */
    public function deleteMatches($id)
    {
        if(Utils::getInstance()->authAndPermissionCheck($id)){
            if($id == "me"){
                $id = $_SERVER['PHP_AUTH_USER'];
            }
        }

        if( User::getInstance()->deleteMatches($id)) // serializes object into JSON 
        {
            $server = RestServer::getInstance()->setStatus(204);
        }
    
    }
}