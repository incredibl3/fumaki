package com.incredibl3.fumaki.XMPP;

import com.incredibl3.fumaki.FumakiActivity;
import com.incredibl3.fumaki.chat.ChatHistory;
import com.incredibl3.fumaki.chat.Message;
import com.incredibl3.utils.Console;

import org.jivesoftware.smack.chat.Chat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tien.dinhvan on 7/20/2015.
 */
public class HistoryManager {
    /**
     * Final Variable
     */
    public final static String TAG = "HistoryManager";

    /**
     * Main Context of the application
     */
    private FumakiActivity mActivity;
    public void SetContext(FumakiActivity activity) {
        mActivity = activity;
    }

    /**
     * Singleton implementation
     */
    private static HistoryManager mInstance;

    public static HistoryManager GetInstance() {
        if (mInstance == null) {
            mInstance = new HistoryManager();
        }

        return mInstance;
    }

    /**
     * List of History
     */
    List<ChatHistory> mHistoryList;

    /**
     * Matcher Model Map
     */
    private Map<String, String> mMatcherMap;

    public HistoryManager() {
        mHistoryList = new ArrayList<>();
        mMatcherMap = new HashMap<>();
    }

    public boolean AddChatHistory(ChatHistory ch) {
        for (ChatHistory history : mHistoryList) {
            if (history.IsSame(ch))
                return false;
        }

        mHistoryList.add(ch);
        return true;
    }

    public List<Message> LoadHistory(String with) {
        List<Message> retList = new ArrayList<>();
        for (ChatHistory c : mHistoryList) {
            if (c.With().equals(with) && c.IsLoaded() != 0) {
                retList.addAll(c.GetHistoryList());
            }
        }

        Console.Log(TAG, "Message size: " + retList.size());

        return retList;
    }

    public void CheckDone() {
        Collections.sort(mHistoryList);

        if (mHistoryList.size() < XMPPManager.GetInstance().GetListIQ().rSet.getCount()) {
            XMPPManager.GetInstance().PullList();
        } else {
            XMPPManager.GetInstance().GetListIQ().SetState(ListIQ.FULLY_LOADED);

            for (ChatHistory ch: mHistoryList) {
                Console.Log(TAG, "Chat with: " + ch.With() + "  | Start At: " + ch.StartAt());
            }
        }
    }

    public ChatHistory GetChat(String with, String startAt) {
        if (with.indexOf("@") != -1)
            with = with.substring(0, with.indexOf("@"));

        for (ChatHistory history : mHistoryList) {
            if (history.IsSame(with, startAt))
                return history;
        }

        Console.Log(TAG, "Actually, Shouldn't enter here!!!");
        ChatHistory ch = new ChatHistory(ChatHistory.HISTORY_ELEMENT_NAME, ChatHistory.HISTORY_ELEMENT_NAMESPACE).AddInformation(with, startAt);
        mHistoryList.add(ch);
        return ch;
    }

    public void UpdateMatcherModel() {
        mMatcherMap.clear();
        for (ChatHistory history : mHistoryList) {
            if (mMatcherMap.containsKey(history.With())) {
                String temp = mMatcherMap.get(history.With());
                if (temp.compareTo(history.StartAt()) < 0) {
                    mMatcherMap.put(history.With(), history.StartAt());
                }
            } else {
                mMatcherMap.put(history.With(), history.StartAt());
            }
        }

        mActivity.UpdateMatcherModel(mMatcherMap);
    }

    public void PullHistory() {
        for (ChatHistory history : mHistoryList) {
            XMPPManager.GetInstance().SendIQPacket(history);
        }
    }
}
