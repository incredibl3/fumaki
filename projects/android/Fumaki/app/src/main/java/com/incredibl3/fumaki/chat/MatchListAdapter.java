package com.incredibl3.fumaki.chat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.incredibl3.fumaki.R;
import com.incredibl3.fumaki.model.MatcherModel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

/**
 * Created by Hung on 6/23/2015.
 */
public class MatchListAdapter extends BaseAdapter {
    private Context context;
    private List<MatcherModel> matchListItems;
    private Drawable placehoderDrawable;
    ImageView ivAvatar;

    public MatchListAdapter(Context context, List<MatcherModel> navDrawerItems) {
        this.context = context;
        this.matchListItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return matchListItems.size();
    }

    @Override
    public Object getItem(int position) {
        return matchListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /**
         * The following list not implemented reusable list items as list items
         * are showing incorrect data Add the solution if you have one
         * */

        MatcherModel m = matchListItems.get(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        // Identifying the message owner
        convertView = mInflater.inflate(R.layout.match_list_item, null);

        ivAvatar = (ImageView) convertView.findViewById(R.id.ivAvatar);
        ImageView ivAction = (ImageView) convertView.findViewById(R.id.ivAction);


        Picasso.with(context)
                .load(m.getAvatarUrl())
                .placeholder(placehoderDrawable)
                .error(R.drawable.puck).into(target);

        ivAction.setImageResource(m.getAction() == 0? R.drawable.fuck_new : R.drawable.marry_new);

        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvContent = (TextView) convertView.findViewById(R.id.tvContent);

        tvName.setText(m.getName());
        tvContent.setText(m.getDescription());

        return convertView;
    }


    public Target target = new Target() {
        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }

        @Override
        public void onBitmapLoaded(final Bitmap bitmap, final Picasso.LoadedFrom from) {
            Drawable drawImage = new BitmapDrawable(context.getResources(), bitmap);
            ivAvatar.setImageDrawable(drawImage);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }
    };
}
