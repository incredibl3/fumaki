package com.incredibl3.fumaki;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.JsonObject;
import com.incredibl3.fumaki.Fragment.AFragment;
import com.incredibl3.fumaki.Fragment.AppSettingsFragment;
import com.incredibl3.fumaki.Fragment.EditProfileFragment;
import com.incredibl3.fumaki.Fragment.FbAlbumListFragment;
import com.incredibl3.fumaki.Fragment.FinderSettingsFragment;
import com.incredibl3.fumaki.Fragment.HomeFragment;
import com.incredibl3.fumaki.Fragment.InstructionsFragment;
import com.incredibl3.fumaki.Fragment.LoadingFragment;
import com.incredibl3.fumaki.Fragment.MainSettingsFragment;
import com.incredibl3.fumaki.Fragment.MatchListFragment;
import com.incredibl3.fumaki.Fragment.MessengerFragment;
import com.incredibl3.fumaki.Fragment.NewMatchFragment;
import com.incredibl3.fumaki.Fragment.PersonalProfileFragment;
import com.incredibl3.fumaki.Fragment.ProfileFragment;
import com.incredibl3.fumaki.Fragment.SplashFragment;
import com.incredibl3.fumaki.GCM.QuickstartPreferences;
import com.incredibl3.fumaki.GCM.RegistrationIntentService;
import com.incredibl3.fumaki.XMPP.HistoryManager;
import com.incredibl3.fumaki.XMPP.XMPPManager;
import com.incredibl3.fumaki.model.MatcherModel;
import com.incredibl3.fumaki.model.UserModel;
import com.incredibl3.utils.AlertDialogManager;
import com.incredibl3.utils.Console;
import com.incredibl3.utils.IUtils;
import com.incredibl3.utils.LocationManager;
import com.incredibl3.utils.NetworkStateReceiver;
import com.incredibl3.utils.Stopwatch;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class FumakiActivity extends FragmentActivity
        implements AFragment.OnFragmentCallbackListener, Observer {
    //hungnt PN
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private final String TAG = "Fumaki Activity";
    public RequestController mRequestController;
    protected FumakiApplication mApp;
    protected ProgressDialog pDialog;
    private HomeFragment homeFragment;
    private SplashFragment splashFragment;
    private LoadingFragment loadingFragment;
    private AppSettingsFragment appSettingsFragment;
    private FinderSettingsFragment finderSettingsFragment;
    private MainSettingsFragment mainSettingsFragment;
    private PersonalProfileFragment personalProfileFragment;
    private ProfileFragment profileFragment;
    private NewMatchFragment newMatchFragment;
    private InstructionsFragment instructionsFragment;
    private MessengerFragment messengerFragment;
    private MatchListFragment matchListFragment;
    private EditProfileFragment editProfileFragment;
    private FbAlbumListFragment fbAlbumListFragment;
    private Map<String, Fragment> fragments = new HashMap<String, Fragment>();
    private FumakiUser mUser;
    private boolean isResumed = false;
    private AccessTokenTracker accessTokenTracker;
    private CallbackManager callbackManager;
    private AlertDialogManager mAlertManager = new AlertDialogManager();
    private boolean m_bIsXMPPStarted;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private boolean m_bLocationChecked = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        if (Console.ShouldPrintLogToFile()) {
            mAlertManager.showAlertDialog(FumakiActivity.this, AlertDialogManager.DIALOG_TYPE_THREE_CHOICE, "Log Enable",
                    "Press OK to clear log file. Press \'print log\' to save log file.", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_POSITIVE) {
                                Console.Log(TAG, "Log file is cleared!!!");
                                Console.ClearLogFile();
                            } else if (which == DialogInterface.BUTTON_NEUTRAL) {
                                Console.Log(TAG, "Log file is printed!!!");
                                Console.PrintLogToFile();
                            }
                        }
                    });
        }

        Console.Log(TAG, "onCreate Fumaki Activity");

        // Create IUtils class
        IUtils.setContext(this);
        pDialog = new ProgressDialog(this);

        // Create XMPPManager instance
        XMPPManager.GetInstance().SetContext(this);
        m_bIsXMPPStarted = false;

        // Create HistoryManager Instance
        HistoryManager.GetInstance().SetContext(this);

        // Create Stopwatch instance
        Stopwatch.GetInstance().reset();

        // Track the facebook accesstoken
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                       AccessToken currentAccessToken) {
                Console.Log(TAG, "onCurrentAccessTokenChanged");

                if (currentAccessToken != null) {
                    Console.Log(TAG, "onCurrentAccessTokenChanged 11111111111");
                    // new fb user logged in
                    getUser().destroy();
                    onFbLoginDone();
                }

                if (isResumed) {
                    FragmentManager manager = getSupportFragmentManager();
                    int backStackSize = manager.getBackStackEntryCount();
                    for (int i = 0; i < backStackSize; i++) {
                        manager.popBackStack();
                    }
//                    if (currentAccessToken != null) {
//                        Console.Log(TAG, "111111111111");
////                        mUser.fetchFbInfo();
////
////                        onFbLoginDone();
//                    } else {
//                        goToSplash();
//                    }

//                    ProcessLogin();
                }
            }
        };

        //hungnt PN
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    Console.Log(TAG, "Token retrieved and sent to server! You can now use gcmsender to\n" +
                            "        send downstream messages to this app.");
                } else {
                    Console.Log(TAG, "An error occurred while either fetching the InstanceID token,\n" +
                            "        sending the fetched token to the server or subscribing to the PubSub topic. Please try\n" +
                            "        running the sample again.");
                }
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
        //end

        mUser = FumakiUser.getInstance();
        mUser.addObserver(this);

        mRequestController = new RequestController(this);

        setContentView(R.layout.main);

        InitializeFragments();
    }

    private void CheckLocationStatus() {
        // start location activity to detect current location
        LocationManager.LOCATION_DETECTOR_REQUEST_CODE = 101;

        Intent intent = new Intent(this, LocationManager.class);
        startActivityForResult(intent, LocationManager.LOCATION_DETECTOR_REQUEST_CODE);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));  //hungnt PN

        isResumed = true;

        // Check if internet connection is available
        if (!CheckIfConnectedToInternet())
            return;

        if (!m_bLocationChecked) {
            CheckLocationStatus();
            return;
        } else {
            // ProcessLogin();
            if (AccessToken.getCurrentAccessToken() != null) {

                Console.Log(TAG, "onResume 111111111111");
                onFbLoginDone();
            } else {
                Console.Log(TAG, "onResume 2222222222");
                goToSplash();
            }
        }
    }

    public FumakiUser getUser() {
        return mUser;
    }

    public RequestController getRequestController() {
        return mRequestController;
    }

    public void startXMPP(String userID) {
        if (!m_bIsXMPPStarted) {
            XMPPManager.GetInstance().connect(userID);
            m_bIsXMPPStarted = true;
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        // This method is notified after data changes.
        Console.Log(TAG, "update observer");
        if (getUser().isReady()) {
            Console.Log(TAG, "update observer 11111");
            onUserInfoChanged();
        }
    }

    private void ProcessLogin() {
        // Fetch user profile
        if (AccessToken.getCurrentAccessToken() != null) {
            if (!mUser.isFetchFbInfoDone()) {
                mUser.fetchFbInfo();

                onFbLoginDone();
            }
        } else {
            FragmentManager manager = getSupportFragmentManager();
            int backStackSize = manager.getBackStackEntryCount();

            if (backStackSize == 0)
                goToSplash();
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    public boolean CheckIfConnectedToInternet() {
        if (NetworkStateReceiver.hasConnectivity() == 0) {
            Console.Log(TAG, "No Internet Connection");
            mAlertManager.showAlertDialog(FumakiActivity.this, AlertDialogManager.DIALOG_TYPE_ONE_CHOICE, "Internet Connection Error",
                    "Please connect to working Internet connection", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            CheckIfConnectedToInternet();
                        }
                    });
            return false;
        }

        return true;
    }

    @Override
    public void onPause() {
        //hungnt PN
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
        isResumed = false;

        XMPPManager.GetInstance().disconnect();
        m_bIsXMPPStarted = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
        pDialog = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LocationManager.LOCATION_DETECTOR_REQUEST_CODE) {
            boolean result = false;
            if (data != null)
                result = data.getBooleanExtra(LocationManager.LOCATION_AVAILABILITY, false);
            if (result == true) {
                String latitude = data.getStringExtra("LOCATION_LATITUDE");
                String longitude = data.getStringExtra("LOCATION_LONGITUDE");
                if (!latitude.isEmpty() && !longitude.isEmpty()) {
                    Console.Log(TAG, "Receive current location!!!!!! lat = " + latitude + " --- long = " + longitude);

                    getUser().setLocation(latitude, longitude);

                } else {
                    Console.Log(TAG, "Could not detect current location!!!!!!");
                }
            } else {
                Console.Log(TAG, "Error when request location!!! Quit the application!!!!");
                finish();
            }

            m_bLocationChecked = true;
        }
    }

    private void InitializeFragments() {

        //Get the Fragment Manager
        FragmentManager fm = getSupportFragmentManager();

        //Add Splash Fragment
        splashFragment = (SplashFragment) fm.findFragmentById(R.id.splashFragment);
        fragments.put(SplashFragment.TAG, splashFragment);

        //Add Loading Fragment
        loadingFragment = (LoadingFragment) fm.findFragmentById(R.id.loadingFragment);
        fragments.put(LoadingFragment.TAG, loadingFragment);

        //Add Home Fragment
        homeFragment = (HomeFragment) fm.findFragmentById(R.id.homeFragment);
        fragments.put(HomeFragment.TAG, homeFragment);

        //Add App Settings Fragment
        appSettingsFragment = (AppSettingsFragment) fm.findFragmentById(R.id.settingsFragment);
        fragments.put(AppSettingsFragment.TAG, appSettingsFragment);

        //Add Finder Settings Fragment
        finderSettingsFragment = (FinderSettingsFragment) fm.findFragmentById(R.id.finderFragment);
        fragments.put(FinderSettingsFragment.TAG, finderSettingsFragment);

        mainSettingsFragment = (MainSettingsFragment) fm.findFragmentById(R.id.mainSettings);
        fragments.put(MainSettingsFragment.TAG, mainSettingsFragment);

        personalProfileFragment = (PersonalProfileFragment) fm.findFragmentById(R.id.personalProfile);
        fragments.put(PersonalProfileFragment.TAG, personalProfileFragment);

        profileFragment = (ProfileFragment) fm.findFragmentById(R.id.profile);
        fragments.put(ProfileFragment.TAG, profileFragment);

        newMatchFragment = (NewMatchFragment) fm.findFragmentById(R.id.newMatch);
        fragments.put(NewMatchFragment.TAG, newMatchFragment);

        instructionsFragment = (InstructionsFragment) fm.findFragmentById(R.id.instructions);
        fragments.put(InstructionsFragment.TAG, instructionsFragment);

        messengerFragment = (MessengerFragment) fm.findFragmentById(R.id.messenger);
        fragments.put(MessengerFragment.TAG, messengerFragment);

        matchListFragment = (MatchListFragment) fm.findFragmentById(R.id.matchList);
        fragments.put(MatchListFragment.TAG, matchListFragment);

        editProfileFragment = (EditProfileFragment) fm.findFragmentById(R.id.editProfile);
        fragments.put(EditProfileFragment.TAG, editProfileFragment);

        fbAlbumListFragment = (FbAlbumListFragment) fm.findFragmentById(R.id.fbAlbumList);
        fragments.put(FbAlbumListFragment.TAG, fbAlbumListFragment);

        FragmentTransaction transaction = fm.beginTransaction();

        for (Object key : fragments.keySet()) {
            transaction.hide((Fragment) (fragments.get((String) key)));
        }

        transaction.commit();
    }

    private void showFragment(String fragmentName, boolean addToBackStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        Iterator it = fragments.entrySet().iterator();
        for (Object key : fragments.keySet()) {
            if (key.equals(fragmentName))
                transaction.show((Fragment) (fragments.get((String) key)));
            else
                transaction.hide((Fragment) (fragments.get((String) key)));
        }

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }

        // TODO: http://www.androiddesignpatterns.com/2013/08/fragment-transaction-commit-state-loss.html
        transaction.commit();
    }

    public void goToSplash() {
        showFragment(SplashFragment.TAG, false);
    }

    public void goToLoading() {
        showFragment(LoadingFragment.TAG, false);
    }

    public void goToHome() {
        showFragment(HomeFragment.TAG, false);
    }

    public void goToMainSettings() {
        showFragment(MainSettingsFragment.TAG, true);
    }

    public void goToAppSettings() {
        showFragment(AppSettingsFragment.TAG, true);
    }

    public void goToFinderSettings() {
        showFragment(FinderSettingsFragment.TAG, true);
    }

    public void goToPersonalProfile() {
        personalProfileFragment.getInterests();
        showFragment(PersonalProfileFragment.TAG, true);
    }

    public void goToProfile(UserModel userModel) {
        profileFragment.setCurrentUser(userModel);
        showFragment(ProfileFragment.TAG, true);
    }

    public void goToNewMatch() {
        showFragment(NewMatchFragment.TAG, true);
    }

    public void goToInstructions() {
        showFragment(InstructionsFragment.TAG, true);
    }

    public void goToMessenger(String userID) {
        showFragment(MessengerFragment.TAG, true);
        messengerFragment.setCurrentWithUser(userID);
    }

    public void goToMatchList() {
        showFragment(MatchListFragment.TAG, true);
    }

    public void goToEditProfile() {
        showFragment(EditProfileFragment.TAG, true);
    }

    public void goToFbAlbumList() {
        showFragment(FbAlbumListFragment.TAG, true);
    }

    public void onFbLoginDone() {
        Console.Log(TAG, "onFbLoginDone");
        goToLoading();
        getUser().fetchFbInfo();
//        loadingFragment.fetchUserBasicInfo();
//        loadingFragment.fetchUserAlbums();
    }

    public void onLoginToServerDone(JsonObject response) {
        getUser().setAuthorized();

        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        String strToken = sharedPreferences.getString(QuickstartPreferences.TOKEN_STRING, "");
        Console.Log(TAG, "hungnt onLoginToServerDone GCM ID: " + strToken);
        getUser().setGCMToken(strToken);

        if (response.get("is_new_user").getAsInt() == 1)
            this.createUser();
        else
            this.updateUserInfo();
    }

    public void onCreateUser(UserModel model) {
//        goToLoading();
        getUser().setModel(model);
        this.seekCandidates();
    }

    public void onUpdateUserInfo(UserModel model) {
//        goToLoading();
        getUser().setModel(model);
        this.seekCandidates();
    }

    public void onUserInfoChanged() {

        String username = mUser.getModel().getUserID();
        String password = "";
        if (AccessToken.getCurrentAccessToken() != null)
            password = AccessToken.getCurrentAccessToken().getToken();
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            Console.Log(TAG, " onUserInfoChanged: username or password is NULL. This is UNEXPECTED behavior!!!");
            return;
        }
        // concatenate username and password with colon for authentication
        final String credentials = username + ":" + password;
        mRequestController.UpdateAuthorizationInfo(credentials);


        startXMPP(username);

//        if(!fbLoggedIn || !m_bLocationDetected) {
//            if(needToLoginToServer)
//                Console.Log(TAG, " This case is very weird! Please REVIEW!!!!!!!!!!!!!");
//
//            return;
//        }
        if (getUser().isAuthorized() == false /*&& fbLoggedIn && m_bLocationDetected*/) {
            this.loginToServer();
        } else
            this.seekCandidates();
    }

    public void onSeekCandidatesDone(UserModel[] candidates) {
        this.getFriends();
        //matchListFragment.setMatchers(candidates); //Just demo, push all candidate to Match List to test chat function
        goToHome();
        homeFragment.setCandidates(candidates);
//        goToNewMatch(); //hungnt test new match screen
//        goToInstructions(); //hungnt test instructions screen
//        goToMessenger(); //hungnt test messenger
//		  goToMatchList(); //hungnt test Match List
    }

    public void onSeekMatchersDone(MatcherModel[] matchers) {
        matchListFragment.setMatchers(matchers);
        goToMatchList();
    }

    public void onGetFriendsDone(MatcherModel[] matchers) {
        matchListFragment.setMatchers(matchers);
    }

    public void onDeleteMatchesDone() {
        matchListFragment.setMatchers(null);
        Toast.makeText(this.getApplicationContext(), "Deleted all your matches", Toast.LENGTH_SHORT).show();
    }

    public void onCreateRequests() {
        this.seekMatchers();
    }

    public void ReceiveMessage() {
        messengerFragment.notifyAdapter();
    }

    public void UpdateMatcherModel(Map<String, String> mMap) {
        matchListFragment.UpdateMatcherModel(mMap);
    }

    //========================================================================================

    public void loginToServer() {
        getRequestController().loginToServer();
    }

    public void createUser() {
        getRequestController().createUser(getUser().getModel());
    }

    public void updateUserInfo() {
        getRequestController().updateUserInfo(getUser().getModel());
    }

    public void seekCandidates() {
        getRequestController().seekCandidates();
    }

    public void getFriends() {
        getRequestController().getFriends();
    }

    public void seekMatchers() {
        getRequestController().getMatchers();
    }
}
