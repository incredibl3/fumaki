package com.incredibl3.fumaki.chat;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import com.incredibl3.fumaki.FumakiActivity;
import com.incredibl3.fumaki.XMPP.HistoryManager;
import com.incredibl3.utils.Console;
import com.incredibl3.utils.IUtils;
import com.incredibl3.utils.Stopwatch;

import org.jivesoftware.smack.chat.Chat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by tien.dinhvan on 6/30/2015.
 */
public class Conversation {
    public final static String TAG = "Conversation";

    private Chat mChat = null;
    private String withUser;
    private List<Message> listMessages;
    private List<Message> listHistory;
    private List<Message> currentChat;
    private boolean IsDirty = false;
    private String mLastReceive = "";

    FumakiActivity mActivity;

    public Conversation(String withUser, Chat chat, FumakiActivity activity) {
        this.withUser = withUser;
        this.mActivity = activity;
        if (chat != null) {
            Console.Log("Conversation", "Create conversation with: " + withUser + "Chat is: " + chat.getThreadID());
            this.mChat = chat;
        } else {
            Console.Log("Conversation", "Create conversation with: " + withUser);
        }

        listMessages = new ArrayList<>();
        listHistory = new ArrayList<>();
        currentChat = new ArrayList<>();

        LoadHistory();
    }

    public void LoadHistory() {
        listHistory = HistoryManager.GetInstance().LoadHistory(withUser);
        MergeListItems(listHistory, listMessages);

        Console.Log(TAG, "History is: " + listHistory.size());
    }

    public void MergeListItems(List<Message> first, List<Message> second) {
        currentChat.clear();
        currentChat.addAll(first);
        currentChat.addAll(second);
    }

    public Chat ChatInstance() { return mChat; }

    public void AddMessage(org.jivesoftware.smack.packet.Message msg) {
        if (msg.getBody().length() <= 0)
            return;

        com.incredibl3.fumaki.chat.Message fmsg = new com.incredibl3.fumaki.chat.Message(msg.getFrom(), msg.getBody(), false);
        listMessages.add(fmsg);
        MergeListItems(listHistory, listMessages);

        IsDirty = true;
        SetLastReceive(Stopwatch.GetInstance().getElapsedTimeString());

        mActivity.ReceiveMessage();

        //PlayBeep();
    }

    public void AddMessage(Message m) {
        listMessages.add(m);
        MergeListItems(listHistory, listMessages);
        Console.Log(TAG, "");
    }

    public void AddHistory(Map<String, Message> m){
        for (String key: m.keySet()) {
            Message mes = m.get(key);
            listHistory.add(mes);
        }

        Collections.sort(listHistory);
        MergeListItems(listHistory, listMessages);

        mActivity.ReceiveMessage();
    }

    public void setChat(Chat c) { mChat = c; }

    public Chat getChat() { return mChat; }

    public List<Message> GetMessageList() { return currentChat; }

    /**
     * Plays device's default notification sound
     * */
    public void PlayBeep() {
        try {
            Uri notification = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(IUtils.getContext(),
                    notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetLastReceive(String last) { mLastReceive = last; }

    public String GetLastReceive() { return mLastReceive; }
}
