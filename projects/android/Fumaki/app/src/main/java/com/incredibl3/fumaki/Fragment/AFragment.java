package com.incredibl3.fumaki.Fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.google.gson.JsonObject;
import com.incredibl3.fumaki.model.MatcherModel;
import com.incredibl3.fumaki.model.UserModel;

/**
 * Created by tien.dinhvan on 4/29/2015.
 */
public class AFragment extends Fragment {

    public static final String COMMAND = "command";
    public static final int SWITCH = 0;

    public static final String TOFRAGMENT = "to_frag";
    public static final String DATA = "data";

    OnFragmentCallbackListener mCallback;

    public interface OnFragmentCallbackListener {

        // navigation callbacks
        public void goToAppSettings();
        public void goToFinderSettings();
        public void goToMainSettings();
        public void goToPersonalProfile();
        public void goToProfile(UserModel user);
        public void goToHome();
        public void goToSplash();
        public void goToLoading();
        public void goToNewMatch();
        public void goToMessenger(String userid);
        public void goToMatchList();
        public void goToEditProfile();
        public void goToFbAlbumList();

        // network callbacks
        public void onFbLoginDone();
        public void onUserInfoChanged();
        public void onCreateUser(UserModel model);
        public void onUpdateUserInfo(UserModel model);
        public void onLoginToServerDone(JsonObject response);
        public void onSeekCandidatesDone(UserModel[] candidates);
        public void onSeekMatchersDone(MatcherModel[] matchers);
        public void onGetFriendsDone(MatcherModel[] matchers);
        public void onDeleteMatchesDone();

        public void onCreateRequests();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnFragmentCallbackListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentCallbackListener");
        }
    }
}
