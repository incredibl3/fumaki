package com.incredibl3.fumaki;

/**
 * Created by Hung on 8/25/2015.
 */
public class FbAlbumList {
    private Integer avatar;
    private String name;
    private String content;

    public FbAlbumList(Integer avatar, String name, String content) {
        this.avatar = avatar;
        this.name = name;
        this.content = content;

    }

    public Integer getAvatar() {
        return avatar;
    }

    public void setAvatar(Integer avatar) {
        this.avatar = avatar;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
