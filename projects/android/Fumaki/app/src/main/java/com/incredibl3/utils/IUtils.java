package com.incredibl3.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by Tiendv on 10/05/2015.
 */
public final class IUtils {
    private static Context currentContext 	= null;

    // give your server registration url here
    static final String SERVER_URL = "http://fumaki.ml";

    // Google project id
    static final String SENDER_ID = "982482255858";

    /**
     * Tag used on log messages.
     */
    static final String TAG = "Fumaki GCM";

    static final String DISPLAY_MESSAGE_ACTION =
            "com.incredibl3.fumaki.DISPLAY_MESSAGE";

    static final String EXTRA_MESSAGE = "message";

    /**
     * Set the current and save it into currentContext
     * @param  context the context from the application
     */
    public static void setContext(Context context)
    {
        Console.Log("IUtils","Set context");
        currentContext = context;
    }

    /**
     * Get the current context
     * @param  context the context from the application
     * @return an Context object
     */
    public static Context getContext()
    {
        if (currentContext == null)
        {
            Console.Log("IUtils", "getContext currentContext NOT PREVIOUSLY SET!!!");
        }
        return currentContext;
    }

    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }

    public static String RetrieveHttpResponse(String RequestUrl, boolean isGetMethod) {
        Log.d("IUtils", "getHttpResponse: " + RequestUrl);

        String response_text = null;
        BufferedReader stream_in = null;

        try {
            HttpClient client= new DefaultHttpClient();
            HttpResponse response;

            if (isGetMethod) {
                HttpGet request = new HttpGet(RequestUrl);
                response = client.execute(request);
            } else {
                HttpPost request = new HttpPost(RequestUrl);
                response = client.execute(request);
            }

            stream_in = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
            StringBuffer buffer = new StringBuffer("");
            String line = "";

            while ((line = stream_in.readLine()) != null) {
                buffer.append(line);
            }

            stream_in.close();
            response_text = buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (stream_in != null) {
                try {
                    stream_in.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response_text;
    }

    public static JSONObject RetrieveHttpResponse(String url, boolean isGetMethod, List<NameValuePair> params) {
        JSONParser jsonParser = new JSONParser();

        if (isGetMethod)
            return jsonParser.makeHttpRequest(url, "GET", params);
        else
            return jsonParser.makeHttpRequest(url, "POST", params);
    }

    /**
     * Set a Shared Preference from the application
     * @param key the key in a String representation
     * @param value the value to store in an Object instance
     * @param pname the preference name
     */
    public static void setPreference(String key, Object value, String pname)
    {
        Console.Log("IUtils", "setPreferences(" + key + ", " + value + ", " + pname + ")");
        SharedPreferences settings = IUtils.getContext().getSharedPreferences(pname, 0);
        SharedPreferences.Editor editor = settings.edit();

        if (value instanceof String)
        {
            editor.putString(key, (String) value);
        }
        else if (value instanceof Integer)
        {
            editor.putInt(key, (Integer) value);
        }
        else if (value instanceof Boolean)
        {
            editor.putBoolean(key, (Boolean) value);
        }
        else if(value instanceof Long)
        {
            editor.putLong(key, (Long)value);
        }
        editor.commit();
    }

    /**
     * To read a string from the preferences file.
     * @param key The key that identifies the preferences file.
     * @param pname the preference name
     * */
    public static String getPreferenceString(String key, String pname)
    {
        SharedPreferences settings = IUtils.getContext().getSharedPreferences(pname, 0);
        String res = settings.getString(key, "");
        Console.Log("IUtils", "getPreferenceString(" + key + ", " + res  + ", " + pname + ")");
        return res;

    }

    /**
     * To read a string from the preferences file.
     * @param key The key that identifies the preferences file.
     * @param defaultValue The default value to be returned if the preference does not exist.
     * @param pname the preference name
     * */
    public static String getPreferenceString(String key, String defaultValue, String pname)
    {
        SharedPreferences settings = IUtils.getContext().getSharedPreferences(pname, 0);
        String res = settings.getString(key, defaultValue);
        Console.Log("IUtils", "getPreferenceString(" + key + ", " + defaultValue + " " + res  + ", " + pname + ")");
        return res;

    }

    /**
     * To read an int from the preferences file.
     * @param key The key that identifies the preferences file.
     * @param defaultValue The default value to be returned if the preference does not exist.
     * @param pname the preference name
     * */
    public static int getPreferenceInt(String key, int defaultValue, String pname)
    {
        SharedPreferences settings = IUtils.getContext().getSharedPreferences(pname, 0);
        int res = settings.getInt(key, defaultValue);
        Console.Log("IUtils", "getPreferenceInt(" + key + ", " + defaultValue + " " + res  + ", " + pname + ")");
        return res;
    }

    /**
     * To read a long from the preferences file.
     * @param key The key that identifies the preferences file.
     * @param defaultValue The default value to be returned if the preference does not exist.
     * @param pname the preference name
     * */
    public static boolean getPreferenceBoolean(String key, boolean defaultValue, String pname)
    {
        try
        {
            SharedPreferences settings = IUtils.getContext().getSharedPreferences(pname, 0);
            boolean res = settings.getBoolean(key, defaultValue);
            Console.Log("IUtils", "getPreferenceBoolean(" + key + ", " + defaultValue + " " + res  + ", " + pname + ")");
            return res;
        }catch(Exception ex){
            Console.Log("IUtils", "getPreferenceBoolean(" + key + ", " + defaultValue + " " + false  + ", " + pname + ")");
            return false;}
    }

    /**
     * To read a long from the preferences file.
     * @param key The key that identifies the preferences file.
     * @param defaultValue The default value to be returned if the preference does not exist.
     * @param pname the preference name
     * */
    public static float getPreferenceFloat(String key, float defaultValue, String pname)
    {
        SharedPreferences settings = IUtils.getContext().getSharedPreferences(pname, 0);
        Float res = settings.getFloat(key, defaultValue);
        Console.Log("IUtils", "getPreferenceLong(" + key + ", " + defaultValue + " " + res + ", " + pname + ")");
        return res;
    }

    /**
     * To remove a preference from the preferences file.
     * @param key The key that identifies the preferences file.
     * @param pname the preference name
     * */
    public static void removePreference(String key, String pname)
    {
        Console.Log("IUtils", "removePreference(" + key +  ", " + pname + ")");
        SharedPreferences settings = IUtils.getContext().getSharedPreferences(pname, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }
}
