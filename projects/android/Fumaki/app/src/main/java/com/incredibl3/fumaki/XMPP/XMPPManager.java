package com.incredibl3.fumaki.XMPP;

import android.os.AsyncTask;

import com.incredibl3.fumaki.Config;
import com.incredibl3.fumaki.FumakiActivity;
import com.incredibl3.fumaki.chat.Conversation;
import com.incredibl3.utils.Console;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.StanzaFilter;
import org.jivesoftware.smack.filter.StanzaTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.time.EntityTimeManager;
import org.jivesoftware.smackx.time.packet.Time;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;



/**
 * Responsible for establishing a connection between a client and an XMPP server.
 * Provides authentication and connection methods, and callbacks for handling
 * connection failures.
 * Library used: Smack 4.1
 *
 * Created by Tiendv on 24/06/2015.
 * http://xmpp-tutorials.blogspot.com/
 * https://nikeshpathak.wordpress.com/2015/01/05/chat_application_xmpp_protocol/
 * http://www.programcreek.com/java-api-examples/index.php?api=org.jivesoftware.smack.AccountManager
 *
 */

public class XMPPManager {
    private final String TAG = "XMPPManager";
    /**
     * Main Context of the application
     */
    private FumakiActivity mActivity;
    public void SetContext(FumakiActivity activity) {
        mActivity = activity;
    }

    /**
     * IP address or Domain address from the XMPP Server that will be connected to.
     */
    private String serverAddress;

    /**
     * Port from the XMPP Server that will be connected to.
     */
    private int serverPort = 5222;

    /**
     * Contains all information and methods about the management of the connection.
     */
    private XMPPTCPConnection connection;

    /**
     * User login name without domain name: {userName}@{domainName}
     */
    private String loginUser;

    /**
     * User password
     */
    private String passwordUser;

    /**
     * Domain or Service name
     */
    private String DOMAIN = "fumaki";

    /**
     * Singleton implementation
     */
    private static XMPPManager mInstance;

    public static XMPPManager GetInstance() {
        if (mInstance == null) {
            mInstance = new XMPPManager();
        }

        return mInstance;
    }

    /**
     * Archive Message
     */
    private ListIQ listIQ = null;
    public ListIQ GetListIQ() { return listIQ; }

    /**
     * Time IQ
     */
    private TimeIQ timeIQ = null;
    public TimeIQ GetTimeIQ() { return timeIQ; }

    /**
     * ChatManager
     */
    private ChatManager mChatManager = null;
    private Map<String, Conversation> mConversationMap = new HashMap<String, Conversation>();     // Store chat by id
    private ChatManagerListener chatManagerListener = new ChatManagerListener() {
        @Override
        public void chatCreated(final Chat chat, final boolean createdLocally) {
            Console.Log(TAG, "Chat is created: " + chat.getParticipant());
            if (!createdLocally) {
                String from = chat.getParticipant();
                from = from.substring(0, from.indexOf("@"));
                Console.Log(TAG, "Chat is not created locally: " + from);
                if (mConversationMap.containsKey(from)) {
                    Console.Log(TAG, "Chat is not created locally, but already created by stanza receiver");
                    mConversationMap.get(from).setChat(chat);
                }
                else {
                    Console.Log(TAG, "Chat is not created locally, and we add a new Conversation");
                    mConversationMap.put(from, new Conversation(from, chat, mActivity));
                }
            }
        }
    };

    /**
     * Default constructor
     *
     * @param  serverAddress	{@link serverAddress}
     * @param  loginUser 	{@link loginUser}
     * @param  passwordUser 	{@link passwordUser}
     */
    public XMPPManager(String serverAddress, String loginUser, String passwordUser) {
        this.serverAddress = serverAddress;
        this.loginUser = loginUser;
        this.passwordUser = passwordUser;
    }

    public XMPPManager() {
        this.serverAddress = "fumaki.ga";
    }

    /**
     * Creates an AsyncTask to starts a connection usign serverAddress attribute from this class.
     * It also attach a listener  to handle with changes on connection, like fall down.
     */
    public void connect(final String userID){
        this.loginUser = userID;
        AsyncTask<Void, Void, Boolean> connectionThread = new AsyncTask<Void, Void, Boolean>(){
            @Override
            protected Boolean doInBackground(Void... arg0){
                boolean isConnected = false;
                Console.Log(TAG, "userID: " + userID);
                XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration
                        .builder()
                        .setHost("128.199.109.41")
                        .setPort(5222)
                        .setUsernameAndPassword(userID, Config.FUMAKI_CHAT_USER_PASSWORD)
                        .setCompressionEnabled(false)
                        .setServiceName(DOMAIN)
                        .setDebuggerEnabled(true)
                        .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                        .build();
                //config.setReconnectionAllowed(true);

                connection = new XMPPTCPConnection(config);

                XMPPConnectionListener connectionListener = new XMPPConnectionListener();
                connection.addConnectionListener(connectionListener);

                try{
                    connection.connect();
                    isConnected = true;
                } catch (IOException e){
                    //e.printStackTrace();
                } catch (SmackException e){
                    //e.printStackTrace();
                } catch (XMPPException e){
                    //e.printStackTrace();
                }

                return isConnected;
            }
        };
        connectionThread.execute();
    }

    public void listeningForMessages() {
        Console.Log(TAG, "Listening for Messages");
        StanzaFilter filter = new AndFilter(new StanzaTypeFilter(Message.class));
        PacketCollector collector = connection.createPacketCollector(filter);

        // Next, create a packet listener. We use an anonymous inner class for brevity.
        StanzaListener myListener = new StanzaListener() {
            public void processPacket(Stanza packet) {
                // Do something with the incoming packet here.
                if (packet instanceof Message) {
                    Console.Log(TAG, "Stanza Listener: " + packet.toString());
                    Message m = (Message) packet;

                    String from = m.getFrom();
                    from = from.substring(0, from.indexOf("@"));

                    Conversation c = mConversationMap.get(from);

                    if (c == null) {
                        c = new Conversation(from, null, mActivity);
                        mConversationMap.put(from, c);
                    }

                    c.AddMessage(m);
                }
            }
        };

        // Register the listener.
        connection.addAsyncStanzaListener(myListener, filter);
    }

    public void AddHistoryChat(String with, Map<String, com.incredibl3.fumaki.chat.Message> lMes) {
        Conversation c = mConversationMap.get(with);

        if (c == null) {
            Console.Log(TAG, "What the hell??? History comes before the conversation is created ???");
            return;
        }

        c.AddHistory(lMes);
    }

    /**
     * Disconnect from openfire server
     *
     */
    public void disconnect() {
        try {
            if (connection != null)
                connection.disconnect();
        } catch (Exception e) {

        }
    }

    /**
     * Provides an authentication to the server using an username and a password.
     *
     * @param  connection	{@link connection}
     * @param  loginUser	{@link loginUser}
     * @param  passwordUser	{@link passwordUser}
     */
    private boolean createAccount(final XMPPTCPConnection connection){
        try{
            Console.Log(TAG, "Create account: " + loginUser);
            AccountManager accountManager = AccountManager.getInstance(connection);
            Map<String,String> attributes=new HashMap<String,String>();
            attributes.put("name", mActivity.getUser().getModel().getName());
            accountManager.createAccount(loginUser, Config.FUMAKI_CHAT_USER_PASSWORD, attributes);
        } catch (SmackException.NotConnectedException e){
            Console.Log(TAG, "failed 1");
        } catch (XMPPException e){
            String error = e.getMessage();
            Console.Log(TAG, "failed 2: " + error);

            if (error.indexOf("conflict") != -1) {
                Console.Log(TAG, "Account already created");
                try {
                    connection.login();
                } catch (SmackException exp) {
                    exp.printStackTrace();
                } catch (IOException exp) {
                    exp.printStackTrace();
                } catch (XMPPException exp) {
                    exp.printStackTrace();
                }
            }

            return false;
        } catch (SmackException e){
            Console.Log(TAG, "failed 3");
        }

        return true;
    }

    /**
     * Listener for changes in connection
     * @see ConnectionListener from org.jivesoftware.smack
     */
    public class XMPPConnectionListener implements ConnectionListener{
        @Override
        public void connected(final XMPPConnection connection){
            Console.Log(TAG, "XMPPConnectionListener connected!!!");
            if(!connection.isAuthenticated()) {
                Console.Log(TAG, "onConnected but not yet authenticated!!! " + connection.getHost());
                if (createAccount((XMPPTCPConnection) connection)) {
                    disconnect();
                    new Timer().schedule(new TimerTask()
                    {
                        @Override
                        public void run()
                        {
                            Console.Log(TAG, "reconnect with: " + loginUser);
                            connect(loginUser);
                        }
                    }, 5 * 1000);
                }
            }
        }
        @Override
        public void authenticated(XMPPConnection arg0, boolean b){
            Console.Log(TAG, "authenticated, Logged in as " + connection.getUser());

            // Create Service provider for Chat history
            ServiceProviders.Register_Providers(new ProviderManager());

            // Request List of conversation
            listIQ = new ListIQ("list", "urn:xmpp:archive");
            listIQ.setType(IQ.Type.get);
            SendIQPacket(listIQ);

            // Get Server Time
            timeIQ = new TimeIQ();
            timeIQ.setType(IQ.Type.get);
            SendIQPacket(timeIQ);

            // Start listening to chat created
            mChatManager = ChatManager.getInstanceFor(connection);
            mChatManager.addChatListener(chatManagerListener);

            // Listen to packet
            listeningForMessages();
        }
        @Override
        public void connectionClosed(){}
        @Override
        public void connectionClosedOnError(Exception arg0){}
        @Override
        public void reconnectingIn(int arg0){}
        @Override
        public void reconnectionFailed(Exception arg0){}
        @Override
        public void reconnectionSuccessful(){}
    }

    public Conversation CreateConversation(String withUser) {
        if (mChatManager != null) {
            Console.Log(TAG, "Connect with user: " + withUser + "! Why logged in as: " + loginUser);

            Chat chat = null;
            Conversation conversation;
            if (mConversationMap.containsKey(withUser)) {
                conversation = mConversationMap.get(withUser);
                if (mChatManager.getThreadChat(conversation.getChat().getThreadID()) == null) {
                    Console.Log(TAG, "Chat already activated");
                    chat = mChatManager.createChat(withUser + "@" + DOMAIN, conversation.getChat().getThreadID(), null);
                    if (chat != null) {
                        conversation.setChat(chat);
                    }
                } else {
                    Console.Log(TAG, "Chat already created, re-create it");
                    return conversation;
                }
            } else {
                chat = mChatManager.createChat(withUser + "@" + DOMAIN);
                conversation = new Conversation(withUser, chat, mActivity);
                mConversationMap.put(withUser, conversation);
            }

            final Chat chat2 = chat;
            if (chat2 != null) {
                // The chat is actually created - chatManagerListener function is called
                // only when a user starts sending a message. Therefore I send an empty message.
                /*try {
                    chat2.sendMessage("");
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }*/
            }
            return conversation;

        } else {
            Console.Log(TAG, "Chat Manager is null !!!");
        }

        return null;
    }

    public void PullList() {
        listIQ.SetState(ListIQ.PARTLY_LOADED);
        listIQ.SetAfter(listIQ.rSet.getLast());
        SendIQPacket(listIQ);
    }

    public void SendIQPacket(IQ iq) {
        try {
            connection.sendStanza(iq);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }
}
