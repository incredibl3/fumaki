package com.incredibl3.fumaki.chat;

/**
 * Created by Hung on 6/23/2015.
 */
public class Message implements Comparable<Message> {
    private String fromName, message;
    private boolean isSelf;
    private String timeStamp;

    public Message() {
    }

    public Message(String fromName, String message, boolean isSelf) {
        this.fromName = fromName;
        this.message = message;
        this.isSelf = isSelf;
        this.timeStamp = "";
    }

    public void setTimeStamp(String timeStamp) { this.timeStamp = timeStamp; }

    public String getTimeStamp() { return timeStamp; }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setSelf(boolean isSelf) {
        this.isSelf = isSelf;
    }

    public int compareTo(Message m) {
        return this.timeStamp.compareTo(m.timeStamp);
    }
}
