package com.incredibl3.fumaki;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.incredibl3.fumaki.model.UserModel;
import com.incredibl3.utils.Console;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by dungnx on 9/8/15.
 */
public class FumakiUser extends Observable implements Observer{

    private static FumakiUser mInstance = null;
    private final String TAG = "FumakiUser";

    private Observer mObserver;

    private UserModel mModel;
    private boolean mIsNew;
    private ArrayList<PhotoAlbum> mPhotoAlbums;

    private boolean mFetchBasicInfoDone;
    private boolean mFetchPhotoAlbumsListDone;
    private boolean mFetchAvatarPictureDone;

    private boolean mIsAuthorized;

    private FumakiUser(){
        mIsAuthorized = false;
        mModel = new UserModel();
        mIsNew = false;
        mFetchBasicInfoDone = false;
        mFetchPhotoAlbumsListDone = false;
        mFetchAvatarPictureDone = false;
        mPhotoAlbums = new ArrayList<PhotoAlbum>();
    }

    public static FumakiUser getInstance( ) {
        if(mInstance == null)
            mInstance = new FumakiUser();

        return mInstance;
    }

    public void destroy(){
        mIsAuthorized = false;
        mModel = new UserModel();
        mIsNew = false;
        mFetchBasicInfoDone = false;
        mFetchPhotoAlbumsListDone = false;
        mFetchAvatarPictureDone = false;
        mPhotoAlbums = null;
    }

    @Override
    public void update(Observable observable, Object data) {
        Console.Log(TAG, "Profile Pictures observe");
        if(observable instanceof PhotoAlbum) {
            Console.Log(TAG, "Profile Pictures observe 1");
            PhotoAlbum album = (PhotoAlbum) observable;
            if (data.equals("Profile Pictures")) {
                Console.Log(TAG, "Profile Pictures observe 2222");
                mFetchAvatarPictureDone = true;
                mPhotoAlbums.add(0, (PhotoAlbum) observable);
                updateProfilePicsToModel();
                setChanged();
                notifyObservers();
            } else {
                mPhotoAlbums.add((PhotoAlbum) observable);
            }
        }
    }

    public void fetchFbInfo(){
        Console.Log(TAG, "fetchFbInfo 11111");
        if(!mFetchBasicInfoDone) {
            Console.Log(TAG, "fetchFbInfo 222222222");
            fetchUserBasicInfo();
        }

        if(!mFetchPhotoAlbumsListDone) {
            Console.Log(TAG, "fetchFbInfo 3333333333");
            fetchPhotoAlbums();
        }
    }

    public UserModel getModel(){
        return mModel;
    }

    public void setModel(UserModel model){
        mModel = model;
    }

    public void setLocation(String lattitude, String longitude) {
        mModel.setLatitude(lattitude);
        mModel.setLongitude(longitude);
        setChanged();
        notifyObservers();
    }

    public boolean isAuthorized(){
        return mIsAuthorized;
    }

    public void setAuthorized(){
        mIsAuthorized = true;
    }

    public boolean isNewUser(){
        return mIsNew;
    }

    public void setIsNewUser(){
        mIsNew = true;
        updateProfilePicsToModel();
    }

    public boolean isReady(){
//        return (mFetchBasicInfoDone && mFetchPhotoAlbumsListDone && mFetchAvatarPictureDone
//                && mModel.getLatitude() != null && mModel.getLongitude() != null);
        return (mFetchBasicInfoDone && mFetchPhotoAlbumsListDone
                && mModel.getLatitude() != null && mModel.getLongitude() != null);
    }

    public boolean isFetchFbInfoDone(){
        return (mFetchBasicInfoDone && mFetchPhotoAlbumsListDone);
    }

    private void fetchUserBasicInfo(){
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject me, GraphResponse response) {
                        try {

                            mModel.setUserID(me.optString("id"));
                            mModel.setName(me.optString("name"));
                            mModel.setGender(me.optString("gender").equals("male") ? 0 : 1);

                            String birthdayStr = me.optString("birthday");
                            if (!birthdayStr.isEmpty()) {
                                mModel.setBirthday(birthdayStr);
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        mFetchBasicInfoDone = true;
                        setChanged();
                        Console.Log(TAG, "fetchUserBasicInfo Done");
                        notifyObservers("fetchUserBasicInfo Done");

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,birthday,gender");
        request.setParameters(parameters);
        GraphRequest.executeBatchAsync(request);
    }

    private void fetchPhotoAlbums(){
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + AccessToken.getCurrentAccessToken().getUserId() + "/albums",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        JSONArray albums = response.getJSONObject().optJSONArray("data");

                        String profilePics_album_id = null;
                        for (int i = 0; i < albums.length(); i++){
                            String album_id = null;
                            String album_name = null;
                            try {
                                album_id = albums.getJSONObject(i).optString("id");
                                album_name = albums.getJSONObject(i).optString("name");

                                PhotoAlbum album = new PhotoAlbum(album_id, album_name);
                                album.addObserver(FumakiUser.getInstance());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        //updateProfilePicsToModel();

                        mFetchPhotoAlbumsListDone = true;
                        setChanged();
                        Console.Log(TAG, "fetchPhotoAlbums Done");
                        notifyObservers("fetchPhotoAlbums Done");
                    }
                }
        ).executeAsync();
    }

    private void updateProfilePicsToModel(){
        PhotoAlbum profilePicsAlbum = mPhotoAlbums.get(0);
        try{
            mModel.setAvatarUrl(profilePicsAlbum.getImageUrls().get(0));
            mModel.setProfilePic0Url(profilePicsAlbum.getImageUrls().get(1));
            mModel.setProfilePic1Url(profilePicsAlbum.getImageUrls().get(2));
            mModel.setProfilePic2Url(profilePicsAlbum.getImageUrls().get(3));
            mModel.setProfilePic3Url(profilePicsAlbum.getImageUrls().get(4));
            mModel.setProfilePic4Url(profilePicsAlbum.getImageUrls().get(5));
            mModel.setProfilePic5Url(profilePicsAlbum.getImageUrls().get(6));
        } catch (Exception e){

        }
    }

    public void setGCMToken(String gcmID) {
        mModel.setGcmID(gcmID);
    }


    private class PhotoAlbum extends Observable {
        private String mAlbumId;
        private String mAlbumName;
        // index 0 is cover picture
        private ArrayList<String> mImageUrls;

        public PhotoAlbum(String id, String name){
            mAlbumId = id;
            mAlbumName = name;
            mImageUrls = null;
            fetchAlbumPhotos();
        }

        public ArrayList<String> getImageUrls(){
            return mImageUrls;
        }

        private void fetchAlbumPhotos(){

            GraphRequest request = GraphRequest.newGraphPathRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/" + mAlbumId,
                    new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(GraphResponse response) {
                            try {
                                final String cover_photo_id = response.getJSONObject().getJSONObject("cover_photo").optString("id");
                                GraphRequest request = GraphRequest.newGraphPathRequest(
                                        AccessToken.getCurrentAccessToken(),
                                        "/" + mAlbumId + "/photos",
                                        new GraphRequest.Callback() {
                                            @Override
                                            public void onCompleted(GraphResponse response) {
                                                JSONArray data = response.getJSONObject().optJSONArray("data");
                                                for(int i = 0; i < data.length(); i++){
                                                    try {
                                                        String photo_id = data.getJSONObject(i).optString("id");
                                                        // the first element is the biggest image
                                                        String biggest_image_url = data.getJSONObject(i).getJSONArray("images").getJSONObject(0).optString("source");

                                                        if(mImageUrls == null)
                                                            mImageUrls = new ArrayList<String>();

                                                        if(cover_photo_id.equalsIgnoreCase(photo_id)) // cover picture
                                                            mImageUrls.add(0, biggest_image_url);
                                                        else
                                                            mImageUrls.add(biggest_image_url);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                                setChanged();
                                                notifyObservers(mAlbumName);
                                            }
                                        });

                                Bundle parameters = new Bundle();
                                parameters.putString("fields", "images");
                                request.setParameters(parameters);
                                request.executeAsync();
                            }catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "cover_photo");
            request.setParameters(parameters);
            request.executeAsync();


        }
    }
}
