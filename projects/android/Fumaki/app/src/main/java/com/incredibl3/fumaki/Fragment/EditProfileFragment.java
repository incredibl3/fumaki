package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.facebook.CallbackManager;
import com.incredibl3.fumaki.R;

/**
 * Created by Tiendv on 28/04/2015.
 */
public class EditProfileFragment extends AFragment {
    public static String TAG = "EditProfileFragment";


    private CallbackManager callbackManager;
    private ImageView ivAddMoreAvatar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_profile, container, false);

        ivAddMoreAvatar = (ImageView)view.findViewById(R.id.ivAddMoreAvatar);
        ivAddMoreAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Go to list
                mCallback.goToFbAlbumList();
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onResume(){
        super.onResume();
    }

}
