package com.incredibl3.fumaki.XMPP;

import com.incredibl3.fumaki.chat.ChatHistory;
import com.incredibl3.fumaki.chat.Message;
import com.incredibl3.utils.Console;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by tien.dinhvan on 7/17/2015.
 */
public class ChatIQProvider extends IQProvider {

    public final static String TAG = "ChatIQProvider";

    public ChatIQProvider()
    {
    }


    @Override
    public IQ parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {

        boolean done = false;

        String with = parser.getAttributeValue("", "with");
        String startAt = parser.getAttributeValue("", "start");
        Console.Log(TAG, "Parse at: " + with + " " + startAt);
        ChatHistory ch = HistoryManager.GetInstance().GetChat(with, startAt);

        String secs = "";
        Message m = new Message();
        boolean isSelf = false;
        while (!done)
        {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {

                if (parser.getName().equals("from")) {
                    m = new Message();
                    m.setFromName(with);
                    secs = parser.getAttributeValue("", "secs");
                    isSelf = false;
                } else if (parser.getName().equals("to")) {
                    m = new Message();
                    m.setFromName(with);
                    secs = parser.getAttributeValue("", "secs");
                    isSelf = true;
                } else if(parser.getName().equals("body")) {
                    if (m != null) {
                        m.setMessage(parser.nextText());
                        m.setTimeStamp(secs);
                        m.setSelf(isSelf == true);
                    } else {
                        Console.Log(TAG, "Oops!!! Trying to parse body before initialize the message");
                    }
                } else if (parser.getName().equals("first")) {
                    int index = parseInt(parser.getAttributeValue("", "index"));
                    ch.rSet.setIndexAtt(index);
                    int first = parseInt(parser.nextText());
                    ch.rSet.setFirst(first);
                } else if (parser.getName().equals("last")) {
                    int last = parseInt(parser.nextText());
                    ch.rSet.setLast(last);
                } else if (parser.getName().equals("count")) {
                    int count = parseInt(parser.nextText());
                    ch.rSet.setCount(count);
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getName().equals("chat")) {
                    ch.HistoryLoaded();
                    done = true;
                } else if (parser.getName().equals("to") || parser.getName().equals("from")) {
                    if (!m.getMessage().isEmpty())
                        ch.AddMessage(secs, m);
                }
            }
        }

        return null;
    }

    private int parseInt(String integer)
    {
        return Integer.parseInt((integer != null ? integer : "0"));
    }
}
