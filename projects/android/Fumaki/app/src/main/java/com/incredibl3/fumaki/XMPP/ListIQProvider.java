package com.incredibl3.fumaki.XMPP;

import com.incredibl3.fumaki.chat.ChatHistory;
import com.incredibl3.utils.Console;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by tien.dinhvan on 7/17/2015.
 */
public class ListIQProvider extends IQProvider {

    public ListIQProvider() {
    }

    @Override
    public IQ parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {
        Console.Log("ListIQProvider", "Enter parse function");
        boolean done = false;
        ListIQ iq = XMPPManager.GetInstance().GetListIQ();

        String with = "", start = "";
        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {
                if (parser.getName().equals("chat")) {
                    with = parser.getAttributeValue("", "with");
                    start = parser.getAttributeValue("", "start");
                    ChatHistory ch = new ChatHistory(ChatHistory.HISTORY_ELEMENT_NAME, ChatHistory.HISTORY_ELEMENT_NAMESPACE).AddInformation(with, start);
                    HistoryManager.GetInstance().AddChatHistory(ch);
                } else if (parser.getName().equals("first")) {
                    int index = parseInt(parser.getAttributeValue("", "index"));
                    int first = parseInt(parser.nextText());
                    iq.rSet.setIndexAtt(index);
                    iq.rSet.setFirst(first);
                } else if (parser.getName().equals("last")) {
                    int last = parseInt(parser.nextText());
                    iq.rSet.setLast(last);
                } else if (parser.getName().equals("count")) {
                    int count = parseInt(parser.nextText());
                    iq.rSet.setCount(count);
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getName().equals("list")) {
                    HistoryManager.GetInstance().CheckDone();
                    done = true;
                }
            }
        }

        // Only start pulling history after done with loading the whole list
        if (XMPPManager.GetInstance().GetListIQ().GetState() == ListIQ.FULLY_LOADED) {
            HistoryManager.GetInstance().UpdateMatcherModel();
            HistoryManager.GetInstance().PullHistory();
        }

        return null;
    }

    private int parseInt(String integer) {
        return Integer.parseInt((integer != null ? integer : "0"));
    }
}
