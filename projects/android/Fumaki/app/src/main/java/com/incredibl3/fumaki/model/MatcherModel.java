
package com.incredibl3.fumaki.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.incredibl3.utils.Console;

public class MatcherModel implements Comparable<MatcherModel> {

    @Expose
    private String userID;
    @Expose
    private String name;
    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;
    @Expose
    private String birthday;
    @Expose
    private int gender;
    @Expose
    private String description;
    @Expose
    private String latitude;
    @Expose
    private String longitude;
    @Expose
    private int action;

    private String lastChat;

    /**
     * 
     * @return
     *     The userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     * 
     * @param userID
     *     The userID
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The avatarUrl
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }

    /**
     * 
     * @param avatarUrl
     *     The avatar_url
     */
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    /**
     * 
     * @return
     *     The birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * 
     * @param birthday
     *     The birthday
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * 
     * @return
     *     The gender
     */
    public int getGender() {
        return gender;
    }

    /**
     * 
     * @param gender
     *     The gender
     */
    public void setGender(int gender) {
        this.gender = gender;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 
     * @return
     *     The action
     */
    public int getAction() {
        return action;
    }

    /**
     * 
     * @param action
     *     The action
     */
    public void setAction(int action) {
        this.action = action;
    }

    /**
     *
     * @return
     *     The lastchat
     */
    public String getLastChat() {
        return lastChat;
    }

    /**
     *
     * @param userID
     *     The userID
     */
    public void setLastChat(String lastChat) {
        this.lastChat = lastChat;
    }

    public int compareTo(MatcherModel mm) {

        if (mm.getLastChat() == null)
            return -1;

        if (this.lastChat == null)
            return 1;

        return mm.lastChat.compareTo(this.lastChat);
    }
}
