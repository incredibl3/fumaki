package com.incredibl3.fumaki.XMPP;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.util.XmppDateTime;

import java.util.Date;

/**
 * Created by tien.dinhvan on 7/22/2015.
 */
public class TimeIQ extends IQ {
    private String utc;
    public String getUtc() { return utc; }
    public void setUtc(String utc) { this.utc = utc; }

    public TimeIQ() {
        super("query", "jabber:iq:time");
    }

    public Date getTime() {
        Date date = null;
        try {
            date = XmppDateTime.parseDate(utc);
        }
        catch (Exception e) {

        }
        return date;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();
        return xml;
    }
}
