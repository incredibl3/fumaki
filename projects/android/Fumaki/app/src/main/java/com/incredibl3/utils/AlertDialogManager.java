package com.incredibl3.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.incredibl3.fumaki.R;

/**
 * Created by Tiendv on 09/05/2015.
 */
public class AlertDialogManager {
    public static int DIALOG_TYPE_ONE_CHOICE = 0;
    public static int DIALOG_TYPE_TWO_CHOICE = 1;
    public static int DIALOG_TYPE_THREE_CHOICE = 2;

    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     *               - pass null if you don't want icon
     * */
    public static void showAlertDialog(Context context, int type, String title, String message,
                                       DialogInterface.OnClickListener onClick) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        // Setting Dialog Title
        builder.setTitle(title);

        // Setting Dialog Message
        builder.setMessage(message);

        //if(status != null)
            // Setting alert dialog icon
            //alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        builder.setPositiveButton("OK", onClick);
        if (type == DIALOG_TYPE_TWO_CHOICE) {
            builder.setNegativeButton("NO", onClick);
        } else if (type == DIALOG_TYPE_THREE_CHOICE) {
            builder.setNegativeButton("NO", onClick);
            builder.setNeutralButton("Print Log", onClick);
        }

        // Create + Show Alert Message
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
