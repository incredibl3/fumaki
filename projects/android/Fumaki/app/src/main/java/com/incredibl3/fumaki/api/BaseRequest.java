package com.incredibl3.fumaki.api;

import com.google.gson.JsonObject;
import com.incredibl3.fumaki.FumakiActivity;
import com.incredibl3.fumaki.RequestController;
import com.incredibl3.fumaki.model.MatcherModel;
import com.incredibl3.fumaki.model.UserModel;
import com.incredibl3.utils.Console;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by tien.dinhvan on 5/27/2015.
 */
public class BaseRequest<T2> {
    public final String TAG = "Base Request";
    public RequestController mController;

    public static final int OP_LOGIN_TO_SERVER = 1001;
    public static final int OP_CREATE_NEW_USER = 1002;
    public static final int OP_UPDATE_USER_INFO = 1003;
    public static final int OP_SEEK_CANDIDATE = 1004;
    public static final int OP_GET_MATCHERS = 1005;
    public static final int OP_GET_FRIENDS = 1006;
    public static final int OP_DELETE_MATCHES = 1007;

    private int Type;

    public Callback<T2> mCallback = new Callback<T2>() {
        @Override
        public void success (T2 t2, Response response){
            onSuccess(t2);
        }

        @Override
        public void failure (RetrofitError error){
            mController.DeleteRequest(Type);
            Console.Log(TAG, " Retrofit error = " + error.toString());
        }
    };

    public BaseRequest(RequestController ReqController, int type) {
        this.mController = ReqController;
        this.Type = type;
    }

    public int GetType() { return Type; }

    public void onSuccess(T2 t2) {
        mController.DeleteRequest(Type);

        switch (Type) {
            case OP_LOGIN_TO_SERVER:
                mController.getActivity().onLoginToServerDone((JsonObject)t2);
                break;
            case OP_CREATE_NEW_USER:
                mController.getActivity().onCreateUser((UserModel) t2);
                break;
            case OP_UPDATE_USER_INFO:
                mController.getActivity().onUpdateUserInfo((UserModel) t2);
                break;
            case OP_SEEK_CANDIDATE:
                mController.getActivity().onSeekCandidatesDone((UserModel[]) t2);
                break;

            case OP_GET_MATCHERS:
                mController.getActivity().onSeekMatchersDone((MatcherModel[]) t2);
                break;

            case OP_GET_FRIENDS:
                mController.getActivity().onGetFriendsDone((MatcherModel[]) t2);
                break;
            case OP_DELETE_MATCHES:
                mController.getActivity().onDeleteMatchesDone();
                break;
        }
    }
}
