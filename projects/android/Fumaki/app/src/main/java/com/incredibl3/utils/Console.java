package com.incredibl3.utils;

import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.incredibl3.fumaki.Config;

import java.io.File;
import java.io.IOException;

/**
 * Created by tien.dinhvan on 5/18/2015.
 */
public class Console {
    public static void Log(String TAG, String message) {
        if (Config.DEBUG_CONSOLE)
            Log.d(TAG, message);
    }

    public static boolean IsLogEnabled() {
        return Config.DEBUG_CONSOLE;
    }

    public static boolean ShouldPrintLogToFile() {
        return Config.DEBUG_TO_FILE;
    }

    public static void ClearLogFile() {
        try {
            Runtime.getRuntime().exec("logcat -c");
        } catch (IOException e) {
            Toast.makeText(IUtils.getContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public static void PrintLogToFile() {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), "Fumaki" + String.valueOf(System.currentTimeMillis()));
            Runtime.getRuntime().exec("logcat -d -v time -f " + file.getAbsolutePath());

            Toast.makeText(IUtils.getContext(), "File saved at: " + "Fumaki" + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(IUtils.getContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}
