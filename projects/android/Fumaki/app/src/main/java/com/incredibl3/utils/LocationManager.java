package com.incredibl3.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by Tiendv on 24/05/2015.
 */
public class LocationManager extends Activity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final String TAG = "Fumaki LocationManager";

    public static int LOCATION_DETECTOR_REQUEST_CODE;
    public static String LOCATION_LATITUDE = "LOCATION_LATITUDE";
    public static String LOCATION_LONGITUDE = "LOCATION_LONGITUDE";
    public static String LOCATION_AVAILABILITY = "LOCATION_AVAILABILITY";

    protected GoogleApiClient mGoogleApiClient = null;
    protected Location mLastLocation = null;
    protected LocationRequest mLocationRequest;
    private boolean mResolvingError = false;    // Bool to track whether the app is already resolving an error
    private static final String STATE_RESOLVING_ERROR = "resolving_error";  // Bool to track across activity restarts
    private static final int REQUEST_RESOLVE_ERROR = 1001;  // Request code to use when launching the resolution activity
    private static final String DIALOG_ERROR = "dialog_error";

    public DialogInterface.OnClickListener OnDialogClick = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(LOCATION_AVAILABILITY, false);
                setResult(LOCATION_DETECTOR_REQUEST_CODE, returnIntent);
                finish();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create Location Request
        mLocationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 minutes, in milliseconds
                .setFastestInterval(1 * 1000); // 1 minute, in milliseconds

        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);

        buildGoogleApiClient();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {  // more about this later
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onResume() {
        super.onResume();

        Console.Log(TAG, "onResume 111");

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } else if (!mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(STATE_RESOLVING_ERROR, mResolvingError);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Console.Log(TAG, "OnConnected!!!");
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            GenerateResultIntent(mLastLocation);
        } else {
            if (!IsLocationServiceEnabled()) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(LocationManager.this);
                dialog.setMessage("Location Service is not enabled. Please enable it to continue!!!");
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //this will navigate user to the device location settings screen
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
                AlertDialog alert = dialog.create();
                alert.show();
            } else {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient, mLocationRequest, this);
            }
        }
    }

    private boolean IsLocationServiceEnabled() {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Console.Log(TAG, "OnConnectionFailed + " + connectionResult.toString());

        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (connectionResult.hasResolution()) {
            try {
                mResolvingError = true;
                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            AlertDialogManager.showAlertDialog(this, AlertDialogManager.DIALOG_TYPE_ONE_CHOICE, "Error", connectionResult.toString(), OnDialogClick);
            mResolvingError = true;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null)
            mLastLocation = location;

        GenerateResultIntent(mLastLocation);
    }

    private void GenerateResultIntent(Location location) {
        Intent returnIntent = new Intent();

        if (location != null) {
            Console.Log(TAG, "GenerateResultIntent success");
            returnIntent.putExtra("LOCATION_LATITUDE", String.valueOf(mLastLocation.getLatitude()));
            returnIntent.putExtra("LOCATION_LONGITUDE", String.valueOf(mLastLocation.getLongitude()));
            returnIntent.putExtra(LOCATION_AVAILABILITY, true);
            setResult(LOCATION_DETECTOR_REQUEST_CODE, returnIntent);
            finish();
        } else {
            Console.Log(TAG, "GenerateResultIntent with location is null");
        }
    }
}
