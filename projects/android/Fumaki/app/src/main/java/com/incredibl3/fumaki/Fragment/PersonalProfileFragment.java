/**
 * Created by dungnx on 4/28/15.
 */

package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.incredibl3.fumaki.FumakiActivity;
import com.incredibl3.fumaki.R;

import org.json.JSONObject;

public class PersonalProfileFragment extends AFragment {

    public static final String TAG = "PersonalProfileFragment";
    private CallbackManager callbackManager;
    private TextView editTV;
    private TextView doneTV;

    private LinearLayout interests_layout;
    private String [] interests;
    private int interests_total;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.personal_profile, container, false);

        callbackManager = CallbackManager.Factory.create();
        editTV = (TextView) view.findViewById(R.id.editTV);
        editTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Not Implemented yet !!!", Toast.LENGTH_SHORT).show();
            }
        });

        doneTV = (TextView) view.findViewById(R.id.doneTV);
        doneTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.goToMainSettings();
            }
        });

        interests_layout = (LinearLayout) view.findViewById(R.id.interests);
        interests_layout.setVisibility(View.VISIBLE);

        interests = new String[100];
        interests_total = 0;

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void getInterests(){
        Bundle params = new Bundle();
        params.putString("limit", "100");
        /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + ((FumakiActivity)getActivity()).getUser().getModel().getUserID() + "/likes",
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        /* handle the result */
                        JSONObject res = response.getJSONObject();
                        parseInterestResponse(res);
                        updateGUI();
                    }
                }
        ).executeAsync();
    }

    private void parseInterestResponse(JSONObject response){
        try {
            for(int i = 0; i < 100; i++) {
                interests[i] = response.optJSONArray("data").getJSONObject(i).optString("name");
                interests_total++;
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void updateGUI(){
        if(interests_total > 0)
            interests_layout.setVisibility(View.VISIBLE);

        try {
            //TODO dungnx: temporary draw 2 of 100 like pages
            for(int i = 0; i < 2 /*interests_total*/; i++){

            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
