/**
 * Created by dungnx on 4/28/15.
 */

package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.facebook.CallbackManager;
import com.incredibl3.fumaki.FumakiActivity;
import com.incredibl3.fumaki.R;
import com.incredibl3.fumaki.XMPP.XMPPManager;
import com.incredibl3.fumaki.chat.Conversation;
import com.incredibl3.fumaki.chat.Message;
import com.incredibl3.fumaki.chat.MessagesListAdapter;
import com.incredibl3.utils.Console;
import com.incredibl3.utils.Stopwatch;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;

import java.util.ArrayList;
import java.util.List;

public class MessengerFragment extends AFragment {

    public static final String TAG = "MessengerFragment";
    private CallbackManager callbackManager;

    private Button btnSend;
    private EditText inputMsg;

    // Chat messages list adapter
    private MessagesListAdapter adapter;
    private List<Message> listMessages;
    private ListView listViewMessages;

    // Current chat information
    private Conversation mCurrentConversation;
    public void setCurrentWithUser(String userId) {
        Console.Log(TAG, "setCurrentWithUser: " + userId);
        mCurrentConversation = XMPPManager.GetInstance().CreateConversation(userId);

        if (mCurrentConversation == null)
            Console.Log(TAG, "Why null?");
        adapter.SetMessageList(mCurrentConversation.GetMessageList());
        if (mCurrentConversation.GetMessageList().size() > 0)
            adapter.notifyDataSetChanged();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Console.Log(TAG, "OnCreateView blah blah");

        View view = inflater.inflate(R.layout.messenger, container, false);

        callbackManager = CallbackManager.Factory.create();

        btnSend = (Button) view.findViewById(R.id.btnSend);
        inputMsg = (EditText) view.findViewById(R.id.inputMsg);
        listViewMessages = (ListView) view.findViewById(R.id.list_view_messages);

        btnSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Clearing the input filed once message was sent
                Message m = new Message(((FumakiActivity)getActivity()).getUser().getModel().getName(), inputMsg.getText().toString(), true);

                try {
                    if (mCurrentConversation != null && inputMsg.getText().toString().length() > 0)
                        mCurrentConversation.ChatInstance().sendMessage(inputMsg.getText().toString());
                    else {
                        Console.Log(TAG, "Sending chat without Chat created!!!");
                    }
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }

                // Appending the message to chat list
                appendMessage(m);
                mCurrentConversation.SetLastReceive(Stopwatch.GetInstance().getElapsedTimeString());

                inputMsg.setText("");
            }
        });

        listMessages = new ArrayList<Message>();

        adapter = new MessagesListAdapter(getActivity().getApplicationContext(), listMessages);
        listViewMessages.setAdapter(adapter);

        return view;
    }

    /**
     * Appending message to list view
     * */
    public void appendMessage(final Message m) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                mCurrentConversation.AddMessage(m);
                adapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * Notify the adapter
     * */
    public void notifyAdapter() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
