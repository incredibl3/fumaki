
package com.incredibl3.fumaki.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {

    @Expose
    private String userID;
    @Expose
    private String name;
    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;
    @SerializedName("profilePic0_url")
    @Expose
    private String profilePic0Url;
    @SerializedName("profilePic1_url")
    @Expose
    private String profilePic1Url;
    @SerializedName("profilePic2_url")
    @Expose
    private String profilePic2Url;
    @SerializedName("profilePic3_url")
    @Expose
    private String profilePic3Url;
    @SerializedName("profilePic4_url")
    @Expose
    private String profilePic4Url;
    @SerializedName("profilePic5_url")
    @Expose
    private String profilePic5Url;
    @Expose
    private String birthday;
    @Expose
    private int gender;
    @Expose
    private String description;
    @Expose
    private String latitude;
    @Expose
    private String longitude;
    @SerializedName("interest_gender")
    @Expose
    private int interestGender;
    @SerializedName("interest_distance")
    @Expose
    private int interestDistance;
    @SerializedName("distance_unit")
    @Expose
    private String distanceUnit;
    @Expose
    private int BeFound;
    @SerializedName("gcm_regid")
    @Expose
    private String gcmID;

    /**
     *
     * @return
     *     The userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     *
     * @param userID
     *     The userID
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     *
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     *     The avatarUrl
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }

    /**
     *
     * @param avatarUrl
     *     The avatar_url
     */
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    /**
     *
     * @return
     *     The profilePic0Url
     */
    public String getProfilePic0Url() {
        return profilePic0Url;
    }

    /**
     *
     * @param profilePic0Url
     *     The profilePic0_url
     */
    public void setProfilePic0Url(String profilePic0Url) {
        this.profilePic0Url = profilePic0Url;
    }

    /**
     *
     * @return
     *     The profilePic1Url
     */
    public String getProfilePic1Url() {
        return profilePic1Url;
    }

    /**
     *
     * @param profilePic1Url
     *     The profilePic1_url
     */
    public void setProfilePic1Url(String profilePic1Url) {
        this.profilePic1Url = profilePic1Url;
    }

    /**
     *
     * @return
     *     The profilePic2Url
     */
    public String getProfilePic2Url() {
        return profilePic2Url;
    }

    /**
     *
     * @param profilePic2Url
     *     The profilePic2_url
     */
    public void setProfilePic2Url(String profilePic2Url) {
        this.profilePic2Url = profilePic2Url;
    }

    /**
     *
     * @return
     *     The profilePic3Url
     */
    public String getProfilePic3Url() {
        return profilePic3Url;
    }

    /**
     *
     * @param profilePic3Url
     *     The profilePic3_url
     */
    public void setProfilePic3Url(String profilePic3Url) {
        this.profilePic3Url = profilePic3Url;
    }

    /**
     *
     * @return
     *     The profilePic4Url
     */
    public String getProfilePic4Url() {
        return profilePic4Url;
    }

    /**
     *
     * @param profilePic4Url
     *     The profilePic4_url
     */
    public void setProfilePic4Url(String profilePic4Url) {
        this.profilePic4Url = profilePic4Url;
    }

    /**
     *
     * @return
     *     The profilePic5Url
     */
    public String getProfilePic5Url() {
        return profilePic5Url;
    }

    /**
     *
     * @param profilePic5Url
     *     The profilePic5_url
     */
    public void setProfilePic5Url(String profilePic5Url) {
        this.profilePic5Url = profilePic5Url;
    }

    /**
     *
     * @return
     *     The birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     *
     * @param birthday
     *     The birthday
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     *
     * @return
     *     The gender
     */
    public int getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     *     The gender
     */
    public void setGender(int gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     *     The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     *     The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     *     The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     *     The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     *     The interestGender
     */
    public int getInterestGender() {
        return interestGender;
    }

    /**
     *
     * @param interestGender
     *     The interest_gender
     */
    public void setInterestGender(int interestGender) {
        this.interestGender = interestGender;
    }

    /**
     *
     * @return
     *     The interestDistance
     */
    public int getInterestDistance() {
        return interestDistance;
    }

    /**
     *
     * @param interestDistance
     *     The interest_distance
     */
    public void setInterestDistance(int interestDistance) {
        this.interestDistance = interestDistance;
    }

    /**
     *
     * @return
     *     The distanceUnit
     */
    public String getDistanceUnit() {
        return distanceUnit;
    }

    /**
     *
     * @param distanceUnit
     *     The distance_unit
     */
    public void setDistanceUnit(String distanceUnit) {
        this.distanceUnit = distanceUnit;
    }

    /**
     *
     * @return
     *     The BeFound
     */
    public int getBeFound() {
        return BeFound;
    }

    /**
     *
     * @param BeFound
     *     The BeFound
     */
    public void setBeFound(int BeFound) {
        this.BeFound = BeFound;
    }

    /**
     *
     * @return
     *     The gcmID
     */
    public String getGcmID() {
        return gcmID;
    }

    /**
     *
     * @param gcmID
     *     The gcm_regid
     */
    public void setGcmID(String gcmID) {
        this.gcmID = gcmID;
    }

}
