package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andtinder.model.CardModel;
import com.andtinder.view.CardContainer;
import com.facebook.CallbackManager;
import com.incredibl3.andTinder.FumakiCardStackAdapter;
import com.incredibl3.fumaki.Config;
import com.incredibl3.fumaki.FumakiActivity;
import com.incredibl3.fumaki.R;
import com.incredibl3.fumaki.api.RequestApi;
import com.incredibl3.fumaki.model.RequestModel;
import com.incredibl3.fumaki.model.UserModel;
import com.incredibl3.utils.Console;
import com.squareup.picasso.Picasso;

import java.util.Vector;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Tiendv on 28/04/2015.
 */
public class HomeFragment extends AFragment {
    public static String TAG = "HomeFragment";

    private ImageView profilePictureView;
    private Candidate currentCandidate;
    private TextView candidateNameView;
    private TextView candidateAgeView;
    private Vector<Candidate> candidates;
    private ImageButton btnSettings;
    private ImageButton btnChat;
    private CardContainer mCardContainer;


    private CallbackManager callbackManager;

    private Vector<RequestModel> fumakiRequests;

    private RestAdapter.Builder builder;
    private RestAdapter requestAdapter;
    private RequestApi requestApi;

//    private RequestController mRequestController;

    private static final String defautl_avatar_url= "https://cms-assets.tutsplus.com/uploads/users/21/posts/19431/featured_image/CodeFeature.jpg";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home, container, false);

        candidates = new Vector<Candidate>();
        fumakiRequests = new Vector<RequestModel> ();

        View viewFumakiCard = inflater.inflate(R.layout.std_fumaki_card, container, false);

        profilePictureView = (ImageView)viewFumakiCard.findViewById(R.id.profilePicture);
        profilePictureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Console.Log(TAG, "On Picture Clicked!!!");
                processCandidateList();
            }
        });

        String avatar_url = defautl_avatar_url;
        if(currentCandidate != null){
            avatar_url = currentCandidate.userModel.getAvatarUrl();
        }

        ShowAvatarPicture(avatar_url);

        //candidateNameView = (TextView) viewFumakiCard.findViewById(R.id.candidateName);
        //candidateAgeView = (TextView) viewFumakiCard.findViewById(R.id.candidateAge);

        ImageButton btnKill = (ImageButton) view.findViewById(R.id.btnKill);
        btnKill.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Console.Log(TAG, "Kill: ");
                if(currentCandidate != null) {
                    Console.Log(TAG, "Kill: " + currentCandidate.userModel.getUserID());
                    fumakiRequests.add(contructFumakiRequest("Kill"));
                }
            }
        });

        ImageButton btnMarry = (ImageButton) view.findViewById(R.id.btnMarry);
        btnMarry.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Console.Log(TAG, "Marry: ");
                if(currentCandidate != null) {
                    Console.Log(TAG, "Marry: " + currentCandidate.userModel.getUserID());
                    fumakiRequests.add(contructFumakiRequest("Marry"));
                }
            }
        });

        ImageButton btnFuck = (ImageButton) view.findViewById(R.id.btnFuck);
        btnFuck.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Console.Log(TAG, "Fuck: ");
                if(currentCandidate != null) {
                    Console.Log(TAG, "Fuck: " + currentCandidate.userModel.getUserID());
                    fumakiRequests.add(contructFumakiRequest("Fuck"));
                }
            }
        });

        ImageButton btnExtreme = (ImageButton) view.findViewById(R.id.btnExtreme);
        btnExtreme.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Console.Log(TAG, "Go to Edit Profile: ");
                mCallback.goToEditProfile();
            }
        });

        btnSettings = (ImageButton) view.findViewById(R.id.btnSetings);
        btnSettings.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.goToMainSettings();
            }
        });

        btnChat = (ImageButton) view.findViewById(R.id.btnChat);
        btnChat.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				//Console.PrintLogToFile();
                //Toast.makeText(getActivity().getApplicationContext(), "Not Implemented yet!!!", Toast.LENGTH_SHORT).show();
                mCallback.goToMatchList();
            }
        });

        mCardContainer = (CardContainer) view.findViewById(R.id.layoutview);


        builder = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.HEADERS)
                .setEndpoint(Config.BASE_URL);

        requestAdapter = builder.build();
        requestApi = requestAdapter.create(RequestApi.class);

//        mRequestController = new RequestController((FumakiActivity)getActivity());

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onResume(){
        super.onResume();
    }

    public void setCandidates(UserModel users[]){
//        getFriends(); // dungnx temporay call it here

        candidates.clear();
        String[] URLs = new String[users.length];
        for(int i = 0; i < users.length; i++){
            URLs[i] = users[i].getAvatarUrl().replace("type=large", "width=500&height=500");
        }
        FumakiCardStackAdapter adapter = new FumakiCardStackAdapter(getActivity(), URLs);

        for(int i = 0; i < users.length; i++) {
            Candidate candidate = new Candidate(users[i]);
            candidates.add(candidate);
            adapter.add(candidate.cardModel);
        }

        mCardContainer.setAdapter(adapter);

        processCandidateList();
    }

    /*
        - This function will show every candidate received from find algorithm,
        one by one, and store user decision before sending it to server

     */
    private void processCandidateList()
    {
        try {
            int numCandidates = candidates.size();
            if (numCandidates == 0) {
                sendFumakiRequests();
                mCallback.goToNewMatch();
                return;
            }
//            Random random = new Random();
//            int randomIndex = random.nextInt(numCandidates);
//
//            currentCandidate = candidates.get(randomIndex);
//            candidates.removeElementAt(randomIndex);
            currentCandidate = candidates.get(0);
            candidates.removeElementAt(0);
        } catch(NullPointerException e){
            e.printStackTrace();
        }
    }

    private void sendFumakiRequests(){

        RequestModel[] requests = new RequestModel[fumakiRequests.size()];
        fumakiRequests.toArray(requests);

        requestApi.createRequests(requests, new Callback<RequestModel[]>() {
            @Override
            public void success(RequestModel[] requests, Response response) {
                currentCandidate = null;
                fumakiRequests.clear();
                candidates.clear();
                mCallback.onCreateRequests();
                //seekMatchers();
            }

            @Override
            public void failure(RetrofitError error) {
                // TODO: error
            }
        });


    }

    private void getFriends(){
        ((FumakiActivity)getActivity()).getRequestController().getFriends();
    }

    private void seekMatchers(){
        ((FumakiActivity)getActivity()).getRequestController().getMatchers();
    }

    private RequestModel contructFumakiRequest(String action){
        UserModel user = ((FumakiActivity)getActivity()).getUser().getModel();
        RequestModel request = new RequestModel();
        request.setFrom(user.getUserID());
        request.setTo(currentCandidate.userModel.getUserID());
        int iAct = 0;
        if(action.equals("Marry"))
            iAct = 1;
        request.setAction(iAct);

        return request;
    }

    private void ShowAvatarPicture(String url){
        Picasso.with(getActivity())
                .load(url)
                .into(profilePictureView);
    }

//    public void UpdateAuthorizationInfo(){
//        String username = ((FumakiActivity)getActivity()).user.getUserID();
//        String password = "";
//        if(AccessToken.getCurrentAccessToken() != null)
//            password = AccessToken.getCurrentAccessToken().getToken();
//        if (username != null && password != null) {
//            // concatenate username and password with colon for authentication
//            final String credentials = username + ":" + password;
//
//            builder.setRequestInterceptor(new RequestInterceptor() {
//                @Override
//                public void intercept(RequestFacade request) {
//                    // create Base64 encodet string
//                    String string = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
//                    request.addHeader("Accept", "application/json");
//                    request.addHeader("Authorization", string);
//                }
//            });
//            requestAdapter = builder.build();
//            requestApi = requestAdapter.create(RequestApi.class);
//        }
//    }

    private class Candidate{
        public UserModel userModel;
        public CardModel cardModel;

        public Candidate(final UserModel userModel){
            this.userModel = userModel;
            this.cardModel = new CardModel("Title1", "Description goes here", getResources().getDrawable(R.drawable.picture1));

            this.cardModel.setOnCardDismissedListener(new CardModel.OnCardDismissedListener() {
                @Override
                public void onFuck() {
                    Console.Log("Swipeable Cards", "I fuck this card");
                    Toast.makeText(getActivity().getApplicationContext(), "I'll fuck this card", Toast.LENGTH_SHORT).show();
                    fumakiRequests.add(contructFumakiRequest("Fuck"));
                    processCandidateList();
                }

                @Override
                public void onCancel() {
                    Console.Log("Swipeable Cards", "I cancel the card");
                    Toast.makeText(getActivity().getApplicationContext(), "I'll cancel this card", Toast.LENGTH_SHORT).show();
                    processCandidateList();
                }

                @Override
                public void onKill() {
                    Console.Log("Swipeable Cards", "I kill this card");
                    Toast.makeText(getActivity().getApplicationContext(), "I'll kill this card", Toast.LENGTH_SHORT).show();
                    fumakiRequests.add(contructFumakiRequest("Kill"));
                    processCandidateList();
                }

                @Override
                public void onMarry() {
                    Console.Log("Swipeable Cards", "I marry this card");
                    Toast.makeText(getActivity().getApplicationContext(), "I'll marry this card", Toast.LENGTH_SHORT).show();
                    fumakiRequests.add(contructFumakiRequest("Marry"));
                    processCandidateList();
                }
            });

            this.cardModel.setOnClickListener(new CardModel.OnClickListener(){
                @Override
                public void OnClickListener(){
                    mCallback.goToProfile(currentCandidate.userModel);
                }
            });
        }
    }
}
