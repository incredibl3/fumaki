package com.incredibl3.fumaki.api;

import com.incredibl3.fumaki.model.*;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by dungnx on 5/24/15.
 */
public interface RequestApi {
    @GET("/requests")
    public void getAllRequests(Callback<RequestModel[]> response);

    @GET("/requests/{reqid}")
    public void getRequest(@Path("reqid") String reqid, Callback<RequestModel> response);

    @POST("/requests")
    public void createRequests(@Body RequestModel[] requests, Callback<RequestModel[]> response);

    @DELETE("/requests/{reqid}")
    public void deleteUser(@Path("reqid") String reqid);

    @PUT("/users/{userid}")
    public void updateUser(@Path("userid") String userid, @Body UserModel user, Callback<UserModel> response);

    @GET("/users/{userid}/candidates")
    public void seekCandidate(@Path("userid") String userid, Callback<UserModel[]> response);
}
