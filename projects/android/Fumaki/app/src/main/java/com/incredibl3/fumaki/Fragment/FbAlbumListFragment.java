/**
 * Created by dungnx on 4/28/15.
 */

package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.incredibl3.fumaki.FbAlbumList;
import com.incredibl3.fumaki.FbAlbumListAdapter;
import com.incredibl3.fumaki.R;

import java.util.ArrayList;
import java.util.List;

public class FbAlbumListFragment extends AFragment {

    public static final String TAG = "FbAlbumListFragment";
    private CallbackManager callbackManager;

    private FbAlbumListAdapter adapter;
    private List<FbAlbumList> listFbAlbum;
    private ListView listViewMatch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fb_album_list, container, false);

        callbackManager = CallbackManager.Factory.create();
        listViewMatch = (ListView) view.findViewById(R.id.fb_album_list);

        listFbAlbum = new ArrayList<FbAlbumList>();

        adapter = new FbAlbumListAdapter(getActivity().getApplicationContext(), listFbAlbum);
        listViewMatch.setAdapter(adapter);
        listViewMatch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "You Clicked at " + position, Toast.LENGTH_SHORT).show();
                //mCallback.goToMessenger();
            }
        });

        FbAlbumList m = new FbAlbumList(R.drawable.bristleback, "Photos of Me", "45 photos");
        FbAlbumList m1 = new FbAlbumList(R.drawable.pudge, "Cover Photos", "2 photos");
        FbAlbumList m2 = new FbAlbumList(R.drawable.puck, "Mobile Updates", "2 photos");
        FbAlbumList m3 = new FbAlbumList(R.drawable.bountyhunter, "Timeline photos", "1 photo");
        FbAlbumList m4 = new FbAlbumList(R.drawable.sandking, "Profile Pictures", "2 photos");

        // Appending the message to chat list
        appendMessage(m);
        appendMessage(m1);
        appendMessage(m2);
        appendMessage(m3);
        appendMessage(m4);
        return view;
    }

    /**
     * Appending message to list view
     * */
    private void appendMessage(final FbAlbumList m) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                listFbAlbum.add(m);
                adapter.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
