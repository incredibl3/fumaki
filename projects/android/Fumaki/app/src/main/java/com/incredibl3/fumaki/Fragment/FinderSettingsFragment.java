/**
 * Created by dungnx on 4/28/15.
 */

package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.incredibl3.fumaki.R;

public class FinderSettingsFragment extends AFragment{

    public static final String TAG = "FinderSettingsFragment";
    private CallbackManager callbackManager;
    private RadioGroup genderRG;
    private SeekBar peopleRangeSB;
    private TextView peopleRangeTV;
    private SeekBar peopleAgesSB;
    private TextView peopleAgesTV;
    private static int minPeopleAges = 18;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.finder_settings, container, false);
        callbackManager = CallbackManager.Factory.create();
        genderRG = (RadioGroup) view.findViewById(R.id.genderRG);

        genderRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.menRadioButton) {
                    Toast.makeText(getActivity().getApplicationContext(), "Men radio selected !!!", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.womenRadioButton) {
                    Toast.makeText(getActivity().getApplicationContext(), "Women radio selected !!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        peopleRangeSB = (SeekBar) view.findViewById(R.id.peopleRangeSB);
        peopleRangeTV = (TextView) view.findViewById(R.id.peopleRangeTV);
        peopleRangeSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                peopleRangeTV.setText("Find people within: " + progress + "km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        peopleAgesSB = (SeekBar) view.findViewById(R.id.peopleAgesSB);
        peopleAgesTV = (TextView) view.findViewById(R.id.peopleAgesTV);
        peopleAgesSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                peopleAgesTV.setText("Find people ages: " + (progress + minPeopleAges));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
