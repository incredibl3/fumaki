/**
 * Created by dungnx on 4/28/15.
 */

package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.incredibl3.fumaki.R;
import com.incredibl3.fumaki.model.UserModel;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class ProfileFragment extends AFragment {

    public static final String TAG = "ProfileFragment";
    private CallbackManager callbackManager;
    private TextView doneTV;
    private ImageView killIV;
    private ImageView fuckIV;
    private ImageView marryIV;

    private ImageView [] mutualFriendAvatarsIV;

    private UserModel currentUser;

    private LinearLayout mutualfriends_layout;
    private String [] mutual_friends;
    private int mutual_friends_total;

    private LinearLayout mutualinterests_layout;
    private int mutual_interests_total;

    // for profile pictures slider
    ImageSlidesFragmentAdapter mSlideApdater;
    ViewPager mImagePager;
    PageIndicator mSlideIndicator;

    // fake urls
    String [] picURLs = new String[]{"https://scontent.xx.fbcdn.net/hphotos-xfa1/t31.0-8/1262593_10200535166290932_183310660_o.jpg",
                        "https://scontent.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/23818_1220826770088_6206141_n.jpg?oh=54dee10e75900cb2727274a6d399727b&oe=566EAF10"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile, container, false);

        callbackManager = CallbackManager.Factory.create();

        currentUser = null;

        // image Slider
        mSlideApdater = new ImageSlidesFragmentAdapter(getActivity().getSupportFragmentManager());
        mImagePager = (ViewPager) view.findViewById(R.id.pager);
        mImagePager.setAdapter(mSlideApdater);
        mSlideIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        mSlideIndicator.setViewPager(mImagePager);

//        String [] picURLs = {};
//        mSlideApdater.setImageURLs(picURLs);

        ((CirclePageIndicator) mSlideIndicator).setSnap(true);

        mutualfriends_layout = (LinearLayout) view.findViewById(R.id.mutualfriends);
        mutualfriends_layout.setVisibility(View.INVISIBLE);
        mutual_friends_total = 0;
        mutual_friends = new String[4];
        mutualFriendAvatarsIV = new ImageView[4];
        mutualFriendAvatarsIV[0] = (ImageView)view.findViewById(R.id.mutalFriends0);
        mutualFriendAvatarsIV[1] = (ImageView)view.findViewById(R.id.mutalFriends1);
        mutualFriendAvatarsIV[2] = (ImageView)view.findViewById(R.id.mutalFriends2);
        mutualFriendAvatarsIV[3] = (ImageView)view.findViewById(R.id.mutalFriends3);

        mutualinterests_layout = (LinearLayout) view.findViewById(R.id.mutualinterests);
        mutualinterests_layout.setVisibility(View.INVISIBLE);
        mutual_interests_total = 0;

        doneTV = (TextView) view.findViewById(R.id.doneTV);
        doneTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.goToHome();
            }
        });

        killIV = (ImageView) view.findViewById(R.id.killIV);
        killIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Not Implemented yet!!!", Toast.LENGTH_SHORT).show();
            }
        });

        fuckIV = (ImageView) view.findViewById(R.id.fuckIV);
        fuckIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Not Implemented yet!!!", Toast.LENGTH_SHORT).show();
            }
        });

        marryIV = (ImageView) view.findViewById(R.id.marryIV);
        marryIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Not Implemented yet!!!", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {

    }

    public void setCurrentUser(UserModel user){
        this.currentUser = user;
        String [] picURLs = new String[]{user.getAvatarUrl(), user.getProfilePic0Url(), user.getProfilePic1Url(),
                user.getProfilePic2Url(), user.getProfilePic3Url(), user.getProfilePic4Url(), user.getProfilePic5Url()};
        mSlideApdater.setImageURLs(picURLs);
        getMutualFriendsAndLikes();
    }

    private void getMutualFriendsAndLikes(){
        Bundle params = new Bundle();
        params.putString("fields", "context.fields(mutual_friends,mutual_likes)");
        /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                this.currentUser.getUserID(),
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                    /* handle the result */
                        JSONObject res = response.getJSONObject();
                        parseFriendsAndLikesResponse(res);
                        updateGUI();
                    }
                }
        ).executeAsync();
    }

    private void parseFriendsAndLikesResponse(JSONObject response) {
        try {
            mutual_friends_total = response.optJSONObject("context").optJSONObject("mutual_friends").optJSONObject("summary").optInt("total_count");
            for (int i = 0; i < 4; i++) {
                mutual_friends[i] = response.optJSONObject("context").optJSONObject("mutual_friends").optJSONArray("data").getJSONObject(i).optString("id");
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        try {
            mutual_interests_total = response.optJSONObject("context").optJSONObject("mutual_likes").optJSONObject("summary").optInt("total_count");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void updateGUI(){
        if(mutual_friends_total > 0)
            mutualfriends_layout.setVisibility(View.VISIBLE);

        for(int i = 0; i < 4; i++) {
            String avatarUrl = "http://graph.facebook.com/" + mutual_friends[i] + "/picture?width=100&height=100";
            Picasso.with(getActivity())
                    .load(avatarUrl)
                    .into(mutualFriendAvatarsIV[i]);
        }

        if(mutual_interests_total > 0)
            mutualinterests_layout.setVisibility(View.VISIBLE);
    }


    private class ImageSlidesFragmentAdapter extends FragmentPagerAdapter {
        private ArrayList<ImageView> mImages;

        public ImageSlidesFragmentAdapter(FragmentManager fm){
            super(fm);
            mImages = new ArrayList<ImageView>();
        }

        @Override
        public Fragment getItem(int position) {
            return SwipeFragment.newInstance(position, mImages);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount(){
            return mImages.size();
        }

        public void setImageURLs(String [] urls){
            mImages.clear();
            for(int i = 0; i < urls.length; i++){
                if(urls[i] != null && !urls[i].isEmpty()) {
                    ImageView image = new ImageView(getActivity());
                    Picasso.with(getActivity())
                            .load(urls[i])
                            .into(image);

                    mImages.add(image);
                }
            }
            notifyDataSetChanged();
        }



    }

    public static class SwipeFragment extends Fragment {
        private static ArrayList<ImageView> mImageViews;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View swipeView = inflater.inflate(R.layout.swipe_fragment, container, false);
            ImageView imageView = (ImageView) swipeView.findViewById(R.id.imageView);
            Bundle bundle = getArguments();
            int position = bundle.getInt("position");
            imageView.setImageDrawable(mImageViews.get(position).getDrawable());
            return swipeView;
        }

        public static SwipeFragment newInstance(int position, ArrayList<ImageView> imgViews) {
            SwipeFragment swipeFragment = new SwipeFragment();
            if(mImageViews == null)
                mImageViews = imgViews;

            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            swipeFragment.setArguments(bundle);
            return swipeFragment;
        }

    }

}
