/**
 * Created by dungnx on 4/28/15.
 */

package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.incredibl3.fumaki.R;

public class NewMatchFragment extends AFragment {

    public static final String TAG = "NewMatch";
    private CallbackManager callbackManager;
    private LinearLayout llLetChat;
    private LinearLayout llPlayOn;
    private LinearLayout llTellYourFriends;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_match, container, false);

        callbackManager = CallbackManager.Factory.create();

        llLetChat = (LinearLayout) view.findViewById(R.id.llLetChat);
        llLetChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Not Implemented yet!!!", Toast.LENGTH_SHORT).show();
            }
        });

        llPlayOn = (LinearLayout) view.findViewById(R.id.llPlayOn);
        llPlayOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Not Implemented yet!!!", Toast.LENGTH_SHORT).show();
            }
        });

        llTellYourFriends = (LinearLayout) view.findViewById(R.id.llTellYourFriends);
        llTellYourFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Not Implemented yet!!!", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
