package com.incredibl3.fumaki;

import android.util.Base64;

import com.facebook.AccessToken;
import com.google.gson.JsonObject;
import com.incredibl3.fumaki.api.BaseRequest;
import com.incredibl3.fumaki.api.RequestApi;
import com.incredibl3.fumaki.api.UserApi;
import com.incredibl3.fumaki.model.MatcherModel;
import com.incredibl3.fumaki.model.UserModel;
import com.incredibl3.utils.Console;

import java.util.HashMap;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.Response;

/**
 * Created by tien.dinhvan on 5/27/2015.
 */
public class RequestController {
    public static final String TAG = "RequestController";
    private RestAdapter.Builder builder;
    private RestAdapter userAdapter;
    private UserApi userApi;
    private RestAdapter requestAdapter;
    private RequestApi requestApi;

    private FumakiActivity mActivity;

    private HashMap<Integer, BaseRequest> mRequestMap;

    public RequestController(FumakiActivity activity) {
        mRequestMap = new HashMap<Integer, BaseRequest>();
        mActivity = activity;
        builder = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Config.BASE_URL);

        userAdapter = builder.build();
        userApi = userAdapter.create(UserApi.class);
    }

    public FumakiActivity getActivity() { return mActivity; }

    private boolean AddRequest(BaseRequest request) {
        if (mRequestMap.containsKey((Integer)request.GetType())) {
            Console.Log(TAG, "Duplicate request with same type: " + (Integer)request.GetType());
            return false;
        }

        mRequestMap.put((Integer) request.GetType(), request);

        return true;
    }

    public void DeleteRequest(int key) {
        mRequestMap.remove(key);
    }

    public void seekCandidates() {
        BaseRequest<UserModel[]> baseRequest = new BaseRequest<UserModel[]>(this, BaseRequest.OP_SEEK_CANDIDATE);
        if (AddRequest(baseRequest))
            userApi.seekCandidate(baseRequest.mCallback);
    }

    public void getMatchers() {
        BaseRequest<MatcherModel[]> baseRequest = new BaseRequest<MatcherModel[]>(this, BaseRequest.OP_GET_MATCHERS);
        if (AddRequest(baseRequest))
            userApi.getMatches(baseRequest.mCallback);
    }

    public void getFriends() {
        BaseRequest<MatcherModel[]> baseRequest = new BaseRequest<MatcherModel[]>(this, BaseRequest.OP_GET_FRIENDS);
        if (AddRequest(baseRequest))
            userApi.getMatches(baseRequest.mCallback);
    }

    public void deleteMatches() {
        BaseRequest<Response> baseRequest = new BaseRequest<Response>(this, BaseRequest.OP_DELETE_MATCHES);
        if (AddRequest(baseRequest))
            userApi.deleteMatches(baseRequest.mCallback);
    }

    public void loginToServer() {
        BaseRequest<JsonObject> baseRequest = new BaseRequest<JsonObject>(this, BaseRequest.OP_LOGIN_TO_SERVER);
        if (AddRequest(baseRequest))
            userApi.auth(baseRequest.mCallback);
    }

    public void createUser(UserModel user) {
        BaseRequest<UserModel> baseRequest = new BaseRequest<UserModel>(this, BaseRequest.OP_CREATE_NEW_USER);
        if (AddRequest(baseRequest))
            userApi.createUser(user, baseRequest.mCallback);
    }

    public void updateUserInfo(UserModel user) {
        BaseRequest<UserModel> baseRequest = new BaseRequest<UserModel>(this, BaseRequest.OP_UPDATE_USER_INFO);
        if (AddRequest(baseRequest))
            userApi.updateUser(user, baseRequest.mCallback);
    }

    public void UpdateAuthorizationInfo(final String credential) {
        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                // create Base64 encodet string
                String string = "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
                request.addHeader("Accept", "application/json");
                request.addHeader("Authorization", string);
            }
        });
        userAdapter = builder.build();
        userApi = userAdapter.create(UserApi.class);

        requestAdapter = builder.build();
        requestApi = requestAdapter.create(RequestApi.class);
    }
}
