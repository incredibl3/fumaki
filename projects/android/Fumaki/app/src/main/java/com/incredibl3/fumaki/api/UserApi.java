package com.incredibl3.fumaki.api;

import com.google.gson.JsonObject;
import com.incredibl3.fumaki.model.*;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
/**
 * Created by dungnx on 5/23/15.
 */
public interface UserApi {
    @GET("/users")
    public void getAllUsers(Callback<UserModel[]> response);

    @GET("/users/me")
    public void getUser(Callback<UserModel> response);

    @POST("/users")
    public void createUser(@Body UserModel user, Callback<UserModel> response);

    @POST("/users/auth")
    public void auth( Callback<JsonObject> response);

    @DELETE("/users/me")
    public void deleteUser(String userid);

    @PATCH("/users/me")
    public void updateUser(@Body UserModel user, Callback<UserModel> response);

    @GET("/users/me/candidates")
    public void seekCandidate(Callback<UserModel[]> response);

    @GET("/users/me/matches")
    public void getMatches(Callback<MatcherModel[]> response);

    @DELETE("/users/me/matches")
    public void deleteMatches(Callback<Response> response);
}
