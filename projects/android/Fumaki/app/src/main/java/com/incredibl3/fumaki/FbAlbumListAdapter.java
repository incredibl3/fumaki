package com.incredibl3.fumaki;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Hung on 8/25/2015.
 */
public class FbAlbumListAdapter extends BaseAdapter {
    private Context context;
    private List<FbAlbumList> fbAlbumListItems;
    ImageView ivAvatar;

    public FbAlbumListAdapter(Context context, List<FbAlbumList> navDrawerItems) {
        this.context = context;
        this.fbAlbumListItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return fbAlbumListItems.size();
    }

    @Override
    public Object getItem(int position) {
        return fbAlbumListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /**
         * The following list not implemented reusable list items as list items
         * are showing incorrect data Add the solution if you have one
         * */

        FbAlbumList m = fbAlbumListItems.get(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        // Identifying the message owner
        convertView = mInflater.inflate(R.layout.fb_album_list_item, null);

        ivAvatar = (ImageView) convertView.findViewById(R.id.ivAvatar);
        ivAvatar.setImageResource(m.getAvatar());

        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvContent = (TextView) convertView.findViewById(R.id.tvContent);

        tvName.setText(m.getName());
        tvContent.setText(m.getContent());

        return convertView;
    }
}
