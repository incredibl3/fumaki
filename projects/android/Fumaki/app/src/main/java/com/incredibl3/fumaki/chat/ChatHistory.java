package com.incredibl3.fumaki.chat;

import com.incredibl3.fumaki.XMPP.XMPPManager;
import com.incredibl3.utils.Console;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tien.dinhvan on 7/20/2015.
 */
public class ChatHistory extends IQ implements Comparable<ChatHistory> {
    /**
     * Final Variable
     */
    public final static String TAG = "ChatHistory";
    public final static int MAX_SIZE = 100;
    public final static int NOT_LOADED = 0;
    public final static int PARTLY_LOADED = 0;
    public final static int FULLY_LOADED = 0;

    /**
     * Final Variable
     */
    public final static String HISTORY_ELEMENT_NAME = "retrieve";
    public final static String HISTORY_ELEMENT_NAMESPACE = "urn:xmpp:archive";

    /**
     * History Information
     */
    private String with = "";
    public String With() { return with; }

    private String startAt = "";
    public String StartAt() { return startAt; }

    /**
     * History Information
     */
    private Map<String, Message> messagesItems;
    public ResultSet rSet;
    private int bIsLoaded; // 0: Not loaded, 1: Loaded but not full; 2: Loaded fully

    public int IsLoaded() { return bIsLoaded; }

    public ChatHistory(String childElementName, String childElementNamespace) {
        super(childElementName, childElementNamespace);
        setType(IQ.Type.get);
    }

    public ChatHistory AddInformation(String withUser, String startAt) {
        this.with = withUser.substring(0, withUser.indexOf("@"));
        this.startAt = startAt;
        this.messagesItems = new HashMap<>();
        this.rSet = new ResultSet();
        this.bIsLoaded = NOT_LOADED;

        return this;
    }

    public List<Message> GetHistoryList() {
        List<Message> retList = new ArrayList<>();

        for (String key : messagesItems.keySet()) {
            retList.add(messagesItems.get(key));
        }

        return retList;
    }

    public void AddMessage(String secs, Message message) {
        messagesItems.put(secs, message);

        Console.Log(TAG, "Add a message at: " + secs + " |body: " + message.getMessage());
        Console.Log(TAG, "this.rSet.count " + this.rSet.count + " |messagesItems.size(): " + messagesItems.size());
    }

    public void HistoryLoaded() {
        if (rSet.getLast() < rSet.getCount() - 1) {
            bIsLoaded = PARTLY_LOADED;
            XMPPManager.GetInstance().SendIQPacket(this);
        } else {
            bIsLoaded = FULLY_LOADED;
        }
    }

    public void AddHistory() {
        // Here we have retrieve all messages for this history chat
        XMPPManager.GetInstance().AddHistoryChat(with, messagesItems);

    }

    public boolean IsSame(ChatHistory ch) {
        if (this.with.equals(ch.with) && this.startAt.equals(ch.startAt))
            return true;

        return false;
    }

    public boolean IsSame(String with, String startAt) {
        if (this.with.equals(with) && this.startAt.equals(startAt))
            return true;

        return false;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml){
        //return "<retrieve  xmlns='urn:xmpp:archive' with='test@customOpenfire.com'><set xmlns='http://jabber.org/protocol/rsm'><max xmlns='http://jabber.org/protocol/rsm'>30</max></set> </retrieve>";
        xml.attribute("with", with + "@fumaki");
        xml.attribute("start", startAt);
        xml.rightAngleBracket();

        xml.element( new ExtensionElement() {
            @Override
            public String getNamespace() {
                return "http://jabber.org/protocol/rsm";
            }

            @Override
            public String getElementName() {
                return "set";
            }

            @Override
            public CharSequence toXML() {
                XmlStringBuilder xml = new XmlStringBuilder(this);
                xml.rightAngleBracket();
                xml.element("max", String.valueOf(MAX_SIZE));
                if (bIsLoaded == PARTLY_LOADED) {
                    xml.element("after", String.valueOf(rSet.getLast()));
                }
                xml.closeElement("set");
                return xml;
            }
        });

        return xml;
    }

    public int compareTo(ChatHistory ch) {
       return this.StartAt().compareTo(ch.StartAt());
    }

    public static Comparator<ChatHistory> ChatHistoryComparator
                = new Comparator<ChatHistory>() {
        @Override
        public int compare(ChatHistory lhs, ChatHistory rhs) {
            return lhs.With().compareTo(rhs.With());
        }
    };

    public class ResultSet {
        private int first;
        private int last;
        private int count;
        private int indexAtt;

        public ResultSet()
        {
        }

        public int getLast()
        {
            return last;
        }

        public void setLast(int last)
        {
            this.last = last;
        }

        public int getCount()
        {
            return count;
        }

        public void setCount(int count)
        {
            this.count = count;
        }

        public int getFirst()
        {
            return first;
        }

        public void setFirst(int first)
        {
            this.first = first;
        }

        public int getIndexAtt()
        {
            return indexAtt;
        }

        public void setIndexAtt(int indexAtt)
        {
            this.indexAtt = indexAtt;
        }
    }
}
