
package com.incredibl3.fumaki.model;

import com.google.gson.annotations.Expose;

public class RequestModel {

    @Expose
    private String from;
    @Expose
    private String to;
    @Expose
    private int action;

    /**
     * 
     * @return
     *     The from
     */
    public String getFrom() {
        return from;
    }

    /**
     * 
     * @param from
     *     The from
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * 
     * @return
     *     The to
     */
    public String getTo() {
        return to;
    }

    /**
     * 
     * @param to
     *     The to
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * 
     * @return
     *     The action
     */
    public int getAction() {
        return action;
    }

    /**
     * 
     * @param action
     *     The action
     */
    public void setAction(int action) {
        this.action = action;
    }

}
