/**
 * Created by dungnx on 4/28/15.
 */

package com.incredibl3.fumaki.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.incredibl3.fumaki.FumakiActivity;
import com.incredibl3.fumaki.R;

public class AppSettingsFragment extends AFragment {

    public static final String TAG = "SettingsFragment";
    private CallbackManager callbackManager;
    private Button logoutButton;
    private Button deleteFumakiButton;
    private Switch newMatchesSwitch;
    private Switch messagesSwitch;
    private Switch profileUpdatesSwitch;
    private Switch inAppVibrationsSwitch;
    private LinearLayout llPrivacy;
    private LinearLayout llTermOfServices;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.app_settings, container, false);

        callbackManager = CallbackManager.Factory.create();
        SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences("PNSettings", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = settings.edit();

        logoutButton = (Button) view.findViewById(R.id.btnLogout);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Log-Out !!!", Toast.LENGTH_SHORT).show();
            }
        });

        deleteFumakiButton = (Button) view.findViewById(R.id.btnDeleteFumaki);
        deleteFumakiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity().getApplicationContext(), "Delete Fumaki !!!", Toast.LENGTH_SHORT).show();
                ((FumakiActivity)getActivity()).getRequestController().deleteMatches();
            }
        });

        newMatchesSwitch = (Switch) view.findViewById(R.id.newMatchesSwitch);
        newMatchesSwitch.setChecked(settings.getBoolean("NewMatches", true));
        newMatchesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(getActivity().getApplicationContext(), "New Matches Notification On !!!", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("NewMatches", true);
                    editor.commit();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "New Matches Notification Off !!!", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("NewMatches", false);
                    editor.commit();
                }
            }
        });

        messagesSwitch = (Switch) view.findViewById(R.id.messagesSwitch);
        messagesSwitch.setChecked(settings.getBoolean("Messages", true));
        messagesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(getActivity().getApplicationContext(), "Messages Notification On !!!", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("Messages", true);
                    editor.commit();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Messages Notification Off !!!", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("Messages", false);
                    editor.commit();
                }
            }
        });

        profileUpdatesSwitch = (Switch) view.findViewById(R.id.profileUpdatesSwitch);
        profileUpdatesSwitch.setChecked(settings.getBoolean("ProfileUpdates", true));
        profileUpdatesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(getActivity().getApplicationContext(), "Profile/Picture Updates Notification On !!!", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("ProfileUpdates", true);
                    editor.commit();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Profile/Picture Updates Notification Off !!!", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("ProfileUpdates", false);
                    editor.commit();
                }
            }
        });

        inAppVibrationsSwitch = (Switch) view.findViewById(R.id.inAppVibrationsSwitch);
        inAppVibrationsSwitch.setChecked(settings.getBoolean("InAppVibrations", true));
        inAppVibrationsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(getActivity().getApplicationContext(), "In-App Vibrations Notification On !!!", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("InAppVibrations", true);
                    editor.commit();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "In-App Vibrations Updates Notification Off !!!", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("InAppVibrations", false);
                    editor.commit();
                }
            }
        });

        llPrivacy = (LinearLayout) view.findViewById(R.id.llPrivacy);
        llPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Privacy Policy not implemented !!!", Toast.LENGTH_SHORT).show();
            }
        });

        llTermOfServices = (LinearLayout) view.findViewById(R.id.llTermOfServices);
        llTermOfServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Term Of Services not implemented !!!", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
