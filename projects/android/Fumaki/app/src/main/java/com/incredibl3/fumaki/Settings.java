package com.incredibl3.fumaki;

import com.incredibl3.utils.IUtils;

/**
 * Created by tien.dinhvan on 5/20/2015.
 */
public class Settings {
    private FumakiActivity mContext;

    public final String FILTER_STRING = "Settings";
    public final String DISTANCE_FILTER = "distance_filter";

    public float mDistance;
    public float GetDistance() { return mDistance; }

    public Settings(FumakiActivity mContext) {
        this.mContext = mContext;

        Initialize();
    }

    public void Initialize() {
        mDistance = IUtils.getPreferenceFloat(DISTANCE_FILTER, 50, FILTER_STRING);
    }

    public void Save() {
        IUtils.setPreference(DISTANCE_FILTER, mDistance, FILTER_STRING);
    }
}
