/**
 * Created by dungnx on 4/28/15.
 */

package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.incredibl3.fumaki.R;
import com.incredibl3.fumaki.chat.MatchListAdapter;
import com.incredibl3.fumaki.model.MatcherModel;
import com.incredibl3.utils.Console;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MatchListFragment extends AFragment {

    public static final String TAG = "MatchListFragment";
    private CallbackManager callbackManager;

    private MatchListAdapter adapter;
    private List<MatcherModel> matchers;
    private ListView listViewMatch;
    private Map<String, String> mLastChatMap = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.match_list, container, false);

        callbackManager = CallbackManager.Factory.create();
        listViewMatch = (ListView) view.findViewById(R.id.match_list);

        matchers = new ArrayList<MatcherModel>();

        adapter = new MatchListAdapter(getActivity().getApplicationContext(), matchers);
        listViewMatch.setAdapter(adapter);
        listViewMatch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MatcherModel m = (MatcherModel)listViewMatch.getAdapter().getItem(position);
                Toast.makeText(getActivity(), "You Clicked at " + position + ": " + m.getName() + " | userid: " + m.getUserID(), Toast.LENGTH_SHORT).show();
                mCallback.goToMessenger(m.getUserID());
            }
        });

        return view;
    }

    /**
     * Appending match to list view
     * */
    private void appendMatch(final MatcherModel m) {
        for (String key: mLastChatMap.keySet()) {
            if (key.equals(m.getUserID())) {
                m.setLastChat(mLastChatMap.get(key));
            }
        }

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                matchers.add(m);
                Collections.sort(matchers);
                adapter.notifyDataSetChanged();
            }
        });
    }


    public void setMatchers(MatcherModel matcherModels[]){
        Console.Log(TAG, "setMatchers on MatchList");

        matchers.clear();
        if(matcherModels == null) return;

        for(int i = 0; i < matcherModels.length; i++) {
//            Integer actionMatchID;
//            if(users[i].getGender() == 0)
//                actionMatchID = R.drawable.fuck_new;
//            else
//                actionMatchID = R.drawable.marry_new;
//            MatchList m = new MatchList(users[i].getAvatarUrl().replace("type=large", "width=50&height=50"), actionMatchID, users[i].getName(), users[i].getBirthday(), users[i].getUserID());
            appendMatch(matcherModels[i]);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void UpdateMatcherModel(Map<String, String> mMap) {
        Console.Log(TAG, "UpdateMatcherModel on MatchList");
        mLastChatMap = mMap;
    }
}
