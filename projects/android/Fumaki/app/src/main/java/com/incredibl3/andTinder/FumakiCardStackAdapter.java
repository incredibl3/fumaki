package com.incredibl3.andTinder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andtinder.model.CardModel;
import com.andtinder.view.CardStackAdapter;

import com.incredibl3.fumaki.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dung.nguyenxuan on 8/5/2015.
 */
public final class FumakiCardStackAdapter extends CardStackAdapter {
    private final List<String> urls = new ArrayList<String>();

    public FumakiCardStackAdapter(Context mContext, final String[] URLS) {
        super(mContext);
        Collections.addAll(this.urls, URLS);
    }

    @Override
    public View getCardView(int position, CardModel model, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.std_fumaki_card, parent, false);
            holder = new ViewHolder();
            holder.image = (ImageView) convertView.findViewById(R.id.profilePicture);
            //holder.title = (TextView) convertView.findViewById(R.id.candidateName);
            //holder.description = (TextView) convertView.findViewById(R.id.candidateAge);
            assert convertView != null;
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        final String url = urls.get(position);

        holder.image.post(new Runnable() {
            @Override
            public void run() {
                // Trigger the download of the URL asynchronously into the image view.
                try {
                    Picasso.with(getContext())
                            .load(url)
                            .resize(holder.image.getWidth(), holder.image.getHeight())
                            .tag(getContext())
                            .into(holder.image);
                } catch (IllegalArgumentException e){
                    e.printStackTrace();
                }
            }
        });



        return convertView;
    }

    static class ViewHolder {
        ImageView image;
        TextView title;
        TextView description;
    }
}
