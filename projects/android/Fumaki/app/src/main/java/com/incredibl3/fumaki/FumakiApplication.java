

/**
 * Created by dungnx on 4/28/15.
 */

package com.incredibl3.fumaki;

import android.support.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;
import com.incredibl3.utils.LocationManager;

/**
 * Use a custom Application class to pass state data between Activities.
 */
public class FumakiApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize Facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
