package com.incredibl3.fumaki.XMPP;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

/**
 * Created by tien.dinhvan on 7/17/2015.
 */
public class ListIQ extends IQ {

    /**
     * Final Variable
     */
    public final static int MAX_SIZE = 100;
    public final static int NOT_LOADED = 0;
    public final static int PARTLY_LOADED = 0;
    public final static int FULLY_LOADED = 0;

    public ResultSet rSet;

    private int state; // 0: Not loaded, 1: Loaded but not full; 2: Loaded fully
    public void SetState(int state) { this.state = state; }
    public int GetState() { return state; }

    private int after = 0;
    public void SetAfter(int after) { this.after = after; }

    public ListIQ(String childElementName, String childElementNamespace) {
        super(childElementName, childElementNamespace);
        rSet = new ResultSet();
        state = NOT_LOADED;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {

        //return "<list  xmlns='urn:xmpp:archive' with='test@customOpenfire.com'><set xmlns='http://jabber.org/protocol/rsm'><max xmlns='http://jabber.org/protocol/rsm'>30</max></set> </list>";
        //xml.attribute("with", "10153435935273949@fumaki");
        xml.rightAngleBracket();
        xml.element( new ExtensionElement() {
            @Override
            public String getNamespace() {
                return "http://jabber.org/protocol/rsm";
            }

            @Override
            public String getElementName() {
                return "set";
            }

            @Override
            public CharSequence toXML() {
                XmlStringBuilder xml = new XmlStringBuilder(this);
                xml.rightAngleBracket();
                xml.element("max", String.valueOf(MAX_SIZE));
                if (state == 1) {
                    xml.element("after", String.valueOf(after));
                }
                xml.closeElement("set");
                return xml;
            }
        });

        return xml;
    }

    public class ResultSet {
        private int first;
        private int last;
        private int count;
        private int indexAtt;

        public ResultSet()
        {
        }

        public int getLast()
        {
            return last;
        }

        public void setLast(int last)
        {
            this.last = last;
        }

        public int getCount()
        {
            return count;
        }

        public void setCount(int count)
        {
            this.count = count;
        }

        public int getFirst()
        {
            return first;
        }

        public void setFirst(int first)
        {
            this.first = first;
        }

        public int getIndexAtt()
        {
            return indexAtt;
        }

        public void setIndexAtt(int indexAtt)
        {
            this.indexAtt = indexAtt;
        }
    }
}