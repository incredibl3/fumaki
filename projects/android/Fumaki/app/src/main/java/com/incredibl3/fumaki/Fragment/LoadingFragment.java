package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.incredibl3.fumaki.FumakiActivity;
import com.incredibl3.fumaki.R;
import com.incredibl3.fumaki.model.UserModel;
import com.incredibl3.utils.Console;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dung.nguyenxuan on 24/05/2015.
 */
public class LoadingFragment extends AFragment {

    public static String TAG = "LoadingFragment";

    private static int MAX_PROFILE_PICS = 6;

    private static final String NAME = "name";
    private static final String ID = "id";
    private static final String PICTURE = "picture";
    private static final String BIRTHDAY = "birthday";
    private static final String GENDER = "gender";
    private static final String FIELDS = "fields";

    private String profilePicsURL[];
    private String avatarURL;

//    private RequestController mRequestController;

    private static final String REQUEST_FIELDS =
            TextUtils.join(",", new String[]{ID, NAME, BIRTHDAY, GENDER, PICTURE});

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.loading, container, false);

//        mRequestController = new RequestController((FumakiActivity)getActivity());

        profilePicsURL = new String[MAX_PROFILE_PICS];

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onResume(){
        super.onResume();
    }

    public void loginToServer(){
        ((FumakiActivity)getActivity()).getRequestController().loginToServer();
    }

    public void createUser(){
        ((FumakiActivity)getActivity()).getRequestController().createUser(((FumakiActivity) getActivity()).getUser().getModel());
    }

    public void updateUserInfo(){
        ((FumakiActivity)getActivity()).getRequestController().updateUserInfo(((FumakiActivity) getActivity()).getUser().getModel());
    }

    public void seekCandidates(){
        ((FumakiActivity)getActivity()).getRequestController().seekCandidates();
    }
}
