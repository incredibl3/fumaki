/**
 * Created by dungnx on 4/28/15.
 */

package com.incredibl3.fumaki.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.facebook.CallbackManager;
import com.incredibl3.fumaki.R;

public class MainSettingsFragment extends AFragment {

    public static final String TAG = "MainSettings";
    private CallbackManager callbackManager;
    private LinearLayout llFinderPreferences;
    private LinearLayout llAppSettings;
    private LinearLayout llViewPersonalProfile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_settings, container, false);

        callbackManager = CallbackManager.Factory.create();
        llFinderPreferences = (LinearLayout) view.findViewById(R.id.llFinderPreferences);
        llFinderPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.goToFinderSettings();
            }
        });

        llAppSettings = (LinearLayout) view.findViewById(R.id.llAppSettings);
        llAppSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.goToAppSettings();
            }
        });

        llViewPersonalProfile = (LinearLayout) view.findViewById(R.id.llViewPersonalProfile);
        llViewPersonalProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.goToPersonalProfile();
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
