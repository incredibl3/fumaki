package com.incredibl3.fumaki.XMPP;

import com.incredibl3.utils.Console;
import com.incredibl3.utils.Stopwatch;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by tien.dinhvan on 7/22/2015.
 */
public class TimeIQProvider extends IQProvider {
    public final static String TAG = "TimeIQProvider";

    public TimeIQProvider() {

    }

    @Override
    public IQ parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {
        Console.Log(TAG, "TimeIQProvider ++++++");
        Stopwatch.GetInstance().reset();
        TimeIQ timeIQ = XMPPManager.GetInstance().GetTimeIQ();

        boolean done = false;

        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {
                if (parser.getName().equals("utc")) {
                    timeIQ.setUtc(parser.nextText());
                    Console.Log(TAG, "Retrieve server time: " + timeIQ.getUtc());
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getName().equals("query")) {
                    done = true;
                }
            }
        }

        return timeIQ;
    }
}