@echo off
rem **********************
rem config.bat
rem **********************
call %~dp0config.bat

if "%USE_PUSH_NOTIFICATION%"=="1" (
	if exist %OPENFIRE_DIR%src\plugins\pushNotification rmdir %OPENFIRE_DIR%src\plugins\pushNotification
	mkdir %OPENFIRE_DIR%src\plugins\pushNotification
	xcopy  %~dp0pushNotification %OPENFIRE_DIR%src\plugins\pushNotification /E /Y>nul
)