@echo off
rem **********************
rem config.bat
rem **********************
call %~dp0config.bat

if "%USE_PUSH_NOTIFICATION%"=="1" (
	if exist %OPENFIRE_DIR%src\plugins\pushNotification (
		xcopy %OPENFIRE_DIR%src\plugins\pushNotification\src %~dp0pushNotification\src /E /Y>nul
	)
)